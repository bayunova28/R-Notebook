Clothes Size Prediction
================
Bayu Nova
July 27, 2021

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook.

``` r
#import library
library(readr)
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

``` r
library(ggplot2)
library(gtools)
library(gmodels)
library(ggthemes)
library(tidyr)
library(MASS)
```

    ## 
    ## Attaching package: 'MASS'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     select

``` r
library(ggpubr)

#data extraction
final_test <- read_csv("D:/final_test.csv")
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   weight = col_double(),
    ##   age = col_double(),
    ##   height = col_double(),
    ##   size = col_character()
    ## )

``` r
View(final_test)
str(final_test)
```

    ## spec_tbl_df [119,734 x 4] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
    ##  $ weight: num [1:119734] 62 59 61 65 62 50 53 51 54 53 ...
    ##  $ age   : num [1:119734] 28 36 34 27 45 27 65 33 26 32 ...
    ##  $ height: num [1:119734] 173 168 165 175 173 ...
    ##  $ size  : chr [1:119734] "XL" "L" "M" "L" ...
    ##  - attr(*, "spec")=
    ##   .. cols(
    ##   ..   weight = col_double(),
    ##   ..   age = col_double(),
    ##   ..   height = col_double(),
    ##   ..   size = col_character()
    ##   .. )

``` r
dim(final_test)
```

    ## [1] 119734      4

``` r
knitr::kable(head(final_test))
```

| weight | age | height | size |
|-------:|----:|-------:|:-----|
|     62 |  28 | 172.72 | XL   |
|     59 |  36 | 167.64 | L    |
|     61 |  34 | 165.10 | M    |
|     65 |  27 | 175.26 | L    |
|     62 |  45 | 172.72 | M    |
|     50 |  27 | 160.02 | S    |

``` r
summary(final_test)
```

    ##      weight            age             height          size          
    ##  Min.   : 22.00   Min.   :  0.00   Min.   :137.2   Length:119734     
    ##  1st Qu.: 55.00   1st Qu.: 29.00   1st Qu.:160.0   Class :character  
    ##  Median : 61.00   Median : 32.00   Median :165.1   Mode  :character  
    ##  Mean   : 61.76   Mean   : 34.03   Mean   :165.8                     
    ##  3rd Qu.: 67.00   3rd Qu.: 37.00   3rd Qu.:170.2                     
    ##  Max.   :136.00   Max.   :117.00   Max.   :193.0                     
    ##                   NA's   :257      NA's   :330

``` r
df <- data.frame(final_test)
head(df)
```

    ##   weight age height size
    ## 1     62  28 172.72   XL
    ## 2     59  36 167.64    L
    ## 3     61  34 165.10    M
    ## 4     65  27 175.26    L
    ## 5     62  45 172.72    M
    ## 6     50  27 160.02    S

``` r
#transform data
df$size <- factor(df$size)
str(df)
```

    ## 'data.frame':    119734 obs. of  4 variables:
    ##  $ weight: num  62 59 61 65 62 50 53 51 54 53 ...
    ##  $ age   : num  28 36 34 27 45 27 65 33 26 32 ...
    ##  $ height: num  173 168 165 175 173 ...
    ##  $ size  : Factor w/ 7 levels "L","M","S","XL",..: 4 1 2 1 2 3 2 6 2 3 ...

``` r
#logistic regression model
set.seed(34000)
target <- sample(2, nrow(df), replace = TRUE)
training <- df[target == 2,]
testing <- df[target == 1,]
prop.table(table(df$size))
```

    ## 
    ##            L            M            S           XL          XXL          XXS 
    ## 0.1468839260 0.2481500660 0.1831058847 0.1596789550 0.0005762774 0.0832177995 
    ##         XXXL 
    ## 0.1783870914

``` r
lr_model <- glm(size ~ weight + age + height, data = training, family = "binomial")
summary(lr_model)
```

    ## 
    ## Call:
    ## glm(formula = size ~ weight + age + height, family = "binomial", 
    ##     data = training)
    ## 
    ## Deviance Residuals: 
    ##     Min       1Q   Median       3Q      Max  
    ## -2.2814   0.4995   0.5438   0.5867   0.7958  
    ## 
    ## Coefficients:
    ##              Estimate Std. Error z value Pr(>|z|)    
    ## (Intercept)  6.738649   0.295012  22.842  < 2e-16 ***
    ## weight       0.001956   0.001282   1.526  0.12705    
    ## age         -0.004345   0.001407  -3.088  0.00202 ** 
    ## height      -0.029750   0.001872 -15.894  < 2e-16 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## (Dispersion parameter for binomial family taken to be 1)
    ## 
    ##     Null deviance: 49657  on 59679  degrees of freedom
    ## Residual deviance: 49368  on 59676  degrees of freedom
    ##   (287 observations deleted due to missingness)
    ## AIC: 49376
    ## 
    ## Number of Fisher Scoring iterations: 4

``` r
#prediction
prediction <- predict(lr_model, testing, type = "response")
summary(prediction)
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
    ##  0.7199  0.8384  0.8565  0.8539  0.8720  0.9345     294

``` r
lr_check <- rep('Down', length(prediction))
lr_check[prediction >= 0.5] <- 'Up'
table(lr_check, testing$size)
```

    ##         
    ## lr_check     L     M     S    XL   XXL   XXS  XXXL
    ##     Down    59    66    48    41     0    29    51
    ##     Up    8757 14824 10853  9496    24  4923 10596

``` r
#visualization
plot(prediction, main = "Prediction of Logistic Linear Model")
```

![](Clothes-Size-Prediction_files/figure-gfm/unnamed-chunk-1-1.png)<!-- -->

``` r
ggplot(df) + geom_jitter(aes(df$weight, df$height, colour=df$size))
```

    ## Warning: Use of `df$weight` is discouraged. Use `weight` instead.

    ## Warning: Use of `df$height` is discouraged. Use `height` instead.

    ## Warning: Use of `df$size` is discouraged. Use `size` instead.

    ## Warning: Removed 330 rows containing missing values (geom_point).

![](Clothes-Size-Prediction_files/figure-gfm/unnamed-chunk-1-2.png)<!-- -->

``` r
ggplot(df) + geom_boxplot(aes(df$size, df$weight))
```

    ## Warning: Use of `df$size` is discouraged. Use `size` instead.

    ## Warning: Use of `df$weight` is discouraged. Use `weight` instead.

![](Clothes-Size-Prediction_files/figure-gfm/unnamed-chunk-1-3.png)<!-- -->

``` r
ggplot(df) + geom_boxplot(aes(df$size, df$height))
```

    ## Warning: Use of `df$size` is discouraged. Use `size` instead.

    ## Warning: Use of `df$height` is discouraged. Use `height` instead.

    ## Warning: Removed 330 rows containing non-finite values (stat_boxplot).

![](Clothes-Size-Prediction_files/figure-gfm/unnamed-chunk-1-4.png)<!-- -->

``` r
ggplot(df, aes(df$weight, fill = df$size))+ geom_histogram(binwidth = 11,col="darkblue")
```

    ## Warning: Use of `df$weight` is discouraged. Use `weight` instead.

    ## Warning: Use of `df$size` is discouraged. Use `size` instead.

![](Clothes-Size-Prediction_files/figure-gfm/unnamed-chunk-1-5.png)<!-- -->

``` r
ggplot(df, aes(df$height, fill = df$size))+ geom_histogram(binwidth = 11,col="darkblue")
```

    ## Warning: Use of `df$height` is discouraged. Use `height` instead.

    ## Warning: Use of `df$size` is discouraged. Use `size` instead.

    ## Warning: Removed 330 rows containing non-finite values (stat_bin).

![](Clothes-Size-Prediction_files/figure-gfm/unnamed-chunk-1-6.png)<!-- -->

``` r
training %>% ggplot(aes(x = weight, color = size, fill = size)) + geom_density(alpha=.25) + theme_bw() + labs(x = "Weight", y = "Density", title = "Weight of Clothes Size")
```

![](Clothes-Size-Prediction_files/figure-gfm/unnamed-chunk-1-7.png)<!-- -->

``` r
training %>% ggplot(aes(x = height, color = size, fill = size)) + geom_density(alpha=.25) + theme_bw() + labs(x = "Height", y = "Density", title = "Height of Clothes Size")
```

    ## Warning: Removed 163 rows containing non-finite values (stat_density).

![](Clothes-Size-Prediction_files/figure-gfm/unnamed-chunk-1-8.png)<!-- -->

``` r
table(df['size'])
```

    ## 
    ##     L     M     S    XL   XXL   XXS  XXXL 
    ## 17587 29712 21924 19119    69  9964 21359

``` r
size <- c(17587, 29712, 21924, 19119, 69, 9964, 21359)
percent <- round(100 * size / sum(size), 1)
pie(size, labels = percent, main = "Clothes Size", col = rainbow(length(size)))
legend("topright", c("L", "M", "S", "XL", "XXL", "XXS", "XXXL"), cex = 0.8, fill = rainbow(length(size)))
```

![](Clothes-Size-Prediction_files/figure-gfm/unnamed-chunk-1-9.png)<!-- -->
