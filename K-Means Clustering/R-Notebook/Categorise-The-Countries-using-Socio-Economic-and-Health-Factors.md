Categorise The Countries using Socio-Economic and Health Factors
================
Bayu Nova
July 22, 2021

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook.

``` r
#import library
library(readr)
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

``` r
library(cluster)
library(factoextra)
```

    ## Loading required package: ggplot2

    ## Welcome! Want to learn more? See two factoextra-related books at https://goo.gl/ve3WBa

``` r
library(tidyverse)
```

    ## -- Attaching packages --------------------------------------- tidyverse 1.3.1 --

    ## v tibble  3.1.2     v stringr 1.4.0
    ## v tidyr   1.1.3     v forcats 0.5.1
    ## v purrr   0.3.4

    ## -- Conflicts ------------------------------------------ tidyverse_conflicts() --
    ## x dplyr::filter() masks stats::filter()
    ## x dplyr::lag()    masks stats::lag()

``` r
library(gridExtra)
```

    ## 
    ## Attaching package: 'gridExtra'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     combine

``` r
#data extraction
Country_data <- read_csv("D:/Country-data.csv")
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   country = col_character(),
    ##   child_mort = col_double(),
    ##   exports = col_double(),
    ##   health = col_double(),
    ##   imports = col_double(),
    ##   income = col_double(),
    ##   inflation = col_double(),
    ##   life_expec = col_double(),
    ##   total_fer = col_double(),
    ##   gdpp = col_double()
    ## )

``` r
View(Country_data)
knitr::kable(head(Country_data))
```

| country             | child\_mort | exports | health | imports | income | inflation | life\_expec | total\_fer |  gdpp |
|:--------------------|------------:|--------:|-------:|--------:|-------:|----------:|------------:|-----------:|------:|
| Afghanistan         |        90.2 |    10.0 |   7.58 |    44.9 |   1610 |      9.44 |        56.2 |       5.82 |   553 |
| Albania             |        16.6 |    28.0 |   6.55 |    48.6 |   9930 |      4.49 |        76.3 |       1.65 |  4090 |
| Algeria             |        27.3 |    38.4 |   4.17 |    31.4 |  12900 |     16.10 |        76.5 |       2.89 |  4460 |
| Angola              |       119.0 |    62.3 |   2.85 |    42.9 |   5900 |     22.40 |        60.1 |       6.16 |  3530 |
| Antigua and Barbuda |        10.3 |    45.5 |   6.03 |    58.9 |  19100 |      1.44 |        76.8 |       2.13 | 12200 |
| Argentina           |        14.5 |    18.9 |   8.10 |    16.0 |  18700 |     20.90 |        75.8 |       2.37 | 10300 |

``` r
str(Country_data)
```

    ## spec_tbl_df [167 x 10] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
    ##  $ country   : chr [1:167] "Afghanistan" "Albania" "Algeria" "Angola" ...
    ##  $ child_mort: num [1:167] 90.2 16.6 27.3 119 10.3 14.5 18.1 4.8 4.3 39.2 ...
    ##  $ exports   : num [1:167] 10 28 38.4 62.3 45.5 18.9 20.8 19.8 51.3 54.3 ...
    ##  $ health    : num [1:167] 7.58 6.55 4.17 2.85 6.03 8.1 4.4 8.73 11 5.88 ...
    ##  $ imports   : num [1:167] 44.9 48.6 31.4 42.9 58.9 16 45.3 20.9 47.8 20.7 ...
    ##  $ income    : num [1:167] 1610 9930 12900 5900 19100 18700 6700 41400 43200 16000 ...
    ##  $ inflation : num [1:167] 9.44 4.49 16.1 22.4 1.44 20.9 7.77 1.16 0.873 13.8 ...
    ##  $ life_expec: num [1:167] 56.2 76.3 76.5 60.1 76.8 75.8 73.3 82 80.5 69.1 ...
    ##  $ total_fer : num [1:167] 5.82 1.65 2.89 6.16 2.13 2.37 1.69 1.93 1.44 1.92 ...
    ##  $ gdpp      : num [1:167] 553 4090 4460 3530 12200 10300 3220 51900 46900 5840 ...
    ##  - attr(*, "spec")=
    ##   .. cols(
    ##   ..   country = col_character(),
    ##   ..   child_mort = col_double(),
    ##   ..   exports = col_double(),
    ##   ..   health = col_double(),
    ##   ..   imports = col_double(),
    ##   ..   income = col_double(),
    ##   ..   inflation = col_double(),
    ##   ..   life_expec = col_double(),
    ##   ..   total_fer = col_double(),
    ##   ..   gdpp = col_double()
    ##   .. )

``` r
dim(Country_data)
```

    ## [1] 167  10

``` r
summary(Country_data)
```

    ##    country            child_mort        exports            health      
    ##  Length:167         Min.   :  2.60   Min.   :  0.109   Min.   : 1.810  
    ##  Class :character   1st Qu.:  8.25   1st Qu.: 23.800   1st Qu.: 4.920  
    ##  Mode  :character   Median : 19.30   Median : 35.000   Median : 6.320  
    ##                     Mean   : 38.27   Mean   : 41.109   Mean   : 6.816  
    ##                     3rd Qu.: 62.10   3rd Qu.: 51.350   3rd Qu.: 8.600  
    ##                     Max.   :208.00   Max.   :200.000   Max.   :17.900  
    ##     imports             income         inflation         life_expec   
    ##  Min.   :  0.0659   Min.   :   609   Min.   : -4.210   Min.   :32.10  
    ##  1st Qu.: 30.2000   1st Qu.:  3355   1st Qu.:  1.810   1st Qu.:65.30  
    ##  Median : 43.3000   Median :  9960   Median :  5.390   Median :73.10  
    ##  Mean   : 46.8902   Mean   : 17145   Mean   :  7.782   Mean   :70.56  
    ##  3rd Qu.: 58.7500   3rd Qu.: 22800   3rd Qu.: 10.750   3rd Qu.:76.80  
    ##  Max.   :174.0000   Max.   :125000   Max.   :104.000   Max.   :82.80  
    ##    total_fer          gdpp       
    ##  Min.   :1.150   Min.   :   231  
    ##  1st Qu.:1.795   1st Qu.:  1330  
    ##  Median :2.410   Median :  4660  
    ##  Mean   :2.948   Mean   : 12964  
    ##  3rd Qu.:3.880   3rd Qu.: 14050  
    ##  Max.   :7.490   Max.   :105000

``` r
#target data 
socio_economic <- Country_data[,c("exports", "imports", "income")]
head(socio_economic)
```

    ## # A tibble: 6 x 3
    ##   exports imports income
    ##     <dbl>   <dbl>  <dbl>
    ## 1    10      44.9   1610
    ## 2    28      48.6   9930
    ## 3    38.4    31.4  12900
    ## 4    62.3    42.9   5900
    ## 5    45.5    58.9  19100
    ## 6    18.9    16    18700

``` r
df1 <- scale(socio_economic)

health_factor <- Country_data[,c("health", "life_expec", "total_fer")]
head(health_factor)
```

    ## # A tibble: 6 x 3
    ##   health life_expec total_fer
    ##    <dbl>      <dbl>     <dbl>
    ## 1   7.58       56.2      5.82
    ## 2   6.55       76.3      1.65
    ## 3   4.17       76.5      2.89
    ## 4   2.85       60.1      6.16
    ## 5   6.03       76.8      2.13
    ## 6   8.1        75.8      2.37

``` r
df2 <- scale(health_factor)

#k-means clustering model 

#socio-economic
fviz_nbclust(df1, kmeans, method = "wss")
```

![](Categorise-The-Countries-using-Socio-Economic-and-Health-Factors_files/figure-gfm/unnamed-chunk-1-1.png)<!-- -->

``` r
fviz_nbclust(df1, kmeans, method = "silhouette")
```

![](Categorise-The-Countries-using-Socio-Economic-and-Health-Factors_files/figure-gfm/unnamed-chunk-1-2.png)<!-- -->

``` r
set.seed(34000)
gap_stat <- clusGap(df1, FUNcluster = kmeans, nstart = 25, K.max = 15, B = 30)
fviz_gap_stat(gap_stat)
```

![](Categorise-The-Countries-using-Socio-Economic-and-Health-Factors_files/figure-gfm/unnamed-chunk-1-3.png)<!-- -->

``` r
#prediction accuracy score
accuracy_score <- sum(diag(df1)) / sum(df1)
print(paste('Accuracy score :', accuracy_score))
```

    ## [1] "Accuracy score : 80889319414085"

``` r
#health factor
fviz_nbclust(df2, kmeans, method = "wss")
```

![](Categorise-The-Countries-using-Socio-Economic-and-Health-Factors_files/figure-gfm/unnamed-chunk-1-4.png)<!-- -->

``` r
fviz_nbclust(df2, kmeans, method = "silhouette")
```

![](Categorise-The-Countries-using-Socio-Economic-and-Health-Factors_files/figure-gfm/unnamed-chunk-1-5.png)<!-- -->

``` r
set.seed(34000)
gap_stat <- clusGap(df2, FUNcluster = kmeans, nstart = 25, K.max = 15, B = 30)
fviz_gap_stat(gap_stat)
```

![](Categorise-The-Countries-using-Socio-Economic-and-Health-Factors_files/figure-gfm/unnamed-chunk-1-6.png)<!-- -->

``` r
#prediction accuracy score
accuracy_score <- sum(diag(df2)) / sum(df2)
print(paste('Accuracy score :', accuracy_score))
```

    ## [1] "Accuracy score : 19574100135377.3"

``` r
#visualization 
kmc <- kmeans(df1, 4, nstart = 25)
print(kmc)
```

    ## K-means clustering with 4 clusters of sizes 29, 86, 3, 49
    ## 
    ## Cluster means:
    ##      exports    imports     income
    ## 1  0.1727656 -0.4169283  1.5209423
    ## 2 -0.5452851 -0.4854374 -0.4686614
    ## 3  4.9208731  4.5344203  2.4322274
    ## 4  0.5535040  0.8211281 -0.2265128
    ## 
    ## Clustering vector:
    ##   [1] 2 2 2 4 4 2 2 1 1 2 2 1 2 2 4 4 4 2 4 2 2 4 2 1 4 2 2 4 2 1 4 2 2 2 2 2 2
    ##  [38] 2 4 2 2 2 1 4 1 2 2 2 2 4 2 4 4 1 1 2 2 2 1 2 2 2 2 2 2 4 2 4 1 2 2 2 2 4
    ##  [75] 1 1 2 1 4 2 2 4 1 4 2 4 4 4 4 1 4 3 4 2 2 4 4 2 3 4 4 4 4 4 4 2 2 2 4 2 1
    ## [112] 1 2 2 1 1 2 4 4 2 2 2 2 1 2 2 2 2 1 2 2 4 2 3 4 4 4 2 1 1 2 2 2 2 1 1 2 2
    ## [149] 4 2 4 2 4 2 4 2 4 1 1 1 2 2 4 2 4 2 2
    ## 
    ## Within cluster sum of squares by cluster:
    ## [1] 49.718661 47.171349  8.034093 52.719042
    ##  (between_SS / total_SS =  68.3 %)
    ## 
    ## Available components:
    ## 
    ## [1] "cluster"      "centers"      "totss"        "withinss"     "tot.withinss"
    ## [6] "betweenss"    "size"         "iter"         "ifault"

``` r
fviz_cluster(kmc, data = df1, main = "Socio-Economic")
```

![](Categorise-The-Countries-using-Socio-Economic-and-Health-Factors_files/figure-gfm/unnamed-chunk-1-7.png)<!-- -->

``` r
kmc <- kmeans(df2, 4, nstart = 25)
print(kmc)
```

    ## K-means clustering with 4 clusters of sizes 17, 32, 68, 50
    ## 
    ## Cluster means:
    ##       health life_expec  total_fer
    ## 1  1.0686791 -1.4257251  0.9704377
    ## 2 -0.6835365 -1.1721845  1.5276710
    ## 3 -0.6529264  0.2503791 -0.4034476
    ## 4  0.9620924  0.8944291 -0.7589695
    ## 
    ## Clustering vector:
    ##   [1] 2 3 3 2 3 4 3 4 4 3 4 3 3 4 3 4 3 2 3 3 4 1 4 3 3 2 1 3 2 4 3 2 2 4 3 4 2
    ##  [38] 2 2 4 2 4 3 4 4 3 4 3 3 2 2 3 3 4 4 2 2 4 4 2 4 3 3 2 1 3 1 4 4 3 3 3 1 4
    ##  [75] 4 4 3 4 4 3 2 1 3 3 3 3 4 1 1 3 3 4 3 2 2 3 3 2 4 2 3 1 4 3 4 3 2 3 1 3 4
    ## [112] 4 2 2 4 3 2 4 3 3 3 4 4 3 3 3 1 3 3 2 4 3 1 3 4 4 1 1 4 4 3 3 2 3 4 4 3 2
    ## [149] 3 1 1 3 3 3 3 1 4 3 4 4 4 3 2 3 3 2 2
    ## 
    ## Within cluster sum of squares by cluster:
    ## [1] 34.43195 25.73896 40.97415 33.87008
    ##  (between_SS / total_SS =  72.9 %)
    ## 
    ## Available components:
    ## 
    ## [1] "cluster"      "centers"      "totss"        "withinss"     "tot.withinss"
    ## [6] "betweenss"    "size"         "iter"         "ifault"

``` r
fviz_cluster(kmc, data = df2, main = "Health Factor")
```

![](Categorise-The-Countries-using-Socio-Economic-and-Health-Factors_files/figure-gfm/unnamed-chunk-1-8.png)<!-- -->

``` r
boxplot(df1, data=socio_economic, main="Socio-Economic", ylab="Count")
```

![](Categorise-The-Countries-using-Socio-Economic-and-Health-Factors_files/figure-gfm/unnamed-chunk-1-9.png)<!-- -->

``` r
boxplot(df2, data=health_factor, main="Health factor", ylab="count")
```

![](Categorise-The-Countries-using-Socio-Economic-and-Health-Factors_files/figure-gfm/unnamed-chunk-1-10.png)<!-- -->

``` r
par(mfrow = c(1,2))
plot(df1, main = "Socio-Economic")
plot(df2, main = "Health Factor")
```

![](Categorise-The-Countries-using-Socio-Economic-and-Health-Factors_files/figure-gfm/unnamed-chunk-1-11.png)<!-- -->

``` r
# compare K value (socio-economic)
k2 <- kmeans(df1, centers = 2, nstart = 25)
k3 <- kmeans(df1, centers = 3, nstart = 25)
k4 <- kmeans(df1, centers = 4, nstart = 25)
k5 <- kmeans(df1, centers = 5, nstart = 25)

p2 <- fviz_cluster(k2, geom = "point", data = df1) + ggtitle("k = 2")
p3 <- fviz_cluster(k3, geom = "point", data = df1) + ggtitle("k = 3")
p4 <- fviz_cluster(k4, geom = "point", data = df1) + ggtitle("k = 4")
p5 <- fviz_cluster(k5, geom = "point", data = df1) + ggtitle("k = 5")
grid.arrange(p2, p3, p4, p5, nrow = 2)
```

![](Categorise-The-Countries-using-Socio-Economic-and-Health-Factors_files/figure-gfm/unnamed-chunk-1-12.png)<!-- -->

``` r
#compare K value (health factor)
k2 <- kmeans(df2, centers = 2, nstart = 25)
k3 <- kmeans(df2, centers = 3, nstart = 25)
k4 <- kmeans(df2, centers = 4, nstart = 25)
k5 <- kmeans(df2, centers = 5, nstart = 25)

p2 <- fviz_cluster(k2, geom = "point", data = df2) + ggtitle("k = 2")
p3 <- fviz_cluster(k3, geom = "point", data = df2) + ggtitle("k = 3")
p4 <- fviz_cluster(k4, geom = "point", data = df2) + ggtitle("k = 4")
p5 <- fviz_cluster(k5, geom = "point", data = df2) + ggtitle("k = 5")
grid.arrange(p2, p3, p4, p5, nrow = 2)
```

![](Categorise-The-Countries-using-Socio-Economic-and-Health-Factors_files/figure-gfm/unnamed-chunk-1-13.png)<!-- -->
