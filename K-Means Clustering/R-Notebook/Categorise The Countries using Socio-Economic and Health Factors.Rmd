---
title: "Categorise The Countries using Socio-Economic and Health Factors"
author: Bayu Nova
date: July 22, 2021
output: github_document
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. 

```{r}
#import library
library(readr)
library(dplyr)
library(cluster)
library(factoextra)
library(tidyverse)
library(gridExtra)

#data extraction
Country_data <- read_csv("D:/Country-data.csv")
View(Country_data)
knitr::kable(head(Country_data))
str(Country_data)
dim(Country_data)
summary(Country_data)

#target data 
socio_economic <- Country_data[,c("exports", "imports", "income")]
head(socio_economic)
df1 <- scale(socio_economic)

health_factor <- Country_data[,c("health", "life_expec", "total_fer")]
head(health_factor)
df2 <- scale(health_factor)

#k-means clustering model 

#socio-economic
fviz_nbclust(df1, kmeans, method = "wss")
fviz_nbclust(df1, kmeans, method = "silhouette")

set.seed(34000)
gap_stat <- clusGap(df1, FUNcluster = kmeans, nstart = 25, K.max = 15, B = 30)
fviz_gap_stat(gap_stat)

#prediction accuracy score
accuracy_score <- sum(diag(df1)) / sum(df1)
print(paste('Accuracy score :', accuracy_score))

#health factor
fviz_nbclust(df2, kmeans, method = "wss")
fviz_nbclust(df2, kmeans, method = "silhouette")

set.seed(34000)
gap_stat <- clusGap(df2, FUNcluster = kmeans, nstart = 25, K.max = 15, B = 30)
fviz_gap_stat(gap_stat)

#prediction accuracy score
accuracy_score <- sum(diag(df2)) / sum(df2)
print(paste('Accuracy score :', accuracy_score))

#visualization 
kmc <- kmeans(df1, 4, nstart = 25)
print(kmc)
fviz_cluster(kmc, data = df1, main = "Socio-Economic")

kmc <- kmeans(df2, 4, nstart = 25)
print(kmc)
fviz_cluster(kmc, data = df2, main = "Health Factor")

boxplot(df1, data=socio_economic, main="Socio-Economic", ylab="Count")
boxplot(df2, data=health_factor, main="Health factor", ylab="count")

par(mfrow = c(1,2))
plot(df1, main = "Socio-Economic")
plot(df2, main = "Health Factor")

# compare K value (socio-economic)
k2 <- kmeans(df1, centers = 2, nstart = 25)
k3 <- kmeans(df1, centers = 3, nstart = 25)
k4 <- kmeans(df1, centers = 4, nstart = 25)
k5 <- kmeans(df1, centers = 5, nstart = 25)

p2 <- fviz_cluster(k2, geom = "point", data = df1) + ggtitle("k = 2")
p3 <- fviz_cluster(k3, geom = "point", data = df1) + ggtitle("k = 3")
p4 <- fviz_cluster(k4, geom = "point", data = df1) + ggtitle("k = 4")
p5 <- fviz_cluster(k5, geom = "point", data = df1) + ggtitle("k = 5")
grid.arrange(p2, p3, p4, p5, nrow = 2)

#compare K value (health factor)
k2 <- kmeans(df2, centers = 2, nstart = 25)
k3 <- kmeans(df2, centers = 3, nstart = 25)
k4 <- kmeans(df2, centers = 4, nstart = 25)
k5 <- kmeans(df2, centers = 5, nstart = 25)

p2 <- fviz_cluster(k2, geom = "point", data = df2) + ggtitle("k = 2")
p3 <- fviz_cluster(k3, geom = "point", data = df2) + ggtitle("k = 3")
p4 <- fviz_cluster(k4, geom = "point", data = df2) + ggtitle("k = 4")
p5 <- fviz_cluster(k5, geom = "point", data = df2) + ggtitle("k = 5")
grid.arrange(p2, p3, p4, p5, nrow = 2)
```
