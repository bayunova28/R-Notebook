Bangladesh Population Clustering
================
Bayu Nova
July 28, 2021

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook.

``` r
#import library
library(readr)
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

``` r
library(caret)
```

    ## Loading required package: lattice

    ## Loading required package: ggplot2

``` r
library(ggplot2)
library(cluster)
library(factoextra)
```

    ## Welcome! Want to learn more? See two factoextra-related books at https://goo.gl/ve3WBa

``` r
library(tidyverse)
```

    ## -- Attaching packages --------------------------------------- tidyverse 1.3.1 --

    ## v tibble  3.1.2     v stringr 1.4.0
    ## v tidyr   1.1.3     v forcats 0.5.1
    ## v purrr   0.3.4

    ## -- Conflicts ------------------------------------------ tidyverse_conflicts() --
    ## x dplyr::filter() masks stats::filter()
    ## x dplyr::lag()    masks stats::lag()
    ## x purrr::lift()   masks caret::lift()

``` r
library(gridExtra)
```

    ## 
    ## Attaching package: 'gridExtra'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     combine

``` r
#data extraction
data_resource_2016_10_24_bangladesh_population_growth_ratio <- read_csv("D:/data-resource_2016_10_24_bangladesh-population-growth-ratio.csv")
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   Year = col_double(),
    ##   Population = col_double(),
    ##   Male = col_double(),
    ##   Female = col_double()
    ## )

``` r
View(data_resource_2016_10_24_bangladesh_population_growth_ratio)
View(data_resource_2016_10_24_bangladesh_population_growth_ratio)
View(data_resource_2016_10_24_bangladesh_population_growth_ratio)
str(data_resource_2016_10_24_bangladesh_population_growth_ratio)
```

    ## spec_tbl_df [46 x 4] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
    ##  $ Year      : num [1:46] 1971 1972 1973 1974 1975 ...
    ##  $ Population: num [1:46] 65733076 66997968 68118479 69248216 70542557 ...
    ##  $ Male      : num [1:46] 34058589 34713973 35294549 35879905 36550548 ...
    ##  $ Female    : num [1:46] 31674487 32283995 32823930 33368311 33992009 ...
    ##  - attr(*, "spec")=
    ##   .. cols(
    ##   ..   Year = col_double(),
    ##   ..   Population = col_double(),
    ##   ..   Male = col_double(),
    ##   ..   Female = col_double()
    ##   .. )

``` r
dim(data_resource_2016_10_24_bangladesh_population_growth_ratio)
```

    ## [1] 46  4

``` r
knitr::kable(head(data_resource_2016_10_24_bangladesh_population_growth_ratio))
```

| Year | Population |     Male |   Female |
|-----:|-----------:|---------:|---------:|
| 1971 |   65733076 | 34058589 | 31674487 |
| 1972 |   66997968 | 34713973 | 32283995 |
| 1973 |   68118479 | 35294549 | 32823930 |
| 1974 |   69248216 | 35879905 | 33368311 |
| 1975 |   70542557 | 36550548 | 33992009 |
| 1976 |   72088680 | 37351648 | 34737032 |

``` r
summary(data_resource_2016_10_24_bangladesh_population_growth_ratio)
```

    ##       Year        Population             Male              Female        
    ##  Min.   :1971   Min.   : 65733076   Min.   :34058589   Min.   :31674487  
    ##  1st Qu.:1982   1st Qu.: 85306609   1st Qu.:44200316   1st Qu.:41106293  
    ##  Median :1994   Median :113446470   Median :58780554   Median :54665916  
    ##  Mean   :1994   Mean   :113164372   Mean   :58634390   Mean   :54529982  
    ##  3rd Qu.:2005   3rd Qu.:141345657   3rd Qu.:73236092   3rd Qu.:68109565  
    ##  Max.   :2016   Max.   :161957017   Max.   :83915553   Max.   :78041464

``` r
#selection data
feature <- data_resource_2016_10_24_bangladesh_population_growth_ratio[,c("Population", "Female", "Male")]
head(feature)
```

    ## # A tibble: 6 x 3
    ##   Population   Female     Male
    ##        <dbl>    <dbl>    <dbl>
    ## 1   65733076 31674487 34058589
    ## 2   66997968 32283995 34713973
    ## 3   68118479 32823930 35294549
    ## 4   69248216 33368311 35879905
    ## 5   70542557 33992009 36550548
    ## 6   72088680 34737032 37351648

``` r
#transform data
df <- scale(feature)
print(df)
```

    ##        Population      Female        Male
    ##  [1,] -1.53594644 -1.53594646 -1.53594641
    ##  [2,] -1.49498600 -1.49498600 -1.49498601
    ##  [3,] -1.45870100 -1.45870102 -1.45870098
    ##  [4,] -1.42211723 -1.42211725 -1.42211721
    ##  [5,] -1.38020317 -1.38020319 -1.38020315
    ##  [6,] -1.33013576 -1.33013578 -1.33013574
    ##  [7,] -1.27182592 -1.27182592 -1.27182592
    ##  [8,] -1.20676659 -1.20676657 -1.20676660
    ##  [9,] -1.13724963 -1.13724961 -1.13724965
    ## [10,] -1.06575223 -1.06575220 -1.06575226
    ## [11,] -0.99357745 -0.99357745 -0.99357744
    ## [12,] -0.92065110 -0.92065111 -0.92065108
    ## [13,] -0.84646866 -0.84646863 -0.84646869
    ## [14,] -0.77051033 -0.77051031 -0.77051035
    ## [15,] -0.69224219 -0.69224218 -0.69224220
    ## [16,] -0.61142735 -0.61142734 -0.61142736
    ## [17,] -0.52830004 -0.52830006 -0.52830003
    ## [18,] -0.44363984 -0.44363983 -0.44363986
    ## [19,] -0.35861447 -0.35861449 -0.35861445
    ## [20,] -0.27436177 -0.27436177 -0.27436177
    ## [21,] -0.19163879 -0.19163879 -0.19163879
    ## [22,] -0.11061243 -0.11061246 -0.11061240
    ## [23,] -0.03074611 -0.03074612 -0.03074610
    ## [24,]  0.04901616  0.04901619  0.04901614
    ## [25,]  0.12973638  0.12973641  0.12973635
    ## [26,]  0.21188143  0.21188146  0.21188140
    ## [27,]  0.29520724  0.29520725  0.29520723
    ## [28,]  0.37911785  0.37911784  0.37911786
    ## [29,]  0.46286181  0.46286183  0.46286180
    ## [30,]  0.54561795  0.54561795  0.54561796
    ## [31,]  0.62705670  0.62705669  0.62705672
    ## [32,]  0.70716654  0.70716651  0.70716657
    ## [33,]  0.78527504  0.78527504  0.78527504
    ## [34,]  0.86000312  0.86000314  0.86000310
    ## [35,]  0.93010821  0.93010819  0.93010822
    ## [36,]  0.99479967  0.99479967  0.99479968
    ## [37,]  1.05410359  1.05410359  1.05410359
    ## [38,]  1.10936820  1.10936824  1.10936817
    ## [39,]  1.16301233  1.16301236  1.16301230
    ## [40,]  1.21748470  1.21748470  1.21748471
    ## [41,]  1.27415054  1.27415052  1.27415057
    ## [42,]  1.33309667  1.33309665  1.33309668
    ## [43,]  1.39384281  1.39384281  1.39384280
    ## [44,]  1.45569552  1.45569552  1.45569552
    ## [45,]  1.51784165  1.51784164  1.51784167
    ## [46,]  1.58003037  1.58003036  1.58003038
    ## attr(,"scaled:center")
    ## Population     Female       Male 
    ##  113164372   54529983   58634390 
    ## attr(,"scaled:scale")
    ## Population     Female       Male 
    ##   30880827   14880399   16000429

``` r
#k-means clustering model
set.seed(34000)
clus_gap <- clusGap(df, FUNcluster = kmeans, nstart = 25, K.max = 10, B = 10)
print(clus_gap)
```

    ## Clustering Gap statistic ["clusGap"] from call:
    ## clusGap(x = df, FUNcluster = kmeans, K.max = 10, B = 10, nstart = 25)
    ## B=10 simulated reference sets, k = 1..10; spaceH0="scaledPCA"
    ##  --> Number of clusters (method 'firstSEmax', SE.factor=1): 1
    ##            logW    E.logW         gap     SE.sim
    ##  [1,] 3.1226161 3.0087442 -0.11387187 0.08502250
    ##  [2,] 2.3644258 2.2669856 -0.09744027 0.09915281
    ##  [3,] 1.9288031 1.8049044 -0.12389864 0.05564919
    ##  [4,] 1.6464534 1.4737218 -0.17273155 0.07017347
    ##  [5,] 1.4178993 1.2280519 -0.18984742 0.06795018
    ##  [6,] 1.2266569 0.9870032 -0.23965368 0.09507043
    ##  [7,] 1.0605260 0.7769586 -0.28356743 0.08220824
    ##  [8,] 0.9250920 0.6023081 -0.32278393 0.08657099
    ##  [9,] 0.7972159 0.4541467 -0.34306918 0.07427036
    ## [10,] 0.6860652 0.3048150 -0.38125024 0.07728299

``` r
kmc_model <- kmeans(df, 4, nstart = 25)
print(kmc_model)
```

    ## K-means clustering with 4 clusters of sizes 13, 10, 13, 10
    ## 
    ## Cluster means:
    ##   Population     Female       Male
    ## 1  1.2218106  1.2218106  1.2218106
    ## 2  0.4192937  0.4192937  0.4192937
    ## 3 -1.2357216 -1.2357216 -1.2357216
    ## 4 -0.4012093 -0.4012093 -0.4012093
    ## 
    ## Clustering vector:
    ##  [1] 3 3 3 3 3 3 3 3 3 3 3 3 3 4 4 4 4 4 4 4 4 4 4 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1
    ## [39] 1 1 1 1 1 1 1 1
    ## 
    ## Within cluster sum of squares by cluster:
    ## [1] 1.881453 1.676445 1.865263 1.700224
    ##  (between_SS / total_SS =  94.7 %)
    ## 
    ## Available components:
    ## 
    ## [1] "cluster"      "centers"      "totss"        "withinss"     "tot.withinss"
    ## [6] "betweenss"    "size"         "iter"         "ifault"

``` r
#accuracy score
accuracy <- sum(diag(df)) / sum(df)
print(paste('Accuracy Score : ', accuracy))
```

    ## [1] "Accuracy Score :  -309803382433740"

``` r
#interpretation model
result <- prcomp(df)
result$rotation
```

    ##                  PC1         PC2        PC3
    ## Population 0.5773503 -0.01709382 -0.8163176
    ## Female     0.5773503  0.71549871  0.3933551
    ## Male       0.5773503 -0.69840489  0.4229625

``` r
#visualization
fviz_nbclust(df, kmeans, method = "wss")
```

![](Bangladesh-Population-Clustering_files/figure-gfm/unnamed-chunk-1-1.png)<!-- -->

``` r
fviz_nbclust(df, kmeans, method = "silhouette")
```

![](Bangladesh-Population-Clustering_files/figure-gfm/unnamed-chunk-1-2.png)<!-- -->

``` r
fviz_gap_stat(clus_gap)
```

![](Bangladesh-Population-Clustering_files/figure-gfm/unnamed-chunk-1-3.png)<!-- -->

``` r
fviz_cluster(kmc_model, data = df, main = "Bangladesh Population Clustering")
```

![](Bangladesh-Population-Clustering_files/figure-gfm/unnamed-chunk-1-4.png)<!-- -->

``` r
boxplot(df, data = feature, main = "Bangladesh Population")
```

![](Bangladesh-Population-Clustering_files/figure-gfm/unnamed-chunk-1-5.png)<!-- -->

``` r
plot(df, main = "Bangladesh Population")
```

![](Bangladesh-Population-Clustering_files/figure-gfm/unnamed-chunk-1-6.png)<!-- -->

``` r
clusplot(df, kmc_model$cluster, lines = 0, shade = TRUE, color = TRUE, labels = 2, plotchar = FALSE,
span = TRUE, main = paste('Clusters of Population Male & Female'), xlab = 'Population',
ylab = 'Year')
```

![](Bangladesh-Population-Clustering_files/figure-gfm/unnamed-chunk-1-7.png)<!-- -->

``` r
#testing k-value
k2 <- kmeans(df, centers = 2, nstart = 25)
k3 <- kmeans(df, centers = 3, nstart = 25)
k4 <- kmeans(df, centers = 4, nstart = 25)
k5 <- kmeans(df, centers = 5, nstart = 25)

p2 <- fviz_cluster(k2, geom = "point", data = df) + ggtitle("k = 2")
p3 <- fviz_cluster(k3, geom = "point", data = df) + ggtitle("k = 3")
p4 <- fviz_cluster(k4, geom = "point", data = df) + ggtitle("k = 4")
p5 <- fviz_cluster(k5, geom = "point", data = df) + ggtitle("k = 5")
grid.arrange(p2, p3, p4, p5, nrow = 2)
```

![](Bangladesh-Population-Clustering_files/figure-gfm/unnamed-chunk-1-8.png)<!-- -->
