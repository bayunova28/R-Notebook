---
title: "Uniqlo Stock Price Prediction"
author: Bayu Nova
date: July 27, 2021
output: github_document
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook.

```{r}
#import library
library(readr)
library(tseries)
library(astsa)
library(forecast)
library(caret)
library(ggplot2)

#data extraction
Uniqlo_FastRetailing_2012_2016_Training_stocks2012_2016 <- read_csv("D:/Uniqlo(FastRetailing) 2012-2016 Training - stocks2012-2016.csv")
View(Uniqlo_FastRetailing_2012_2016_Training_stocks2012_2016)
str(Uniqlo_FastRetailing_2012_2016_Training_stocks2012_2016)
dim(Uniqlo_FastRetailing_2012_2016_Training_stocks2012_2016)
knitr::kable(head(Uniqlo_FastRetailing_2012_2016_Training_stocks2012_2016))
summary(Uniqlo_FastRetailing_2012_2016_Training_stocks2012_2016)

#selection data
df <- Uniqlo_FastRetailing_2012_2016_Training_stocks2012_2016[,c("Date", "Close")]
head(df)

#arima model
ts_model <- arima.sim(list(order = c(1,1,2), ma = c(0.32, 0.47), ar = 0.8), n = 50) + 20
set.seed(34000)
training <- ts_model[1:59]
testing <- ts_model[1:66]
print(training)
print(testing)

ts_1 <- arima(training, order = c(0,1,2))
summary(ts_1)
ts_2 <- arima(training, order = c(1,1,0))
summary(ts_2)
ts_3 <- arima(training, order = c(1,1,2))

#prediction
forecast_1 <- predict(ts_1, 59)
print(forecast_1)
forecast_2 <- predict(ts_2, 59)
print(forecast_2)
forecast_3 <- predict(ts_3, 59)
print(forecast_3)

#cleaning data target model
count_close <- ts(df[, c('Close')])
df$clean_close <- tsclean(count_close)
summary(count_close)
summary(df$clean_close)

#moving avearge
df$close_ma <- ma(df$clean_close, order = 7)
df$close_ma_60 <- ma(df$clean_close, order = 59)
summary(df$close_ma)
summary(df$close_ma_60)

#visualization
ggplot(df, aes(Date, Close)) + geom_line() + scale_x_date('Date') + ylab("Close") +
xlab("")
ggplot() + geom_line(data = df, aes(x = Date, y = clean_close)) + ylab('Cleaned Close Price Count')
ggplot() + geom_line(data = df, aes(x = Date, y = clean_close, colour = "Counts")) + geom_line(data = df, aes(x = Date, y = close_ma,   colour = "Weekly Moving Average")) + geom_line(data = df, aes(x = Date, y = close_ma_60, colour = "Monthly Moving Average")) + ylab('High Price Count')

astsa::sarima(ts_model, p = 1, d = 0, q = 0)
plot(ts_model)
astsa::acf2(ts_model)

ma <- ts(na.omit(df$close_ma), frequency = 60)
decompose <- stl(ma, s.window = "periodic")
season_close <- seasadj(decompose)
plot(decompose)

#check closing target
ts_arima <- auto.arima(season_close, seasonal = FALSE)
summary(ts_arima)
count_season <- diff(season_close, differences = 1)
plot(count_season)
tsdisplay(residuals(ts_arima), lag.max= 10, main = 'Residuals Closing Model')

#testing moving average & season
adf.test(ma, alternative = "stationary")
adf.test(count_season, alternative = "stationary")

result <- forecast(ts_arima, h = 59)
summary(result)
plot(result)
```
