Uniqlo Stock Price Prediction
================
Bayu Nova
July 27, 2021

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook.

``` r
#import library
library(readr)
library(tseries)
```

    ## Registered S3 method overwritten by 'quantmod':
    ##   method            from
    ##   as.zoo.data.frame zoo

``` r
library(astsa)
library(forecast)
```

    ## 
    ## Attaching package: 'forecast'

    ## The following object is masked from 'package:astsa':
    ## 
    ##     gas

``` r
library(caret)
```

    ## Loading required package: lattice

    ## Loading required package: ggplot2

``` r
library(ggplot2)

#data extraction
Uniqlo_FastRetailing_2012_2016_Training_stocks2012_2016 <- read_csv("D:/Uniqlo(FastRetailing) 2012-2016 Training - stocks2012-2016.csv")
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   Date = col_date(format = ""),
    ##   Open = col_double(),
    ##   High = col_double(),
    ##   Low = col_double(),
    ##   Close = col_double(),
    ##   Volume = col_double(),
    ##   `Stock Trading` = col_double()
    ## )

``` r
View(Uniqlo_FastRetailing_2012_2016_Training_stocks2012_2016)
str(Uniqlo_FastRetailing_2012_2016_Training_stocks2012_2016)
```

    ## spec_tbl_df [1,226 x 7] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
    ##  $ Date         : Date[1:1226], format: "2016-12-30" "2016-12-29" ...
    ##  $ Open         : num [1:1226] 42120 43000 43940 43140 43310 ...
    ##  $ High         : num [1:1226] 42330 43220 43970 43700 43660 ...
    ##  $ Low          : num [1:1226] 41700 42540 43270 43140 43090 ...
    ##  $ Close        : num [1:1226] 41830 42660 43270 43620 43340 ...
    ##  $ Volume       : num [1:1226] 610000 448400 339900 400100 358200 ...
    ##  $ Stock Trading: num [1:1226] 2.56e+10 1.92e+10 1.48e+10 1.74e+10 1.55e+10 ...
    ##  - attr(*, "spec")=
    ##   .. cols(
    ##   ..   Date = col_date(format = ""),
    ##   ..   Open = col_double(),
    ##   ..   High = col_double(),
    ##   ..   Low = col_double(),
    ##   ..   Close = col_double(),
    ##   ..   Volume = col_double(),
    ##   ..   `Stock Trading` = col_double()
    ##   .. )

``` r
dim(Uniqlo_FastRetailing_2012_2016_Training_stocks2012_2016)
```

    ## [1] 1226    7

``` r
knitr::kable(head(Uniqlo_FastRetailing_2012_2016_Training_stocks2012_2016))
```

| Date       |  Open |  High |   Low | Close | Volume | Stock Trading |
|:-----------|------:|------:|------:|------:|-------:|--------------:|
| 2016-12-30 | 42120 | 42330 | 41700 | 41830 | 610000 |   25628028000 |
| 2016-12-29 | 43000 | 43220 | 42540 | 42660 | 448400 |   19188227000 |
| 2016-12-28 | 43940 | 43970 | 43270 | 43270 | 339900 |   14780670000 |
| 2016-12-27 | 43140 | 43700 | 43140 | 43620 | 400100 |   17427993000 |
| 2016-12-26 | 43310 | 43660 | 43090 | 43340 | 358200 |   15547803000 |
| 2016-12-22 | 43660 | 43840 | 43190 | 43480 | 381600 |   16586491000 |

``` r
summary(Uniqlo_FastRetailing_2012_2016_Training_stocks2012_2016)
```

    ##       Date                 Open            High            Low       
    ##  Min.   :2012-01-04   Min.   :13720   Min.   :13840   Min.   :13600  
    ##  1st Qu.:2013-04-01   1st Qu.:27789   1st Qu.:28091   1st Qu.:27401  
    ##  Median :2014-06-30   Median :34445   Median :34835   Median :33925  
    ##  Mean   :2014-06-30   Mean   :33754   Mean   :34179   Mean   :33348  
    ##  3rd Qu.:2015-09-29   3rd Qu.:41413   3rd Qu.:41900   3rd Qu.:40810  
    ##  Max.   :2016-12-30   Max.   :61550   Max.   :61970   Max.   :60740  
    ##      Close           Volume        Stock Trading      
    ##  Min.   :13720   Min.   : 139100   Min.   :3.966e+09  
    ##  1st Qu.:27675   1st Qu.: 487300   1st Qu.:1.454e+10  
    ##  Median :34413   Median : 626000   Median :2.154e+10  
    ##  Mean   :33761   Mean   : 727556   Mean   :2.441e+10  
    ##  3rd Qu.:41365   3rd Qu.: 826700   3rd Qu.:3.016e+10  
    ##  Max.   :61930   Max.   :4937300   Max.   :1.460e+11

``` r
#selection data
df <- Uniqlo_FastRetailing_2012_2016_Training_stocks2012_2016[,c("Date", "Close")]
head(df)
```

    ## # A tibble: 6 x 2
    ##   Date       Close
    ##   <date>     <dbl>
    ## 1 2016-12-30 41830
    ## 2 2016-12-29 42660
    ## 3 2016-12-28 43270
    ## 4 2016-12-27 43620
    ## 5 2016-12-26 43340
    ## 6 2016-12-22 43480

``` r
#arima model
ts_model <- arima.sim(list(order = c(1,1,2), ma = c(0.32, 0.47), ar = 0.8), n = 50) + 20
set.seed(34000)
training <- ts_model[1:59]
testing <- ts_model[1:66]
print(training)
```

    ##  [1]  20.000000  11.756381   3.938687  -3.956722 -10.219532 -14.303750
    ##  [7] -17.068424 -18.921017 -19.497965 -20.628964 -22.493138 -25.813917
    ## [13] -28.177008 -29.833962 -30.421745 -31.075496 -30.580235 -30.126043
    ## [19] -30.355727 -32.654670 -36.785614 -40.826962 -43.810744 -46.667412
    ## [25] -48.745632 -49.598567 -49.671801 -48.861645 -48.104556 -46.158899
    ## [31] -44.244829 -43.218669 -44.365342 -45.355263 -46.330688 -45.701084
    ## [37] -46.365851 -45.004325 -45.446592 -44.697714 -43.431980 -43.626867
    ## [43] -43.248412 -42.750881 -42.004686 -39.213167 -37.378855 -35.427115
    ## [49] -35.348138 -35.003771 -35.236453         NA         NA         NA
    ## [55]         NA         NA         NA         NA         NA

``` r
print(testing)
```

    ##  [1]  20.000000  11.756381   3.938687  -3.956722 -10.219532 -14.303750
    ##  [7] -17.068424 -18.921017 -19.497965 -20.628964 -22.493138 -25.813917
    ## [13] -28.177008 -29.833962 -30.421745 -31.075496 -30.580235 -30.126043
    ## [19] -30.355727 -32.654670 -36.785614 -40.826962 -43.810744 -46.667412
    ## [25] -48.745632 -49.598567 -49.671801 -48.861645 -48.104556 -46.158899
    ## [31] -44.244829 -43.218669 -44.365342 -45.355263 -46.330688 -45.701084
    ## [37] -46.365851 -45.004325 -45.446592 -44.697714 -43.431980 -43.626867
    ## [43] -43.248412 -42.750881 -42.004686 -39.213167 -37.378855 -35.427115
    ## [49] -35.348138 -35.003771 -35.236453         NA         NA         NA
    ## [55]         NA         NA         NA         NA         NA         NA
    ## [61]         NA         NA         NA         NA         NA         NA

``` r
ts_1 <- arima(training, order = c(0,1,2))
summary(ts_1)
```

    ## 
    ## Call:
    ## arima(x = training, order = c(0, 1, 2))
    ## 
    ## Coefficients:
    ##          ma1     ma2
    ##       0.8385  0.7507
    ## s.e.  0.1598  0.0965
    ## 
    ## sigma^2 estimated as 1.962:  log likelihood = -88.76,  aic = 183.52
    ## 
    ## Training set error measures:
    ##                      ME     RMSE      MAE      MPE     MAPE     MASE      ACF1
    ## Training set -0.4050299 1.387044 1.027214 0.561396 6.109976 0.538941 0.1990057

``` r
ts_2 <- arima(training, order = c(1,1,0))
summary(ts_2)
```

    ## 
    ## Call:
    ## arima(x = training, order = c(1, 1, 0))
    ## 
    ## Coefficients:
    ##          ar1
    ##       0.9588
    ## s.e.  0.0433
    ## 
    ## sigma^2 estimated as 1.279:  log likelihood = -78.35,  aic = 160.7
    ## 
    ## Training set error measures:
    ##                      ME     RMSE       MAE      MPE     MAPE      MASE
    ## Training set 0.06718094 1.119616 0.8946707 -0.84942 3.377155 0.4694004
    ##                    ACF1
    ## Training set 0.03260574

``` r
ts_3 <- arima(training, order = c(1,1,2))

#prediction
forecast_1 <- predict(ts_1, 59)
print(forecast_1)
```

    ## $pred
    ## Time Series:
    ## Start = 60 
    ## End = 118 
    ## Frequency = 1 
    ##  [1] -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716
    ##  [8] -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716
    ## [15] -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716
    ## [22] -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716
    ## [29] -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716
    ## [36] -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716
    ## [43] -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716
    ## [50] -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716 -35.97716
    ## [57] -35.97716 -35.97716 -35.97716
    ## 
    ## $se
    ## Time Series:
    ## Start = 60 
    ## End = 118 
    ## Frequency = 1 
    ##  [1] 10.03417 10.66959 11.26924 11.83855 12.38172 12.90203 13.40217 13.88429
    ##  [9] 14.35023 14.80151 15.23943 15.66512 16.07954 16.48354 16.87787 17.26320
    ## [17] 17.64012 18.00915 18.37076 18.72540 19.07344 19.41524 19.75113 20.08140
    ## [25] 20.40633 20.72616 21.04114 21.35146 21.65735 21.95897 22.25650 22.55011
    ## [33] 22.83994 23.12614 23.40885 23.68817 23.96425 24.23718 24.50707 24.77402
    ## [41] 25.03812 25.29947 25.55814 25.81422 26.06779 26.31892 26.56767 26.81411
    ## [49] 27.05831 27.30032 27.54021 27.77803 28.01382 28.24765 28.47956 28.70960
    ## [57] 28.93781 29.16423 29.38891

``` r
forecast_2 <- predict(ts_2, 59)
print(forecast_2)
```

    ## $pred
    ## Time Series:
    ## Start = 60 
    ## End = 118 
    ## Frequency = 1 
    ##  [1] -36.94369 -37.09652 -37.24307 -37.38359 -37.51832 -37.64750 -37.77137
    ##  [8] -37.89014 -38.00402 -38.11322 -38.21792 -38.31831 -38.41457 -38.50687
    ## [15] -38.59536 -38.68022 -38.76158 -38.83960 -38.91440 -38.98612 -39.05490
    ## [22] -39.12084 -39.18407 -39.24469 -39.30282 -39.35856 -39.41200 -39.46325
    ## [29] -39.51238 -39.55949 -39.60467 -39.64798 -39.68951 -39.72933 -39.76751
    ## [36] -39.80413 -39.83923 -39.87289 -39.90516 -39.93611 -39.96578 -39.99423
    ## [43] -40.02151 -40.04767 -40.07275 -40.09680 -40.11985 -40.14196 -40.16316
    ## [50] -40.18349 -40.20298 -40.22167 -40.23958 -40.25677 -40.27324 -40.28904
    ## [57] -40.30418 -40.31870 -40.33263
    ## 
    ## $se
    ## Time Series:
    ## Start = 60 
    ## End = 118 
    ## Frequency = 1 
    ##  [1]  16.87912  19.33335  21.84509  24.40554  27.00705  29.64294  32.30734
    ##  [8]  34.99507  37.70151  40.42258  43.15462  45.89438  48.63892  51.38564
    ## [15]  54.13218  56.87645  59.61655  62.35078  65.07762  67.79571  70.50383
    ## [22]  73.20088  75.88589  78.55799  81.21640  83.86044  86.48950  89.10305
    ## [29]  91.70063  94.28183  96.84631  99.39376 101.92393 104.43662 106.93165
    ## [36] 109.40888 111.86823 114.30960 116.73296 119.13828 121.52556 123.89482
    ## [43] 126.24610 128.57947 130.89498 133.19272 135.47279 137.73530 139.98036
    ## [50] 142.20811 144.41868 146.61221 148.78886 150.94877 153.09211 155.21904
    ## [57] 157.32973 159.42435 161.50308

``` r
forecast_3 <- predict(ts_3, 59)
print(forecast_3)
```

    ## $pred
    ## Time Series:
    ## Start = 60 
    ## End = 118 
    ## Frequency = 1 
    ##  [1] -35.17860 -35.18127 -35.18346 -35.18527 -35.18675 -35.18797 -35.18897
    ##  [8] -35.18979 -35.19047 -35.19103 -35.19149 -35.19186 -35.19217 -35.19242
    ## [15] -35.19263 -35.19280 -35.19295 -35.19306 -35.19316 -35.19324 -35.19330
    ## [22] -35.19335 -35.19340 -35.19343 -35.19346 -35.19349 -35.19351 -35.19352
    ## [29] -35.19354 -35.19355 -35.19356 -35.19356 -35.19357 -35.19357 -35.19358
    ## [36] -35.19358 -35.19358 -35.19359 -35.19359 -35.19359 -35.19359 -35.19359
    ## [43] -35.19359 -35.19359 -35.19359 -35.19360 -35.19360 -35.19360 -35.19360
    ## [50] -35.19360 -35.19360 -35.19360 -35.19360 -35.19360 -35.19360 -35.19360
    ## [57] -35.19360 -35.19360 -35.19360
    ## 
    ## $se
    ## Time Series:
    ## Start = 60 
    ## End = 118 
    ## Frequency = 1 
    ##  [1] 18.09929 20.21999 22.26989 24.24865 26.15754 27.99882 29.77536 31.49036
    ##  [9] 33.14717 34.74914 36.29955 37.80157 39.25820 40.67230 42.04653 43.38335
    ## [17] 44.68509 45.95389 47.19173 48.40043 49.58170 50.73711 51.86809 52.97598
    ## [25] 54.06202 55.12736 56.17305 57.20007 58.20934 59.20169 60.17792 61.13875
    ## [33] 62.08486 63.01690 63.93546 64.84108 65.73430 66.61559 67.48542 68.34422
    ## [41] 69.19239 70.03031 70.85834 71.67682 72.48607 73.28639 74.07808 74.86141
    ## [49] 75.63662 76.40398 77.16371 77.91603 78.66117 79.39931 80.13065 80.85538
    ## [57] 81.57368 82.28570 82.99162

``` r
#cleaning data target model
count_close <- ts(df[, c('Close')])
df$clean_close <- tsclean(count_close)
summary(count_close)
```

    ##      Close      
    ##  Min.   :13720  
    ##  1st Qu.:27675  
    ##  Median :34413  
    ##  Mean   :33761  
    ##  3rd Qu.:41365  
    ##  Max.   :61930

``` r
summary(df$clean_close)
```

    ##      Close      
    ##  Min.   :13720  
    ##  1st Qu.:27675  
    ##  Median :34413  
    ##  Mean   :33757  
    ##  3rd Qu.:41365  
    ##  Max.   :60740

``` r
#moving avearge
df$close_ma <- ma(df$clean_close, order = 7)
df$close_ma_60 <- ma(df$clean_close, order = 59)
summary(df$close_ma)
```

    ##        V1       
    ##  Min.   :14234  
    ##  1st Qu.:28218  
    ##  Median :34346  
    ##  Mean   :33784  
    ##  3rd Qu.:40924  
    ##  Max.   :59914  
    ##  NA's   :6

``` r
summary(df$close_ma_60)
```

    ##        V1       
    ##  Min.   :16111  
    ##  1st Qu.:29330  
    ##  Median :34326  
    ##  Mean   :34041  
    ##  3rd Qu.:39612  
    ##  Max.   :55337  
    ##  NA's   :58

``` r
#visualization
ggplot(df, aes(Date, Close)) + geom_line() + scale_x_date('Date') + ylab("Close") +
xlab("")
```

![](Uniqlo-Stock-Price-Prediction_files/figure-gfm/unnamed-chunk-1-1.png)<!-- -->

``` r
ggplot() + geom_line(data = df, aes(x = Date, y = clean_close)) + ylab('Cleaned Close Price Count')
```

    ## Don't know how to automatically pick scale for object of type ts. Defaulting to continuous.

![](Uniqlo-Stock-Price-Prediction_files/figure-gfm/unnamed-chunk-1-2.png)<!-- -->

``` r
ggplot() + geom_line(data = df, aes(x = Date, y = clean_close, colour = "Counts")) + geom_line(data = df, aes(x = Date, y = close_ma,   colour = "Weekly Moving Average")) + geom_line(data = df, aes(x = Date, y = close_ma_60, colour = "Monthly Moving Average")) + ylab('High Price Count')
```

    ## Don't know how to automatically pick scale for object of type ts. Defaulting to continuous.

    ## Warning: Removed 6 row(s) containing missing values (geom_path).

    ## Warning: Removed 58 row(s) containing missing values (geom_path).

![](Uniqlo-Stock-Price-Prediction_files/figure-gfm/unnamed-chunk-1-3.png)<!-- -->

``` r
astsa::sarima(ts_model, p = 1, d = 0, q = 0)
```

    ## initial  value 2.645397 
    ## iter   2 value 1.508062
    ## iter   3 value 0.805133
    ## iter   4 value 0.641566
    ## iter   5 value 0.526400
    ## iter   6 value 0.435516
    ## iter   7 value 0.422100
    ## iter   8 value 0.417180
    ## iter   9 value 0.417062
    ## iter  10 value 0.415576
    ## iter  11 value 0.415554
    ## iter  12 value 0.415554
    ## iter  12 value 0.415554
    ## iter  12 value 0.415554
    ## final  value 0.415554 
    ## converged
    ## initial  value 1.515327 
    ## iter   2 value 1.132275
    ## iter   3 value 1.094168
    ## iter   4 value 1.070359
    ## iter   5 value 1.062632
    ## iter   6 value 1.059978
    ## iter   7 value 1.059881
    ## iter   8 value 1.058111
    ## iter   9 value 1.057940
    ## iter  10 value 1.057845
    ## iter  11 value 1.057725
    ## iter  12 value 1.057677
    ## iter  13 value 1.057075
    ## iter  14 value 1.057002
    ## iter  15 value 1.056996
    ## iter  16 value 1.056994
    ## iter  17 value 1.056993
    ## iter  18 value 1.056964
    ## iter  19 value 1.056963
    ## iter  20 value 1.056963
    ## iter  21 value 1.056962
    ## iter  22 value 1.056962
    ## iter  23 value 1.056962
    ## iter  23 value 1.056962
    ## iter  23 value 1.056962
    ## final  value 1.056962 
    ## converged

![](Uniqlo-Stock-Price-Prediction_files/figure-gfm/unnamed-chunk-1-4.png)<!-- -->

    ## $fit
    ## 
    ## Call:
    ## arima(x = xdata, order = c(p, d, q), seasonal = list(order = c(P, D, Q), period = S), 
    ##     xreg = xmean, include.mean = FALSE, transform.pars = trans, fixed = fixed, 
    ##     optim.control = list(trace = trc, REPORT = 1, reltol = tol))
    ## 
    ## Coefficients:
    ##          ar1     xmean
    ##       0.9951  -10.4544
    ## s.e.  0.0063   26.4341
    ## 
    ## sigma^2 estimated as 7.563:  log likelihood = -126.27,  aic = 258.54
    ## 
    ## $degrees_of_freedom
    ## [1] 49
    ## 
    ## $ttable
    ##       Estimate      SE  t.value p.value
    ## ar1     0.9951  0.0063 157.3446  0.0000
    ## xmean -10.4544 26.4341  -0.3955  0.6942
    ## 
    ## $AIC
    ## [1] 5.069448
    ## 
    ## $AICc
    ## [1] 5.07435
    ## 
    ## $BIC
    ## [1] 5.183085

``` r
plot(ts_model)
```

![](Uniqlo-Stock-Price-Prediction_files/figure-gfm/unnamed-chunk-1-5.png)<!-- -->

``` r
astsa::acf2(ts_model)
```

![](Uniqlo-Stock-Price-Prediction_files/figure-gfm/unnamed-chunk-1-6.png)<!-- -->

    ##      [,1]  [,2]  [,3] [,4] [,5] [,6] [,7]  [,8]
    ## ACF  0.87  0.76  0.65 0.57 0.51 0.45  0.4  0.36
    ## PACF 0.87 -0.03 -0.01 0.03 0.03 0.01  0.0 -0.01

``` r
ma <- ts(na.omit(df$close_ma), frequency = 60)
decompose <- stl(ma, s.window = "periodic")
season_close <- seasadj(decompose)
plot(decompose)
```

![](Uniqlo-Stock-Price-Prediction_files/figure-gfm/unnamed-chunk-1-7.png)<!-- -->

``` r
#check closing target
ts_arima <- auto.arima(season_close, seasonal = FALSE)
summary(ts_arima)
```

    ## Series: season_close 
    ## ARIMA(2,1,3) with drift 
    ## 
    ## Coefficients:
    ##          ar1      ar2      ma1     ma2     ma3     drift
    ##       1.3554  -0.6068  -0.7112  0.3152  0.4948  -23.7199
    ## s.e.  0.0299   0.0265   0.0234  0.0287  0.0285   18.2811
    ## 
    ## sigma^2 estimated as 21493:  log likelihood=-7809.02
    ## AIC=15632.05   AICc=15632.14   BIC=15667.79
    ## 
    ## Training set error measures:
    ##                       ME     RMSE      MAE           MPE      MAPE       MASE
    ## Training set -0.06027241 146.1827 105.3258 -0.0008422756 0.3173359 0.02161184
    ##                    ACF1
    ## Training set 0.09035797

``` r
count_season <- diff(season_close, differences = 1)
plot(count_season)
```

![](Uniqlo-Stock-Price-Prediction_files/figure-gfm/unnamed-chunk-1-8.png)<!-- -->

``` r
tsdisplay(residuals(ts_arima), lag.max= 10, main = 'Residuals Closing Model')
```

![](Uniqlo-Stock-Price-Prediction_files/figure-gfm/unnamed-chunk-1-9.png)<!-- -->

``` r
#testing moving average & season
adf.test(ma, alternative = "stationary")
```

    ## 
    ##  Augmented Dickey-Fuller Test
    ## 
    ## data:  ma
    ## Dickey-Fuller = -1.8226, Lag order = 10, p-value = 0.6534
    ## alternative hypothesis: stationary

``` r
adf.test(count_season, alternative = "stationary")
```

    ## Warning in adf.test(count_season, alternative = "stationary"): p-value smaller
    ## than printed p-value

    ## 
    ##  Augmented Dickey-Fuller Test
    ## 
    ## data:  count_season
    ## Dickey-Fuller = -8.5715, Lag order = 10, p-value = 0.01
    ## alternative hypothesis: stationary

``` r
result <- forecast(ts_arima, h = 59)
summary(result)
```

    ## 
    ## Forecast method: ARIMA(2,1,3) with drift
    ## 
    ## Model Information:
    ## Series: season_close 
    ## ARIMA(2,1,3) with drift 
    ## 
    ## Coefficients:
    ##          ar1      ar2      ma1     ma2     ma3     drift
    ##       1.3554  -0.6068  -0.7112  0.3152  0.4948  -23.7199
    ## s.e.  0.0299   0.0265   0.0234  0.0287  0.0285   18.2811
    ## 
    ## sigma^2 estimated as 21493:  log likelihood=-7809.02
    ## AIC=15632.05   AICc=15632.14   BIC=15667.79
    ## 
    ## Error measures:
    ##                       ME     RMSE      MAE           MPE      MAPE       MASE
    ## Training set -0.06027241 146.1827 105.3258 -0.0008422756 0.3173359 0.02161184
    ##                    ACF1
    ## Training set 0.09035797
    ## 
    ## Forecasts:
    ##          Point Forecast     Lo 80    Hi 80     Lo 95    Hi 95
    ## 21.33333       14076.56 13888.675 14264.44 13789.217 14363.89
    ## 21.35000       13923.11 13561.550 14284.67 13370.151 14476.07
    ## 21.36667       13782.04 13229.232 14334.85 12936.591 14627.49
    ## 21.38333       13677.98 12872.522 14483.44 12446.138 14909.83
    ## 21.40000       13616.57 12518.242 14714.90 11936.821 15296.32
    ## 21.41667       13590.52 12193.652 14987.38 11454.197 15726.83
    ## 21.43333       13586.50 11912.138 15260.86 11025.784 16147.21
    ## 21.45000       13590.90 11673.640 15508.17 10658.702 16523.11
    ## 21.46667       13593.35 11470.112 15716.59 10346.138 16840.56
    ## 21.48333       13588.03 11291.108 15884.95 10075.192 17100.86
    ## 21.50000       13573.37 11127.504 16019.23  9832.741 17314.00
    ## 21.51667       13550.77 10973.009 16128.52  9608.426 17493.11
    ## 21.53333       13523.06 10824.097 16222.03  9395.350 17650.78
    ## 21.55000       13493.27 10679.223 16307.31  9189.559 17796.97
    ## 21.56667       13463.73 10537.969 16389.48  8989.166 17938.29
    ## 21.58333       13435.80 10400.393 16471.22  8793.543 18078.07
    ## 21.60000       13409.92 10266.638 16553.21  8602.683 18217.16
    ## 21.61667       13385.82 10136.726 16634.91  8416.760 18354.88
    ## 21.63333       13362.89 10010.505 16715.28  8235.856 18489.93
    ## 21.65000       13340.48  9887.670 16793.30  8059.861 18621.11
    ## 21.66667       13318.05  9767.847 16868.26  7888.481 18747.63
    ## 21.68333       13295.29  9650.665 16939.92  7721.316 18869.27
    ## 21.70000       13272.08  9535.808 17008.36  7557.943 18986.23
    ## 21.71667       13248.48  9423.042 17073.92  7397.977 19098.98
    ## 21.73333       13224.60  9312.201 17137.01  7241.100 19208.11
    ## 21.75000       13200.60  9203.176 17198.03  7087.066 19314.14
    ## 21.76667       13176.60  9095.887 17257.31  6935.690 19417.51
    ## 21.78333       13152.66  8990.264 17315.06  6786.824 19518.50
    ## 21.80000       13128.82  8886.234 17371.40  6640.346 19617.29
    ## 21.81667       13105.07  8783.723 17426.41  6496.142 19713.99
    ## 21.83333       13081.38  8682.650 17480.10  6354.104 19808.65
    ## 21.85000       13057.72  8582.933 17532.50  6214.125 19901.31
    ## 21.86667       13034.06  8484.496 17583.63  6076.101 19992.02
    ## 21.88333       13010.39  8387.267 17633.51  5939.933 20080.85
    ## 21.90000       12986.70  8291.184 17682.21  5805.529 20167.87
    ## 21.91667       12962.99  8196.193 17729.78  5672.804 20253.17
    ## 21.93333       12939.26  8102.245 17776.28  5541.683 20336.84
    ## 21.95000       12915.53  8009.298 17821.76  5412.095 20418.96
    ## 21.96667       12891.80  7917.311 17866.28  5283.978 20499.61
    ## 21.98333       12868.06  7826.249 17909.88  5157.273 20578.86
    ## 22.00000       12844.34  7736.076 17952.60  5031.925 20656.75
    ## 22.01667       12820.62  7646.758 17994.47  4907.882 20733.35
    ## 22.03333       12796.90  7558.262 18035.53  4785.097 20808.70
    ## 22.05000       12773.18  7470.560 18075.80  4663.522 20882.84
    ## 22.06667       12749.46  7383.620 18115.31  4543.115 20955.81
    ## 22.08333       12725.75  7297.417 18154.07  4423.834 21027.66
    ## 22.10000       12702.03  7211.925 18192.13  4305.640 21098.41
    ## 22.11667       12678.31  7127.119 18229.50  4188.498 21168.12
    ## 22.13333       12654.59  7042.978 18266.20  4072.372 21236.80
    ## 22.15000       12630.87  6959.481 18302.25  3957.230 21304.50
    ## 22.16667       12607.15  6876.607 18337.69  3843.042 21371.25
    ## 22.18333       12583.43  6794.337 18372.52  3729.778 21437.07
    ## 22.20000       12559.71  6712.654 18406.76  3617.411 21502.00
    ## 22.21667       12535.99  6631.539 18440.43  3505.914 21566.06
    ## 22.23333       12512.27  6550.978 18473.55  3395.262 21629.27
    ## 22.25000       12488.55  6470.953 18506.14  3285.432 21691.66
    ## 22.26667       12464.83  6391.450 18538.20  3176.399 21753.25
    ## 22.28333       12441.11  6312.455 18569.76  3068.143 21814.07
    ## 22.30000       12417.39  6233.955 18600.82  2960.643 21874.13

``` r
plot(result)
```

![](Uniqlo-Stock-Price-Prediction_files/figure-gfm/unnamed-chunk-1-10.png)<!-- -->
