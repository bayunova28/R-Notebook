Netflix Stock Price
================
Bayu Nova
July 23, 2021

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook.

``` r
#import library
library(readr)
library(tseries)
```

    ## Registered S3 method overwritten by 'quantmod':
    ##   method            from
    ##   as.zoo.data.frame zoo

``` r
library(astsa)
library(forecast)
```

    ## 
    ## Attaching package: 'forecast'

    ## The following object is masked from 'package:astsa':
    ## 
    ##     gas

``` r
library(DMwR2)
```

    ## 
    ## Attaching package: 'DMwR2'

    ## The following object is masked from 'package:astsa':
    ## 
    ##     sales

``` r
library(ggplot2)

#data extraction
NFLX <- read_csv("D:/NFLX.csv")
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   Date = col_date(format = ""),
    ##   Open = col_double(),
    ##   High = col_double(),
    ##   Low = col_double(),
    ##   Close = col_double(),
    ##   `Adj Close` = col_double(),
    ##   Volume = col_double()
    ## )

``` r
View(NFLX)
str(NFLX)
```

    ## spec_tbl_df [1,007 x 7] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
    ##  $ Date     : Date[1:1007], format: "2015-12-16" "2015-12-17" ...
    ##  $ Open     : num [1:1007] 120 124 121 120 117 ...
    ##  $ High     : num [1:1007] 123 126 122 120 117 ...
    ##  $ Low      : num [1:1007] 118 122 118 116 115 ...
    ##  $ Close    : num [1:1007] 123 123 118 117 116 ...
    ##  $ Adj Close: num [1:1007] 123 123 118 117 116 ...
    ##  $ Volume   : num [1:1007] 13181000 17284900 17948100 11670000 9689000 ...
    ##  - attr(*, "spec")=
    ##   .. cols(
    ##   ..   Date = col_date(format = ""),
    ##   ..   Open = col_double(),
    ##   ..   High = col_double(),
    ##   ..   Low = col_double(),
    ##   ..   Close = col_double(),
    ##   ..   `Adj Close` = col_double(),
    ##   ..   Volume = col_double()
    ##   .. )

``` r
dim(NFLX)
```

    ## [1] 1007    7

``` r
knitr::kable(head(NFLX))
```

| Date       |   Open |   High |    Low |  Close | Adj Close |   Volume |
|:-----------|-------:|-------:|-------:|-------:|----------:|---------:|
| 2015-12-16 | 119.80 | 123.00 | 118.09 | 122.64 |    122.64 | 13181000 |
| 2015-12-17 | 123.97 | 126.35 | 122.42 | 122.51 |    122.51 | 17284900 |
| 2015-12-18 | 120.85 | 122.19 | 117.92 | 118.02 |    118.02 | 17948100 |
| 2015-12-21 | 119.51 | 119.59 | 115.66 | 116.63 |    116.63 | 11670000 |
| 2015-12-22 | 117.30 | 117.43 | 114.86 | 116.24 |    116.24 |  9689000 |
| 2015-12-23 | 116.91 | 118.56 | 115.78 | 118.16 |    118.16 |  9324300 |

``` r
summary(NFLX)
```

    ##       Date                 Open             High            Low        
    ##  Min.   :2015-12-16   Min.   : 80.57   Min.   : 84.7   Min.   : 79.95  
    ##  1st Qu.:2016-12-14   1st Qu.:125.78   1st Qu.:127.2   1st Qu.:124.36  
    ##  Median :2017-12-14   Median :197.85   Median :199.4   Median :195.33  
    ##  Mean   :2017-12-16   Mean   :226.71   Mean   :230.0   Mean   :223.11  
    ##  3rd Qu.:2018-12-15   3rd Qu.:325.53   3rd Qu.:330.8   3rd Qu.:318.87  
    ##  Max.   :2019-12-16   Max.   :421.38   Max.   :423.2   Max.   :413.08  
    ##      Close          Adj Close          Volume        
    ##  Min.   : 82.79   Min.   : 82.79   Min.   : 1616300  
    ##  1st Qu.:125.52   1st Qu.:125.52   1st Qu.: 5573200  
    ##  Median :198.00   Median :198.00   Median : 7857600  
    ##  Mean   :226.67   Mean   :226.67   Mean   : 9730586  
    ##  3rd Qu.:325.22   3rd Qu.:325.22   3rd Qu.:11786300  
    ##  Max.   :418.97   Max.   :418.97   Max.   :58410400

``` r
#selection data target
df <- NFLX[,c("Date", "Open", "High", "Low", "Close", "Volume")]
head(df)
```

    ## # A tibble: 6 x 6
    ##   Date        Open  High   Low Close   Volume
    ##   <date>     <dbl> <dbl> <dbl> <dbl>    <dbl>
    ## 1 2015-12-16  120.  123   118.  123. 13181000
    ## 2 2015-12-17  124.  126.  122.  123. 17284900
    ## 3 2015-12-18  121.  122.  118.  118. 17948100
    ## 4 2015-12-21  120.  120.  116.  117. 11670000
    ## 5 2015-12-22  117.  117.  115.  116.  9689000
    ## 6 2015-12-23  117.  119.  116.  118.  9324300

``` r
time_series <- arima.sim(list(order = c(1,1,2), ma = c(0.32, 0.47), ar = 0.8), n = 50)+20

#training & testing data target
set.seed(34000)
training <- time_series[1:40]
testing <- time_series[41:50]
print(training)
```

    ##  [1] 20.00000 20.37372 19.85426 19.97250 19.73035 21.23645 22.09644 24.59580
    ##  [9] 27.16901 29.81936 32.11246 35.34665 38.88764 42.47876 47.28453 50.72251
    ## [17] 53.19106 54.77533 56.54620 58.06515 60.47868 62.54992 66.18865 67.82031
    ## [25] 68.14498 66.61276 65.34469 64.59793 63.79665 63.42227 62.72299 61.65339
    ## [33] 60.45351 59.85463 60.64425 63.09044 65.18550 69.21226 73.28216 76.23628

``` r
print(testing)
```

    ##  [1] 78.45051 78.54318 78.03075 77.09079 76.56212 77.04474 78.39084 81.49898
    ##  [9] 84.64258 88.50891

``` r
#arima model
a1 <- arima(training, order = c(0,1,2))
a2 <- arima(training, order = c(1,1,0))
a3 <- arima(training, order = c(1,1,2))
summary(a1)
```

    ## 
    ## Call:
    ## arima(x = training, order = c(0, 1, 2))
    ## 
    ## Coefficients:
    ##          ma1     ma2
    ##       1.0061  0.7206
    ## s.e.  0.1553  0.1131
    ## 
    ## sigma^2 estimated as 1.254:  log likelihood = -60.69,  aic = 127.39
    ## 
    ## Training set error measures:
    ##                     ME     RMSE       MAE      MPE     MAPE      MASE
    ## Training set 0.5179043 1.105722 0.8250512 1.259826 1.975335 0.4328344
    ##                    ACF1
    ## Training set 0.05119103

``` r
summary(a2)
```

    ## 
    ## Call:
    ## arima(x = training, order = c(1, 1, 0))
    ## 
    ## Coefficients:
    ##          ar1
    ##       0.9025
    ## s.e.  0.0622
    ## 
    ## sigma^2 estimated as 0.9313:  log likelihood = -54.79,  aic = 113.59
    ## 
    ## Training set error measures:
    ##                     ME      RMSE       MAE       MPE     MAPE      MASE
    ## Training set 0.1989689 0.9528996 0.7381946 0.5796885 1.727058 0.3872682
    ##                     ACF1
    ## Training set 0.002050514

``` r
summary(a3)
```

    ## 
    ## Call:
    ## arima(x = training, order = c(1, 1, 2))
    ## 
    ## Coefficients:
    ##          ar1     ma1     ma2
    ##       0.8238  0.1254  0.3441
    ## s.e.  0.1028  0.1975  0.1444
    ## 
    ## sigma^2 estimated as 0.8283:  log likelihood = -52.65,  aic = 113.3
    ## 
    ## Training set error measures:
    ##                     ME    RMSE       MAE       MPE     MAPE      MASE
    ## Training set 0.2025324 0.89864 0.7194769 0.5740686 1.631903 0.3774485
    ##                     ACF1
    ## Training set -0.04007112

``` r
fc1 <- predict(a1, 10)
fc2 <- predict(a2, 10)
fc3 <- predict(a3, 10)
print(fc1)
```

    ## $pred
    ## Time Series:
    ## Start = 41 
    ## End = 50 
    ## Frequency = 1 
    ##  [1] 76.72370 76.50159 76.50159 76.50159 76.50159 76.50159 76.50159 76.50159
    ##  [9] 76.50159 76.50159
    ## 
    ## $se
    ## Time Series:
    ## Start = 41 
    ## End = 50 
    ## Frequency = 1 
    ##  [1] 1.119804 2.510085 3.952667 4.994659 5.854030 6.602478 7.274322 7.889158
    ##  [9] 8.459424 8.993604

``` r
print(fc2)
```

    ## $pred
    ## Time Series:
    ## Start = 41 
    ## End = 50 
    ## Frequency = 1 
    ##  [1] 78.90224 81.30816 83.47939 85.43882 87.20713 88.80294 90.24309 91.54276
    ##  [9] 92.71566 93.77414
    ## 
    ## $se
    ## Time Series:
    ## Start = 41 
    ## End = 50 
    ## Frequency = 1 
    ##  [1]  0.9650336  2.0741119  3.3430823  4.7194119  6.1679613  7.6639669
    ##  [7]  9.1893191 10.7304605 12.2770938 13.8213351

``` r
print(fc3)
```

    ## $pred
    ## Time Series:
    ## Start = 41 
    ## End = 50 
    ## Frequency = 1 
    ##  [1] 78.76956 80.46701 81.86532 83.01721 83.96611 84.74779 85.39171 85.92216
    ##  [9] 86.35913 86.71909
    ## 
    ## $se
    ## Time Series:
    ## Start = 41 
    ## End = 50 
    ## Frequency = 1 
    ##  [1]  0.9100824  1.9937395  3.4362134  5.0077826  6.6255811  8.2472621
    ##  [7]  9.8485628 11.4152257 12.9390789 14.4158209

``` r
#cleaning data target model
count_high <- ts(df[, c('High')])
df$clean_high <- tsclean(count_high)

#moving avearge
df$high_ma <- ma(df$clean_high, order = 7)
df$high_ma_30 <- ma(df$clean_high, order = 30)

#visualization
ggplot(df, aes(Date, Open)) + geom_line() + scale_x_date('Date') + ylab("Open") +
xlab("")
```

![](Netflix-Stock-Price_files/figure-gfm/unnamed-chunk-1-1.png)<!-- -->

``` r
ggplot() + geom_line(data = df, aes(x = Date, y = clean_high)) + ylab('Cleaned High Price Count')
```

    ## Don't know how to automatically pick scale for object of type ts. Defaulting to continuous.

![](Netflix-Stock-Price_files/figure-gfm/unnamed-chunk-1-2.png)<!-- -->

``` r
ggplot() + geom_line(data = df, aes(x = Date, y = clean_high, colour = "Counts")) + geom_line(data = df, aes(x = Date, y = high_ma,   colour = "Weekly Moving Average")) + geom_line(data = df, aes(x = Date, y = high_ma_30, colour = "Monthly Moving Average")) + ylab('High Price Count')
```

    ## Don't know how to automatically pick scale for object of type ts. Defaulting to continuous.

    ## Warning: Removed 6 row(s) containing missing values (geom_path).

    ## Warning: Removed 30 row(s) containing missing values (geom_path).

![](Netflix-Stock-Price_files/figure-gfm/unnamed-chunk-1-3.png)<!-- -->

``` r
moving_average <- ts(na.omit(df$high_ma), frequency = 30)
decompose <- stl(moving_average, s.window = "periodic")
season_high <- seasadj(decompose)
plot(decompose)
```

![](Netflix-Stock-Price_files/figure-gfm/unnamed-chunk-1-4.png)<!-- -->

``` r
arima <- auto.arima(season_high, seasonal = FALSE)
summary(arima)
```

    ## Series: season_high 
    ## ARIMA(5,1,2) 
    ## 
    ## Coefficients:
    ##           ar1     ar2     ar3      ar4      ar5     ma1     ma2
    ##       -0.5793  0.7773  0.7179  -0.2172  -0.3152  1.7619  0.9675
    ## s.e.   0.0318  0.0349  0.0360   0.0346   0.0314  0.0153  0.0123
    ## 
    ## sigma^2 estimated as 0.8453:  log likelihood=-1334.13
    ## AIC=2684.25   AICc=2684.4   BIC=2723.52
    ## 
    ## Training set error measures:
    ##                     ME      RMSE       MAE        MPE      MAPE       MASE
    ## Training set 0.0307334 0.9157336 0.6240006 0.01794954 0.2807389 0.02697162
    ##                      ACF1
    ## Training set -0.008061861

``` r
tsdisplay(residuals(arima), lag.max=10, main='Residuals Model')
```

![](Netflix-Stock-Price_files/figure-gfm/unnamed-chunk-1-5.png)<!-- -->

``` r
count_season <- diff(season_high, differences = 1)
plot(count_season)
```

![](Netflix-Stock-Price_files/figure-gfm/unnamed-chunk-1-6.png)<!-- -->

``` r
#testing moving average
adf.test(moving_average, alternative = "stationary")
```

    ## 
    ##  Augmented Dickey-Fuller Test
    ## 
    ## data:  moving_average
    ## Dickey-Fuller = -1.9585, Lag order = 9, p-value = 0.5959
    ## alternative hypothesis: stationary

``` r
adf.test(count_season, alternative = "stationary")
```

    ## Warning in adf.test(count_season, alternative = "stationary"): p-value smaller
    ## than printed p-value

    ## 
    ##  Augmented Dickey-Fuller Test
    ## 
    ## data:  count_season
    ## Dickey-Fuller = -6.7562, Lag order = 9, p-value = 0.01
    ## alternative hypothesis: stationary

``` r
#prediction
prediction <- forecast(arima, h = 30)
print(prediction)
```

    ##          Point Forecast    Lo 80    Hi 80    Lo 95    Hi 95
    ## 34.36667       304.3761 303.1978 305.5544 302.5741 306.1781
    ## 34.40000       304.4895 301.6607 307.3183 300.1632 308.8158
    ## 34.43333       305.3868 300.6331 310.1405 298.1166 312.6569
    ## 34.46667       305.7183 298.8002 312.6363 295.1380 316.2985
    ## 34.50000       306.2248 297.0393 315.4103 292.1767 320.2729
    ## 34.53333       306.6117 295.2931 317.9303 289.3014 323.9220
    ## 34.56667       306.7887 293.3590 320.2184 286.2497 327.3276
    ## 34.60000       306.9956 291.6670 322.3243 283.5524 330.4389
    ## 34.63333       307.0765 289.9748 324.1783 280.9217 333.2314
    ## 34.66667       307.0740 288.3620 325.7859 278.4565 335.6915
    ## 34.70000       307.1266 286.9524 327.3007 276.2729 337.9802
    ## 34.73333       307.0514 285.5319 328.5709 274.1402 339.9626
    ## 34.76667       307.0512 284.2974 329.8049 272.2522 341.8501
    ## 34.80000       307.0057 283.1073 330.9041 270.4562 343.5552
    ## 34.83333       306.9673 281.9926 331.9420 268.7718 345.1628
    ## 34.86667       306.9538 280.9684 332.9392 267.2126 346.6950
    ## 34.90000       306.9229 279.9706 333.8751 265.7029 348.1428
    ## 34.93333       306.9126 279.0363 334.7889 264.2795 349.5458
    ## 34.96667       306.9075 278.1398 335.6752 262.9111 350.9039
    ## 35.00000       306.8954 277.2640 336.5268 261.5780 352.2127
    ## 35.03333       306.9020 276.4325 337.3716 260.3029 353.5012
    ## 35.06667       306.8970 275.6104 338.1837 259.0482 354.7459
    ## 35.10000       306.9007 274.8168 338.9847 257.8326 355.9689
    ## 35.13333       306.9037 274.0411 339.7663 256.6447 357.1628
    ## 35.16667       306.9037 273.2787 340.5286 255.4787 358.3286
    ## 35.20000       306.9077 272.5370 341.2783 254.3423 359.4731
    ## 35.23333       306.9082 271.8067 342.0098 253.2250 360.5914
    ## 35.26667       306.9092 271.0913 342.7270 252.1305 361.6879
    ## 35.30000       306.9110 270.3906 343.4313 251.0579 362.7641
    ## 35.33333       306.9102 269.7003 344.1201 250.0026 363.8178

``` r
plot(prediction)
```

![](Netflix-Stock-Price_files/figure-gfm/unnamed-chunk-1-7.png)<!-- -->
