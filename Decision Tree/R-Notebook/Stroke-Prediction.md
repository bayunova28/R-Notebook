Stroke Prediction
================
Bayu Nova
July 26, 2021

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook.

``` r
#import library
library(readr)
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

``` r
library(caret)
```

    ## Loading required package: lattice

    ## Loading required package: ggplot2

``` r
library(ggplot2)
library(party)
```

    ## Loading required package: grid

    ## Loading required package: mvtnorm

    ## Loading required package: modeltools

    ## Loading required package: stats4

    ## Loading required package: strucchange

    ## Loading required package: zoo

    ## 
    ## Attaching package: 'zoo'

    ## The following objects are masked from 'package:base':
    ## 
    ##     as.Date, as.Date.numeric

    ## Loading required package: sandwich

``` r
library(rpart)
library(rpart.plot)

#data extraction
healthcare_dataset_stroke_data <- read_csv("D:/healthcare-dataset-stroke-data.csv")
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   id = col_double(),
    ##   gender = col_character(),
    ##   age = col_double(),
    ##   hypertension = col_double(),
    ##   heart_disease = col_double(),
    ##   ever_married = col_character(),
    ##   work_type = col_character(),
    ##   Residence_type = col_character(),
    ##   avg_glucose_level = col_double(),
    ##   bmi = col_character(),
    ##   smoking_status = col_character(),
    ##   stroke = col_double()
    ## )

``` r
View(healthcare_dataset_stroke_data)
str(healthcare_dataset_stroke_data)
```

    ## spec_tbl_df [5,110 x 12] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
    ##  $ id               : num [1:5110] 9046 51676 31112 60182 1665 ...
    ##  $ gender           : chr [1:5110] "Male" "Female" "Male" "Female" ...
    ##  $ age              : num [1:5110] 67 61 80 49 79 81 74 69 59 78 ...
    ##  $ hypertension     : num [1:5110] 0 0 0 0 1 0 1 0 0 0 ...
    ##  $ heart_disease    : num [1:5110] 1 0 1 0 0 0 1 0 0 0 ...
    ##  $ ever_married     : chr [1:5110] "Yes" "Yes" "Yes" "Yes" ...
    ##  $ work_type        : chr [1:5110] "Private" "Self-employed" "Private" "Private" ...
    ##  $ Residence_type   : chr [1:5110] "Urban" "Rural" "Rural" "Urban" ...
    ##  $ avg_glucose_level: num [1:5110] 229 202 106 171 174 ...
    ##  $ bmi              : chr [1:5110] "36.6" "N/A" "32.5" "34.4" ...
    ##  $ smoking_status   : chr [1:5110] "formerly smoked" "never smoked" "never smoked" "smokes" ...
    ##  $ stroke           : num [1:5110] 1 1 1 1 1 1 1 1 1 1 ...
    ##  - attr(*, "spec")=
    ##   .. cols(
    ##   ..   id = col_double(),
    ##   ..   gender = col_character(),
    ##   ..   age = col_double(),
    ##   ..   hypertension = col_double(),
    ##   ..   heart_disease = col_double(),
    ##   ..   ever_married = col_character(),
    ##   ..   work_type = col_character(),
    ##   ..   Residence_type = col_character(),
    ##   ..   avg_glucose_level = col_double(),
    ##   ..   bmi = col_character(),
    ##   ..   smoking_status = col_character(),
    ##   ..   stroke = col_double()
    ##   .. )

``` r
dim(healthcare_dataset_stroke_data)
```

    ## [1] 5110   12

``` r
knitr::kable(head(healthcare_dataset_stroke_data))
```

|    id | gender | age | hypertension | heart\_disease | ever\_married | work\_type    | Residence\_type | avg\_glucose\_level | bmi  | smoking\_status | stroke |
|------:|:-------|----:|-------------:|---------------:|:--------------|:--------------|:----------------|--------------------:|:-----|:----------------|-------:|
|  9046 | Male   |  67 |            0 |              1 | Yes           | Private       | Urban           |              228.69 | 36.6 | formerly smoked |      1 |
| 51676 | Female |  61 |            0 |              0 | Yes           | Self-employed | Rural           |              202.21 | N/A  | never smoked    |      1 |
| 31112 | Male   |  80 |            0 |              1 | Yes           | Private       | Rural           |              105.92 | 32.5 | never smoked    |      1 |
| 60182 | Female |  49 |            0 |              0 | Yes           | Private       | Urban           |              171.23 | 34.4 | smokes          |      1 |
|  1665 | Female |  79 |            1 |              0 | Yes           | Self-employed | Rural           |              174.12 | 24   | never smoked    |      1 |
| 56669 | Male   |  81 |            0 |              0 | Yes           | Private       | Urban           |              186.21 | 29   | formerly smoked |      1 |

``` r
summary(healthcare_dataset_stroke_data)
```

    ##        id           gender               age         hypertension    
    ##  Min.   :   67   Length:5110        Min.   : 0.08   Min.   :0.00000  
    ##  1st Qu.:17741   Class :character   1st Qu.:25.00   1st Qu.:0.00000  
    ##  Median :36932   Mode  :character   Median :45.00   Median :0.00000  
    ##  Mean   :36518                      Mean   :43.23   Mean   :0.09746  
    ##  3rd Qu.:54682                      3rd Qu.:61.00   3rd Qu.:0.00000  
    ##  Max.   :72940                      Max.   :82.00   Max.   :1.00000  
    ##  heart_disease     ever_married        work_type         Residence_type    
    ##  Min.   :0.00000   Length:5110        Length:5110        Length:5110       
    ##  1st Qu.:0.00000   Class :character   Class :character   Class :character  
    ##  Median :0.00000   Mode  :character   Mode  :character   Mode  :character  
    ##  Mean   :0.05401                                                           
    ##  3rd Qu.:0.00000                                                           
    ##  Max.   :1.00000                                                           
    ##  avg_glucose_level     bmi            smoking_status         stroke       
    ##  Min.   : 55.12    Length:5110        Length:5110        Min.   :0.00000  
    ##  1st Qu.: 77.25    Class :character   Class :character   1st Qu.:0.00000  
    ##  Median : 91.89    Mode  :character   Mode  :character   Median :0.00000  
    ##  Mean   :106.15                                          Mean   :0.04873  
    ##  3rd Qu.:114.09                                          3rd Qu.:0.00000  
    ##  Max.   :271.74                                          Max.   :1.00000

``` r
#selection data
df <- healthcare_dataset_stroke_data[,c("gender", "age", "hypertension", "heart_disease", "avg_glucose_level", "smoking_status", "stroke")]
head(df)
```

    ## # A tibble: 6 x 7
    ##   gender   age hypertension heart_disease avg_glucose_lev~ smoking_status stroke
    ##   <chr>  <dbl>        <dbl>         <dbl>            <dbl> <chr>           <dbl>
    ## 1 Male      67            0             1             229. formerly smok~      1
    ## 2 Female    61            0             0             202. never smoked        1
    ## 3 Male      80            0             1             106. never smoked        1
    ## 4 Female    49            0             0             171. smokes              1
    ## 5 Female    79            1             0             174. never smoked        1
    ## 6 Male      81            0             0             186. formerly smok~      1

``` r
#transform data
df$gender <- as.factor(df$gender)
df$smoking_status <- as.factor(df$smoking_status)
df$stroke <- as.factor(df$stroke)
str(df)
```

    ## tibble [5,110 x 7] (S3: tbl_df/tbl/data.frame)
    ##  $ gender           : Factor w/ 3 levels "Female","Male",..: 2 1 2 1 1 2 2 1 1 1 ...
    ##  $ age              : num [1:5110] 67 61 80 49 79 81 74 69 59 78 ...
    ##  $ hypertension     : num [1:5110] 0 0 0 0 1 0 1 0 0 0 ...
    ##  $ heart_disease    : num [1:5110] 1 0 1 0 0 0 1 0 0 0 ...
    ##  $ avg_glucose_level: num [1:5110] 229 202 106 171 174 ...
    ##  $ smoking_status   : Factor w/ 4 levels "formerly smoked",..: 1 2 2 3 2 1 2 2 4 4 ...
    ##  $ stroke           : Factor w/ 2 levels "0","1": 2 2 2 2 2 2 2 2 2 2 ...

``` r
#linear regression model
set.seed(34000)
target <- sample(2, nrow(df), replace = TRUE, prob = c(0.8, 0.2))
training <- df[target == 2,]
testing <- df[target == 1,]

prop.table(table(training$stroke))
```

    ## 
    ##          0          1 
    ## 0.94930417 0.05069583

``` r
prop.table(table(testing$stroke))
```

    ## 
    ##          0          1 
    ## 0.95175439 0.04824561

``` r
stroke_tree_train <- ctree(stroke ~ gender + age + hypertension + heart_disease + avg_glucose_level + smoking_status, data = training)
print(stroke_tree_train)
```

    ## 
    ##   Conditional inference tree with 3 terminal nodes
    ## 
    ## Response:  stroke 
    ## Inputs:  gender, age, hypertension, heart_disease, avg_glucose_level, smoking_status 
    ## Number of observations:  1006 
    ## 
    ## 1) age <= 56; criterion = 1, statistic = 59.958
    ##   2) age <= 44; criterion = 0.988, statistic = 9.613
    ##     3)*  weights = 519 
    ##   2) age > 44
    ##     4)*  weights = 187 
    ## 1) age > 56
    ##   5)*  weights = 300

``` r
stroke_tree_test <- ctree(stroke ~ gender + age + hypertension + heart_disease + avg_glucose_level + smoking_status, data = testing)
print(stroke_tree_test)
```

    ## 
    ##   Conditional inference tree with 5 terminal nodes
    ## 
    ## Response:  stroke 
    ## Inputs:  gender, age, hypertension, heart_disease, avg_glucose_level, smoking_status 
    ## Number of observations:  4104 
    ## 
    ## 1) age <= 67; criterion = 1, statistic = 247.532
    ##   2) age <= 50; criterion = 1, statistic = 67.047
    ##     3) age <= 37; criterion = 0.997, statistic = 12.173
    ##       4)*  weights = 1608 
    ##     3) age > 37
    ##       5)*  weights = 784 
    ##   2) age > 50
    ##     6) heart_disease <= 0; criterion = 1, statistic = 18.443
    ##       7)*  weights = 952 
    ##     6) heart_disease > 0
    ##       8)*  weights = 70 
    ## 1) age > 67
    ##   9)*  weights = 690

``` r
stroke_rpart_train <- rpart(stroke ~ gender + age + hypertension + heart_disease + avg_glucose_level + smoking_status, data = training)
print(stroke_rpart_train)
```

    ## n= 1006 
    ## 
    ## node), split, n, loss, yval, (yprob)
    ##       * denotes terminal node
    ## 
    ##  1) root 1006 51 0 (0.94930417 0.05069583)  
    ##    2) age< 56.5 706  8 0 (0.98866856 0.01133144) *
    ##    3) age>=56.5 300 43 0 (0.85666667 0.14333333)  
    ##      6) smoking_status=formerly smoked,never smoked,Unknown 255 31 0 (0.87843137 0.12156863) *
    ##      7) smoking_status=smokes 45 12 0 (0.73333333 0.26666667)  
    ##       14) avg_glucose_level< 169.05 35  6 0 (0.82857143 0.17142857) *
    ##       15) avg_glucose_level>=169.05 10  4 1 (0.40000000 0.60000000) *

``` r
stroke_rpart_test <- rpart(stroke ~ gender + age + hypertension + heart_disease + avg_glucose_level + smoking_status, data = testing)
print(stroke_rpart_test)
```

    ## n= 4104 
    ## 
    ## node), split, n, loss, yval, (yprob)
    ##       * denotes terminal node
    ## 
    ## 1) root 4104 198 0 (0.95175439 0.04824561) *

``` r
#prediction
result <- predict(stroke_rpart_test, testing, type = 'class')
check <- table(testing$stroke, result)
print(check)
```

    ##    result
    ##        0    1
    ##   0 3906    0
    ##   1  198    0

``` r
#accuracy score
accuracy <- sum(diag(check)) / sum(check)
print(paste('Accuracy Score : ', accuracy))
```

    ## [1] "Accuracy Score :  0.951754385964912"

``` r
#confusion matrix
matrix <- confusionMatrix(testing$stroke, result)
print(matrix)
```

    ## Confusion Matrix and Statistics
    ## 
    ##           Reference
    ## Prediction    0    1
    ##          0 3906    0
    ##          1  198    0
    ##                                           
    ##                Accuracy : 0.9518          
    ##                  95% CI : (0.9447, 0.9581)
    ##     No Information Rate : 1               
    ##     P-Value [Acc > NIR] : 1               
    ##                                           
    ##                   Kappa : 0               
    ##                                           
    ##  Mcnemar's Test P-Value : <2e-16          
    ##                                           
    ##             Sensitivity : 0.9518          
    ##             Specificity :     NA          
    ##          Pos Pred Value :     NA          
    ##          Neg Pred Value :     NA          
    ##              Prevalence : 1.0000          
    ##          Detection Rate : 0.9518          
    ##    Detection Prevalence : 0.9518          
    ##       Balanced Accuracy :     NA          
    ##                                           
    ##        'Positive' Class : 0               
    ## 

``` r
#visualization
plot(stroke_tree_train, main = "Training")
```

![](Stroke-Prediction_files/figure-gfm/unnamed-chunk-1-1.png)<!-- -->

``` r
plot(stroke_tree_test, main = "testing")
```

![](Stroke-Prediction_files/figure-gfm/unnamed-chunk-1-2.png)<!-- -->

``` r
rpart.plot(stroke_rpart_train, main = "Training")
```

![](Stroke-Prediction_files/figure-gfm/unnamed-chunk-1-3.png)<!-- -->

``` r
rpart.plot(stroke_rpart_test, main = "Testing")
```

![](Stroke-Prediction_files/figure-gfm/unnamed-chunk-1-4.png)<!-- -->

``` r
sex_stroke <- table(healthcare_dataset_stroke_data$stroke, healthcare_dataset_stroke_data$gender)
print(sex_stroke)
```

    ##    
    ##     Female Male Other
    ##   0   2853 2007     1
    ##   1    141  108     0

``` r
barplot(sex_stroke, xlab = "Sex", main = "Gender ~ Stroke",
col = c("red","blue"), beside=TRUE, names.arg=c("Female","Male","Other"))
legend("topright",c("No", "Yes"), fill=c("red","blue"))
```

![](Stroke-Prediction_files/figure-gfm/unnamed-chunk-1-5.png)<!-- -->

``` r
gender_stroke <- c(4861, 249)
percent <- round(100 * gender_stroke / sum(gender_stroke), 1)
pie(gender_stroke, labels = percent, main = "Gender ~ Stroke", col = rainbow(length(gender_stroke)))
legend("topright", c("Female", "Male"), cex = 0.8, fill = rainbow(length(gender_stroke)))
```

![](Stroke-Prediction_files/figure-gfm/unnamed-chunk-1-6.png)<!-- -->

``` r
smoke_stroke <- table(healthcare_dataset_stroke_data$stroke, healthcare_dataset_stroke_data$smoking_status)
print(smoke_stroke)
```

    ##    
    ##     formerly smoked never smoked smokes Unknown
    ##   0             815         1802    747    1497
    ##   1              70           90     42      47

``` r
barplot(smoke_stroke, xlab = "Smoking Status", main = "Smoking Status ~ Stroke",
col = c("red","blue"), beside=TRUE, names.arg=c("Formerly Smoked","Never Smoked","Smokes", "Unknown"))
legend("topright",c("No", "Yes"), fill=c("red","blue"))
```

![](Stroke-Prediction_files/figure-gfm/unnamed-chunk-1-7.png)<!-- -->

``` r
status_troke <- c(885, 1892, 789, 1544)
percent <- round(100 * status_troke / sum(status_troke), 1)
pie(status_troke, labels = percent, main = "Smoking Status ~ Stroke", col = rainbow(length(status_troke)))
legend("topright", c("Formerly Smoked", "Never Smoked", "Smokes", "Unknown"), cex = 0.8, fill = rainbow(length(status_troke)))
```

![](Stroke-Prediction_files/figure-gfm/unnamed-chunk-1-8.png)<!-- -->
