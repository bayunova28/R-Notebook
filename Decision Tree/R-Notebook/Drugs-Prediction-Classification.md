Drugs Prediction Classification
================
Bayu Nova
July 22, 2021

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook.

``` r
#import library
library(readr)
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

``` r
library(party)
```

    ## Loading required package: grid

    ## Loading required package: mvtnorm

    ## Loading required package: modeltools

    ## Loading required package: stats4

    ## Loading required package: strucchange

    ## Loading required package: zoo

    ## 
    ## Attaching package: 'zoo'

    ## The following objects are masked from 'package:base':
    ## 
    ##     as.Date, as.Date.numeric

    ## Loading required package: sandwich

``` r
library(rpart)
library(rpart.plot)
library(ggplot2)
library(tidyverse)
```

    ## -- Attaching packages --------------------------------------- tidyverse 1.3.1 --

    ## v tibble  3.1.2     v stringr 1.4.0
    ## v tidyr   1.1.3     v forcats 0.5.1
    ## v purrr   0.3.4

    ## -- Conflicts ------------------------------------------ tidyverse_conflicts() --
    ## x stringr::boundary() masks strucchange::boundary()
    ## x dplyr::filter()     masks stats::filter()
    ## x dplyr::lag()        masks stats::lag()

``` r
#data extraction
drug200 <- read_csv("D:/drug200.csv")
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   Age = col_double(),
    ##   Sex = col_character(),
    ##   BP = col_character(),
    ##   Cholesterol = col_character(),
    ##   Na_to_K = col_double(),
    ##   Drug = col_character()
    ## )

``` r
View(drug200)
df <- drug200
knitr::kable(head(df))
```

| Age | Sex | BP     | Cholesterol | Na\_to\_K | Drug  |
|----:|:----|:-------|:------------|----------:|:------|
|  23 | F   | HIGH   | HIGH        |    25.355 | drugY |
|  47 | M   | LOW    | HIGH        |    13.093 | drugC |
|  47 | M   | LOW    | HIGH        |    10.114 | drugC |
|  28 | F   | NORMAL | HIGH        |     7.798 | drugX |
|  61 | F   | LOW    | HIGH        |    18.043 | drugY |
|  22 | F   | NORMAL | HIGH        |     8.607 | drugX |

``` r
str(df)
```

    ## spec_tbl_df [200 x 6] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
    ##  $ Age        : num [1:200] 23 47 47 28 61 22 49 41 60 43 ...
    ##  $ Sex        : chr [1:200] "F" "M" "M" "F" ...
    ##  $ BP         : chr [1:200] "HIGH" "LOW" "LOW" "NORMAL" ...
    ##  $ Cholesterol: chr [1:200] "HIGH" "HIGH" "HIGH" "HIGH" ...
    ##  $ Na_to_K    : num [1:200] 25.4 13.1 10.1 7.8 18 ...
    ##  $ Drug       : chr [1:200] "drugY" "drugC" "drugC" "drugX" ...
    ##  - attr(*, "spec")=
    ##   .. cols(
    ##   ..   Age = col_double(),
    ##   ..   Sex = col_character(),
    ##   ..   BP = col_character(),
    ##   ..   Cholesterol = col_character(),
    ##   ..   Na_to_K = col_double(),
    ##   ..   Drug = col_character()
    ##   .. )

``` r
dim(df)
```

    ## [1] 200   6

``` r
summary(df)
```

    ##       Age            Sex                 BP            Cholesterol       
    ##  Min.   :15.00   Length:200         Length:200         Length:200        
    ##  1st Qu.:31.00   Class :character   Class :character   Class :character  
    ##  Median :45.00   Mode  :character   Mode  :character   Mode  :character  
    ##  Mean   :44.31                                                           
    ##  3rd Qu.:58.00                                                           
    ##  Max.   :74.00                                                           
    ##     Na_to_K           Drug          
    ##  Min.   : 6.269   Length:200        
    ##  1st Qu.:10.445   Class :character  
    ##  Median :13.937   Mode  :character  
    ##  Mean   :16.084                     
    ##  3rd Qu.:19.380                     
    ##  Max.   :38.247

``` r
#transform target data
df$Drug <- factor(df$Drug)
df$Sex <- factor(df$Sex)
df$BP <- factor(df$BP)
df$Cholesterol <- factor(df$Cholesterol)
str(df)
```

    ## spec_tbl_df [200 x 6] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
    ##  $ Age        : num [1:200] 23 47 47 28 61 22 49 41 60 43 ...
    ##  $ Sex        : Factor w/ 2 levels "F","M": 1 2 2 1 1 1 1 2 2 2 ...
    ##  $ BP         : Factor w/ 3 levels "HIGH","LOW","NORMAL": 1 2 2 3 2 3 3 2 3 2 ...
    ##  $ Cholesterol: Factor w/ 2 levels "HIGH","NORMAL": 1 1 1 1 1 1 1 1 1 2 ...
    ##  $ Na_to_K    : num [1:200] 25.4 13.1 10.1 7.8 18 ...
    ##  $ Drug       : Factor w/ 5 levels "drugA","drugB",..: 5 3 3 4 5 4 5 3 5 5 ...
    ##  - attr(*, "spec")=
    ##   .. cols(
    ##   ..   Age = col_double(),
    ##   ..   Sex = col_character(),
    ##   ..   BP = col_character(),
    ##   ..   Cholesterol = col_character(),
    ##   ..   Na_to_K = col_double(),
    ##   ..   Drug = col_character()
    ##   .. )

``` r
#decision tree model
set.seed(34000)
target <- sample(2, nrow(df), replace = TRUE, prob = c(0.8,0.2))
training <- df[target == 1,]
testing <- df[target == 2,]

#check proportion data target
prop.table(table(training$Drug))
```

    ## 
    ##      drugA      drugB      drugC      drugX      drugY 
    ## 0.11949686 0.08176101 0.06918239 0.29559748 0.43396226

``` r
prop.table(table(testing$Drug))
```

    ## 
    ##      drugA      drugB      drugC      drugX      drugY 
    ## 0.09756098 0.07317073 0.12195122 0.17073171 0.53658537

``` r
#training & testing party
drugs_tree <- ctree(Drug~Age+Sex+BP+Cholesterol, data = training)
print(drugs_tree)
```

    ## 
    ##   Conditional inference tree with 6 terminal nodes
    ## 
    ## Response:  Drug 
    ## Inputs:  Age, Sex, BP, Cholesterol 
    ## Number of observations:  159 
    ## 
    ## 1) BP == {HIGH}; criterion = 1, statistic = 112.554
    ##   2) Age <= 50; criterion = 1, statistic = 22.325
    ##     3) Age <= 34; criterion = 0.986, statistic = 8.455
    ##       4)*  weights = 25 
    ##     3) Age > 34
    ##       5)*  weights = 15 
    ##   2) Age > 50
    ##     6)*  weights = 22 
    ## 1) BP == {LOW, NORMAL}
    ##   7) Cholesterol == {HIGH}; criterion = 0.999, statistic = 16.792
    ##     8) BP == {LOW}; criterion = 1, statistic = 26.63
    ##       9)*  weights = 20 
    ##     8) BP == {NORMAL}
    ##       10)*  weights = 30 
    ##   7) Cholesterol == {NORMAL}
    ##     11)*  weights = 47

``` r
plot(drugs_tree, main = "Training")
```

![](Drugs-Prediction-Classification_files/figure-gfm/unnamed-chunk-1-1.png)<!-- -->

``` r
drugs_tree <- ctree(Drug~Age+Sex+BP+Cholesterol, data = testing)
print(drugs_tree)
```

    ## 
    ##   Conditional inference tree with 3 terminal nodes
    ## 
    ## Response:  Drug 
    ## Inputs:  Age, Sex, BP, Cholesterol 
    ## Number of observations:  41 
    ## 
    ## 1) BP == {NORMAL}; criterion = 0.999, statistic = 30.93
    ##   2)*  weights = 8 
    ## 1) BP == {HIGH, LOW}
    ##   3) BP == {HIGH}; criterion = 0.97, statistic = 13.885
    ##     4)*  weights = 15 
    ##   3) BP == {LOW}
    ##     5)*  weights = 18

``` r
plot(drugs_tree, main = "Testing")
```

![](Drugs-Prediction-Classification_files/figure-gfm/unnamed-chunk-1-2.png)<!-- -->

``` r
#prediction testing party
predict(drugs_tree, testing)
```

    ##  [1] drugY drugY drugX drugY drugY drugY drugY drugY drugX drugX drugX drugY
    ## [13] drugY drugY drugY drugY drugY drugY drugY drugX drugY drugY drugY drugY
    ## [25] drugY drugY drugY drugY drugY drugY drugY drugX drugY drugX drugX drugY
    ## [37] drugY drugY drugY drugY drugY
    ## Levels: drugA drugB drugC drugX drugY

``` r
#training & testing rpart
drugs_rpart <- rpart(Drug~Age+Sex+BP+Cholesterol, training)
rpart.plot(drugs_rpart, extra = 2, main = "Training")
```

![](Drugs-Prediction-Classification_files/figure-gfm/unnamed-chunk-1-3.png)<!-- -->

``` r
drugs_rpart <- rpart(Drug~Age+Sex+BP+Cholesterol, testing)
rpart.plot(drugs_rpart, main = "Testing")
```

![](Drugs-Prediction-Classification_files/figure-gfm/unnamed-chunk-1-4.png)<!-- -->

``` r
#prediction testing rpart
predict(drugs_rpart, testing)
```

    ##        drugA      drugB     drugC      drugX     drugY
    ## 1  0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 2  0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 3  0.0000000 0.00000000 0.0000000 0.62500000 0.3750000
    ## 4  0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 5  0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 6  0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 7  0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 8  0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 9  0.0000000 0.00000000 0.0000000 0.62500000 0.3750000
    ## 10 0.0000000 0.00000000 0.0000000 0.62500000 0.3750000
    ## 11 0.0000000 0.00000000 0.0000000 0.62500000 0.3750000
    ## 12 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 13 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 14 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 15 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 16 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 17 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 18 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 19 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 20 0.0000000 0.00000000 0.0000000 0.62500000 0.3750000
    ## 21 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 22 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 23 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 24 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 25 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 26 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 27 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 28 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 29 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 30 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 31 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 32 0.0000000 0.00000000 0.0000000 0.62500000 0.3750000
    ## 33 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 34 0.0000000 0.00000000 0.0000000 0.62500000 0.3750000
    ## 35 0.0000000 0.00000000 0.0000000 0.62500000 0.3750000
    ## 36 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 37 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 38 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 39 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 40 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576
    ## 41 0.1212121 0.09090909 0.1515152 0.06060606 0.5757576

``` r
#prediction accuracy score
predict_netral <- predict(drugs_rpart, testing, type = 'class')
check <- table(testing$Drug, predict_netral)
print(check)
```

    ##        predict_netral
    ##         drugA drugB drugC drugX drugY
    ##   drugA     0     0     0     0     4
    ##   drugB     0     0     0     0     3
    ##   drugC     0     0     0     0     5
    ##   drugX     0     0     0     5     2
    ##   drugY     0     0     0     3    19

``` r
accuracy_score <- sum(diag(check)) / sum(check)
print(paste('Accuracy score :', accuracy_score))
```

    ## [1] "Accuracy score : 0.585365853658537"

``` r
#visualization
hist(df$Na_to_K)
```

![](Drugs-Prediction-Classification_files/figure-gfm/unnamed-chunk-1-5.png)<!-- -->
