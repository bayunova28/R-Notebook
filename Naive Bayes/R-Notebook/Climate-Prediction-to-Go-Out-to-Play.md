Climate Prediction to Go Out to Play
================
Bayu Nova
July 22, 2021

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook.

``` r
#import library
library(readr)
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

``` r
library(naivebayes)
```

    ## naivebayes 0.9.7 loaded

``` r
library(caret)
```

    ## Loading required package: lattice

    ## Loading required package: ggplot2

``` r
library(ggplot2)
library(psych)
```

    ## 
    ## Attaching package: 'psych'

    ## The following objects are masked from 'package:ggplot2':
    ## 
    ##     %+%, alpha

``` r
library(e1071)

#data extraction
dataset_sunny <- read_csv("D:/dataset_sunny.csv")
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   Outlook = col_character(),
    ##   Temperature = col_character(),
    ##   Humidity = col_character(),
    ##   Windy = col_logical(),
    ##   Play = col_character()
    ## )

``` r
View(dataset_sunny)
str(dataset_sunny)
```

    ## spec_tbl_df [14 x 5] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
    ##  $ Outlook    : chr [1:14] "sunny" "sunny" "overcast" "rainy" ...
    ##  $ Temperature: chr [1:14] "hot" "hot" "hot" "mild" ...
    ##  $ Humidity   : chr [1:14] "high" "high" "high" "high" ...
    ##  $ Windy      : logi [1:14] FALSE TRUE FALSE FALSE FALSE TRUE ...
    ##  $ Play       : chr [1:14] "no" "no" "yes" "yes" ...
    ##  - attr(*, "spec")=
    ##   .. cols(
    ##   ..   Outlook = col_character(),
    ##   ..   Temperature = col_character(),
    ##   ..   Humidity = col_character(),
    ##   ..   Windy = col_logical(),
    ##   ..   Play = col_character()
    ##   .. )

``` r
knitr::kable(dataset_sunny)
```

| Outlook  | Temperature | Humidity | Windy | Play |
|:---------|:------------|:---------|:------|:-----|
| sunny    | hot         | high     | FALSE | no   |
| sunny    | hot         | high     | TRUE  | no   |
| overcast | hot         | high     | FALSE | yes  |
| rainy    | mild        | high     | FALSE | yes  |
| rainy    | cool        | normal   | FALSE | yes  |
| rainy    | cool        | normal   | TRUE  | no   |
| overcast | cool        | normal   | TRUE  | yes  |
| sunny    | mild        | high     | FALSE | no   |
| sunny    | cool        | normal   | FALSE | yes  |
| rainy    | mild        | normal   | FALSE | yes  |
| sunny    | mild        | normal   | TRUE  | yes  |
| overcast | mild        | high     | TRUE  | yes  |
| overcast | hot         | normal   | FALSE | yes  |
| rainy    | mild        | high     | TRUE  | no   |

``` r
df <- dataset_sunny
summary(df)
```

    ##    Outlook          Temperature          Humidity           Windy        
    ##  Length:14          Length:14          Length:14          Mode :logical  
    ##  Class :character   Class :character   Class :character   FALSE:8        
    ##  Mode  :character   Mode  :character   Mode  :character   TRUE :6        
    ##      Play          
    ##  Length:14         
    ##  Class :character  
    ##  Mode  :character

``` r
dim(df)
```

    ## [1] 14  5

``` r
#transform data target
df$Outlook <- factor(df$Outlook)
df$Temperature <- factor(df$Temperature)
df$Humidity <- factor(df$Humidity)
df$Windy <- factor(df$Windy)
df$Play <- factor(df$Play)
str(df)
```

    ## spec_tbl_df [14 x 5] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
    ##  $ Outlook    : Factor w/ 3 levels "overcast","rainy",..: 3 3 1 2 2 2 1 3 3 2 ...
    ##  $ Temperature: Factor w/ 3 levels "cool","hot","mild": 2 2 2 3 1 1 1 3 1 3 ...
    ##  $ Humidity   : Factor w/ 2 levels "high","normal": 1 1 1 1 2 2 2 1 2 2 ...
    ##  $ Windy      : Factor w/ 2 levels "FALSE","TRUE": 1 2 1 1 1 2 2 1 1 1 ...
    ##  $ Play       : Factor w/ 2 levels "no","yes": 1 1 2 2 2 1 2 1 2 2 ...
    ##  - attr(*, "spec")=
    ##   .. cols(
    ##   ..   Outlook = col_character(),
    ##   ..   Temperature = col_character(),
    ##   ..   Humidity = col_character(),
    ##   ..   Windy = col_logical(),
    ##   ..   Play = col_character()
    ##   .. )

``` r
#training & testing data target
set.seed(34000)
target <- sample(2, nrow(df), replace = TRUE, prob = c(0.8,0.2))
training <- df[target == 1,]
testing <- df[target == 2,]

#naive bayes model
nb <- naiveBayes(training$Play ~ ., data = training)
print(nb)
```

    ## 
    ## Naive Bayes Classifier for Discrete Predictors
    ## 
    ## Call:
    ## naiveBayes.default(x = X, y = Y, laplace = laplace)
    ## 
    ## A-priori probabilities:
    ## Y
    ##  no yes 
    ## 0.3 0.7 
    ## 
    ## Conditional probabilities:
    ##      Outlook
    ## Y      overcast     rainy     sunny
    ##   no  0.0000000 0.6666667 0.3333333
    ##   yes 0.5714286 0.2857143 0.1428571
    ## 
    ##      Temperature
    ## Y          cool       hot      mild
    ##   no  0.3333333 0.3333333 0.3333333
    ##   yes 0.2857143 0.2857143 0.4285714
    ## 
    ##      Humidity
    ## Y          high    normal
    ##   no  0.6666667 0.3333333
    ##   yes 0.4285714 0.5714286
    ## 
    ##      Windy
    ## Y         FALSE      TRUE
    ##   no  0.3333333 0.6666667
    ##   yes 0.5714286 0.4285714

``` r
nb <- naiveBayes(testing$Play ~ ., data = testing)
print(nb)
```

    ## 
    ## Naive Bayes Classifier for Discrete Predictors
    ## 
    ## Call:
    ## naiveBayes.default(x = X, y = Y, laplace = laplace)
    ## 
    ## A-priori probabilities:
    ## Y
    ##  no yes 
    ## 0.5 0.5 
    ## 
    ## Conditional probabilities:
    ##      Outlook
    ## Y     overcast rainy sunny
    ##   no       0.0   0.0   1.0
    ##   yes      0.0   0.5   0.5
    ## 
    ##      Temperature
    ## Y     cool hot mild
    ##   no   0.0 0.5  0.5
    ##   yes  0.5 0.0  0.5
    ## 
    ##      Humidity
    ## Y     high normal
    ##   no     1      0
    ##   yes    0      1
    ## 
    ##      Windy
    ## Y     FALSE TRUE
    ##   no    0.5  0.5
    ##   yes   1.0  0.0

``` r
#prediction
prediction <- predict(nb, testing)
mean(prediction == testing$Play)
```

    ## [1] 1

``` r
confusionMatrix(prediction, testing$Play)
```

    ## Confusion Matrix and Statistics
    ## 
    ##           Reference
    ## Prediction no yes
    ##        no   2   0
    ##        yes  0   2
    ##                                      
    ##                Accuracy : 1          
    ##                  95% CI : (0.3976, 1)
    ##     No Information Rate : 0.5        
    ##     P-Value [Acc > NIR] : 0.0625     
    ##                                      
    ##                   Kappa : 1          
    ##                                      
    ##  Mcnemar's Test P-Value : NA         
    ##                                      
    ##             Sensitivity : 1.0        
    ##             Specificity : 1.0        
    ##          Pos Pred Value : 1.0        
    ##          Neg Pred Value : 1.0        
    ##              Prevalence : 0.5        
    ##          Detection Rate : 0.5        
    ##    Detection Prevalence : 0.5        
    ##       Balanced Accuracy : 1.0        
    ##                                      
    ##        'Positive' Class : no         
    ## 

``` r
#visualization
plot(training$Outlook, training$Play, xlab = "Outlook", ylab = "Playing", main = "Outlook ~ Playing")
```

![](Climate-Prediction-to-Go-Out-to-Play_files/figure-gfm/unnamed-chunk-1-1.png)<!-- -->

``` r
plot(training$Temperature, training$Play, xlab = "Temperature", ylab = "Playing", main = "Temperature ~ Playing")
```

![](Climate-Prediction-to-Go-Out-to-Play_files/figure-gfm/unnamed-chunk-1-2.png)<!-- -->

``` r
plot(training$Humidity, training$Play, xlab = "Humidity", ylab = "Playing", main = "Humidity ~ Playing")
```

![](Climate-Prediction-to-Go-Out-to-Play_files/figure-gfm/unnamed-chunk-1-3.png)<!-- -->

``` r
plot(training$Windy, training$Play, xlab = "Windy", ylab = "Playing", main = "Windy ~ Playing")
```

![](Climate-Prediction-to-Go-Out-to-Play_files/figure-gfm/unnamed-chunk-1-4.png)<!-- -->
