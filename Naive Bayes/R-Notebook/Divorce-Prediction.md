Divorce Prediction
================
Bayu Nova
July 27, 2021

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook.

``` r
#import library
library(readr)
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

``` r
library(caret)
```

    ## Loading required package: lattice

    ## Loading required package: ggplot2

``` r
library(naivebayes)
```

    ## naivebayes 0.9.7 loaded

``` r
library(psych)
```

    ## 
    ## Attaching package: 'psych'

    ## The following objects are masked from 'package:ggplot2':
    ## 
    ##     %+%, alpha

``` r
library(ggplot2)
library(e1071)

#data extraction
divorce_data <- read_delim("D:/divorce_data.csv", ";", escape_double = FALSE, trim_ws = TRUE)
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   .default = col_double()
    ## )
    ## i Use `spec()` for the full column specifications.

``` r
View(divorce_data)
str(divorce_data)
```

    ## spec_tbl_df [170 x 55] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
    ##  $ Q1     : num [1:170] 2 4 2 3 2 0 3 2 2 1 ...
    ##  $ Q2     : num [1:170] 2 4 2 2 2 0 3 1 2 1 ...
    ##  $ Q3     : num [1:170] 4 4 2 3 1 1 3 2 1 1 ...
    ##  $ Q4     : num [1:170] 1 4 2 2 1 0 2 2 0 1 ...
    ##  $ Q5     : num [1:170] 0 4 1 3 1 0 1 2 0 1 ...
    ##  $ Q6     : num [1:170] 0 0 3 3 1 2 3 1 4 2 ...
    ##  $ Q7     : num [1:170] 0 0 2 3 0 0 4 0 1 0 ...
    ##  $ Q8     : num [1:170] 0 4 1 3 0 0 3 3 3 2 ...
    ##  $ Q9     : num [1:170] 0 4 1 3 0 0 2 3 3 2 ...
    ##  $ Q10    : num [1:170] 0 4 2 3 0 1 2 2 3 2 ...
    ##  $ Q11    : num [1:170] 1 4 3 4 0 0 2 4 3 3 ...
    ##  $ Q12    : num [1:170] 0 3 4 3 1 2 2 3 3 0 ...
    ##  $ Q13    : num [1:170] 1 4 2 3 0 1 2 2 3 0 ...
    ##  $ Q14    : num [1:170] 1 0 3 4 1 0 3 3 3 2 ...
    ##  $ Q15    : num [1:170] 0 4 3 3 1 2 2 4 3 1 ...
    ##  $ Q16    : num [1:170] 1 4 3 3 1 0 3 3 3 0 ...
    ##  $ Q17    : num [1:170] 0 4 3 3 1 2 3 2 3 1 ...
    ##  $ Q18    : num [1:170] 0 4 3 3 1 1 3 3 3 2 ...
    ##  $ Q19    : num [1:170] 0 3 3 3 2 0 3 2 3 1 ...
    ##  $ Q20    : num [1:170] 1 2 2 4 1 1 2 1 3 0 ...
    ##  $ Q21    : num [1:170] 0 1 1 1 1 0 3 2 2 0 ...
    ##  $ Q22    : num [1:170] 0 1 0 1 0 0 3 1 2 0 ...
    ##  $ Q23    : num [1:170] 0 0 1 1 0 0 3 1 2 0 ...
    ##  $ Q24    : num [1:170] 0 2 2 1 0 0 3 2 3 1 ...
    ##  $ Q25    : num [1:170] 0 2 2 2 0 2 2 3 2 1 ...
    ##  $ Q26    : num [1:170] 0 1 2 1 2 2 3 3 3 1 ...
    ##  $ Q27    : num [1:170] 0 2 2 1 1 0 3 2 2 1 ...
    ##  $ Q28    : num [1:170] 0 0 2 1 2 0 2 2 3 1 ...
    ##  $ Q29    : num [1:170] 0 1 3 1 1 0 2 2 2 1 ...
    ##  $ Q30    : num [1:170] 1 1 2 3 1 0 2 3 3 1 ...
    ##  $ Q31    : num [1:170] 1 0 3 2 1 4 1 1 1 1 ...
    ##  $ Q32    : num [1:170] 2 4 3 3 1 1 2 1 1 1 ...
    ##  $ Q33    : num [1:170] 1 2 1 2 1 1 2 0 1 0 ...
    ##  $ Q34    : num [1:170] 2 3 1 2 1 1 1 2 1 1 ...
    ##  $ Q35    : num [1:170] 0 0 1 1 0 1 1 2 1 0 ...
    ##  $ Q36    : num [1:170] 1 2 1 1 0 1 2 1 1 0 ...
    ##  $ Q37    : num [1:170] 2 3 2 3 0 1 3 4 1 1 ...
    ##  $ Q38    : num [1:170] 1 4 1 3 0 2 2 4 2 1 ...
    ##  $ Q39    : num [1:170] 3 2 3 4 2 0 2 4 2 2 ...
    ##  $ Q40    : num [1:170] 3 4 3 4 1 2 3 4 2 2 ...
    ##  $ Q41    : num [1:170] 2 2 3 2 0 2 3 4 2 1 ...
    ##  $ Q42    : num [1:170] 1 2 3 2 2 1 3 4 2 2 ...
    ##  $ Q43    : num [1:170] 1 3 2 3 3 2 3 3 2 3 ...
    ##  $ Q44    : num [1:170] 2 4 3 2 0 3 4 2 2 2 ...
    ##  $ Q45    : num [1:170] 3 2 2 3 2 0 3 0 2 2 ...
    ##  $ Q46    : num [1:170] 2 2 3 2 2 2 3 0 1 2 ...
    ##  $ Q47    : num [1:170] 1 2 2 2 1 2 2 1 1 0 ...
    ##  $ Q48    : num [1:170] 3 3 3 3 2 1 3 2 1 2 ...
    ##  $ Q49    : num [1:170] 3 4 1 3 3 2 2 2 1 2 ...
    ##  $ Q50    : num [1:170] 3 4 1 3 2 1 3 2 1 2 ...
    ##  $ Q51    : num [1:170] 2 4 1 3 2 1 3 1 1 2 ...
    ##  $ Q52    : num [1:170] 3 4 2 2 2 1 2 1 1 4 ...
    ##  $ Q53    : num [1:170] 2 2 2 2 1 2 2 1 1 3 ...
    ##  $ Q54    : num [1:170] 1 2 2 2 0 0 2 0 1 3 ...
    ##  $ Divorce: num [1:170] 1 1 1 1 1 1 1 1 1 1 ...
    ##  - attr(*, "spec")=
    ##   .. cols(
    ##   ..   Q1 = col_double(),
    ##   ..   Q2 = col_double(),
    ##   ..   Q3 = col_double(),
    ##   ..   Q4 = col_double(),
    ##   ..   Q5 = col_double(),
    ##   ..   Q6 = col_double(),
    ##   ..   Q7 = col_double(),
    ##   ..   Q8 = col_double(),
    ##   ..   Q9 = col_double(),
    ##   ..   Q10 = col_double(),
    ##   ..   Q11 = col_double(),
    ##   ..   Q12 = col_double(),
    ##   ..   Q13 = col_double(),
    ##   ..   Q14 = col_double(),
    ##   ..   Q15 = col_double(),
    ##   ..   Q16 = col_double(),
    ##   ..   Q17 = col_double(),
    ##   ..   Q18 = col_double(),
    ##   ..   Q19 = col_double(),
    ##   ..   Q20 = col_double(),
    ##   ..   Q21 = col_double(),
    ##   ..   Q22 = col_double(),
    ##   ..   Q23 = col_double(),
    ##   ..   Q24 = col_double(),
    ##   ..   Q25 = col_double(),
    ##   ..   Q26 = col_double(),
    ##   ..   Q27 = col_double(),
    ##   ..   Q28 = col_double(),
    ##   ..   Q29 = col_double(),
    ##   ..   Q30 = col_double(),
    ##   ..   Q31 = col_double(),
    ##   ..   Q32 = col_double(),
    ##   ..   Q33 = col_double(),
    ##   ..   Q34 = col_double(),
    ##   ..   Q35 = col_double(),
    ##   ..   Q36 = col_double(),
    ##   ..   Q37 = col_double(),
    ##   ..   Q38 = col_double(),
    ##   ..   Q39 = col_double(),
    ##   ..   Q40 = col_double(),
    ##   ..   Q41 = col_double(),
    ##   ..   Q42 = col_double(),
    ##   ..   Q43 = col_double(),
    ##   ..   Q44 = col_double(),
    ##   ..   Q45 = col_double(),
    ##   ..   Q46 = col_double(),
    ##   ..   Q47 = col_double(),
    ##   ..   Q48 = col_double(),
    ##   ..   Q49 = col_double(),
    ##   ..   Q50 = col_double(),
    ##   ..   Q51 = col_double(),
    ##   ..   Q52 = col_double(),
    ##   ..   Q53 = col_double(),
    ##   ..   Q54 = col_double(),
    ##   ..   Divorce = col_double()
    ##   .. )

``` r
dim(divorce_data)
```

    ## [1] 170  55

``` r
knitr::kable(head(divorce_data))
```

|  Q1 |  Q2 |  Q3 |  Q4 |  Q5 |  Q6 |  Q7 |  Q8 |  Q9 | Q10 | Q11 | Q12 | Q13 | Q14 | Q15 | Q16 | Q17 | Q18 | Q19 | Q20 | Q21 | Q22 | Q23 | Q24 | Q25 | Q26 | Q27 | Q28 | Q29 | Q30 | Q31 | Q32 | Q33 | Q34 | Q35 | Q36 | Q37 | Q38 | Q39 | Q40 | Q41 | Q42 | Q43 | Q44 | Q45 | Q46 | Q47 | Q48 | Q49 | Q50 | Q51 | Q52 | Q53 | Q54 | Divorce |
|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|----:|--------:|
|   2 |   2 |   4 |   1 |   0 |   0 |   0 |   0 |   0 |   0 |   1 |   0 |   1 |   1 |   0 |   1 |   0 |   0 |   0 |   1 |   0 |   0 |   0 |   0 |   0 |   0 |   0 |   0 |   0 |   1 |   1 |   2 |   1 |   2 |   0 |   1 |   2 |   1 |   3 |   3 |   2 |   1 |   1 |   2 |   3 |   2 |   1 |   3 |   3 |   3 |   2 |   3 |   2 |   1 |       1 |
|   4 |   4 |   4 |   4 |   4 |   0 |   0 |   4 |   4 |   4 |   4 |   3 |   4 |   0 |   4 |   4 |   4 |   4 |   3 |   2 |   1 |   1 |   0 |   2 |   2 |   1 |   2 |   0 |   1 |   1 |   0 |   4 |   2 |   3 |   0 |   2 |   3 |   4 |   2 |   4 |   2 |   2 |   3 |   4 |   2 |   2 |   2 |   3 |   4 |   4 |   4 |   4 |   2 |   2 |       1 |
|   2 |   2 |   2 |   2 |   1 |   3 |   2 |   1 |   1 |   2 |   3 |   4 |   2 |   3 |   3 |   3 |   3 |   3 |   3 |   2 |   1 |   0 |   1 |   2 |   2 |   2 |   2 |   2 |   3 |   2 |   3 |   3 |   1 |   1 |   1 |   1 |   2 |   1 |   3 |   3 |   3 |   3 |   2 |   3 |   2 |   3 |   2 |   3 |   1 |   1 |   1 |   2 |   2 |   2 |       1 |
|   3 |   2 |   3 |   2 |   3 |   3 |   3 |   3 |   3 |   3 |   4 |   3 |   3 |   4 |   3 |   3 |   3 |   3 |   3 |   4 |   1 |   1 |   1 |   1 |   2 |   1 |   1 |   1 |   1 |   3 |   2 |   3 |   2 |   2 |   1 |   1 |   3 |   3 |   4 |   4 |   2 |   2 |   3 |   2 |   3 |   2 |   2 |   3 |   3 |   3 |   3 |   2 |   2 |   2 |       1 |
|   2 |   2 |   1 |   1 |   1 |   1 |   0 |   0 |   0 |   0 |   0 |   1 |   0 |   1 |   1 |   1 |   1 |   1 |   2 |   1 |   1 |   0 |   0 |   0 |   0 |   2 |   1 |   2 |   1 |   1 |   1 |   1 |   1 |   1 |   0 |   0 |   0 |   0 |   2 |   1 |   0 |   2 |   3 |   0 |   2 |   2 |   1 |   2 |   3 |   2 |   2 |   2 |   1 |   0 |       1 |
|   0 |   0 |   1 |   0 |   0 |   2 |   0 |   0 |   0 |   1 |   0 |   2 |   1 |   0 |   2 |   0 |   2 |   1 |   0 |   1 |   0 |   0 |   0 |   0 |   2 |   2 |   0 |   0 |   0 |   0 |   4 |   1 |   1 |   1 |   1 |   1 |   1 |   2 |   0 |   2 |   2 |   1 |   2 |   3 |   0 |   2 |   2 |   1 |   2 |   1 |   1 |   1 |   2 |   0 |       1 |

``` r
summary(divorce_data)
```

    ##        Q1              Q2              Q3              Q4       
    ##  Min.   :0.000   Min.   :0.000   Min.   :0.000   Min.   :0.000  
    ##  1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000  
    ##  Median :2.000   Median :2.000   Median :2.000   Median :1.000  
    ##  Mean   :1.776   Mean   :1.653   Mean   :1.765   Mean   :1.482  
    ##  3rd Qu.:3.000   3rd Qu.:3.000   3rd Qu.:3.000   3rd Qu.:3.000  
    ##  Max.   :4.000   Max.   :4.000   Max.   :4.000   Max.   :4.000  
    ##        Q5              Q6               Q7               Q8       
    ##  Min.   :0.000   Min.   :0.0000   Min.   :0.0000   Min.   :0.000  
    ##  1st Qu.:0.000   1st Qu.:0.0000   1st Qu.:0.0000   1st Qu.:0.000  
    ##  Median :1.000   Median :0.0000   Median :0.0000   Median :1.000  
    ##  Mean   :1.541   Mean   :0.7471   Mean   :0.4941   Mean   :1.453  
    ##  3rd Qu.:3.000   3rd Qu.:1.0000   3rd Qu.:1.0000   3rd Qu.:3.000  
    ##  Max.   :4.000   Max.   :4.0000   Max.   :4.0000   Max.   :4.000  
    ##        Q9             Q10             Q11             Q12       
    ##  Min.   :0.000   Min.   :0.000   Min.   :0.000   Min.   :0.000  
    ##  1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000  
    ##  Median :1.000   Median :2.000   Median :1.000   Median :1.500  
    ##  Mean   :1.459   Mean   :1.576   Mean   :1.688   Mean   :1.653  
    ##  3rd Qu.:3.000   3rd Qu.:3.000   3rd Qu.:3.000   3rd Qu.:3.000  
    ##  Max.   :4.000   Max.   :4.000   Max.   :4.000   Max.   :4.000  
    ##       Q13             Q14             Q15             Q16       
    ##  Min.   :0.000   Min.   :0.000   Min.   :0.000   Min.   :0.000  
    ##  1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000  
    ##  Median :2.000   Median :1.000   Median :1.000   Median :1.000  
    ##  Mean   :1.835   Mean   :1.571   Mean   :1.571   Mean   :1.476  
    ##  3rd Qu.:3.000   3rd Qu.:3.000   3rd Qu.:3.000   3rd Qu.:3.000  
    ##  Max.   :4.000   Max.   :4.000   Max.   :4.000   Max.   :4.000  
    ##       Q17             Q18             Q19             Q20       
    ##  Min.   :0.000   Min.   :0.000   Min.   :0.000   Min.   :0.000  
    ##  1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000  
    ##  Median :1.000   Median :1.000   Median :1.000   Median :1.000  
    ##  Mean   :1.653   Mean   :1.518   Mean   :1.641   Mean   :1.459  
    ##  3rd Qu.:3.000   3rd Qu.:3.000   3rd Qu.:3.000   3rd Qu.:3.000  
    ##  Max.   :4.000   Max.   :4.000   Max.   :4.000   Max.   :4.000  
    ##       Q21             Q22             Q23             Q24       
    ##  Min.   :0.000   Min.   :0.000   Min.   :0.000   Min.   :0.000  
    ##  1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000  
    ##  Median :1.000   Median :0.000   Median :0.000   Median :1.000  
    ##  Mean   :1.388   Mean   :1.247   Mean   :1.412   Mean   :1.512  
    ##  3rd Qu.:3.000   3rd Qu.:3.000   3rd Qu.:3.000   3rd Qu.:3.000  
    ##  Max.   :4.000   Max.   :4.000   Max.   :4.000   Max.   :4.000  
    ##       Q25             Q26             Q27           Q28             Q29       
    ##  Min.   :0.000   Min.   :0.000   Min.   :0.0   Min.   :0.000   Min.   :0.000  
    ##  1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.0   1st Qu.:0.000   1st Qu.:0.000  
    ##  Median :1.000   Median :1.000   Median :1.0   Median :0.500   Median :1.000  
    ##  Mean   :1.629   Mean   :1.488   Mean   :1.4   Mean   :1.306   Mean   :1.494  
    ##  3rd Qu.:3.000   3rd Qu.:3.000   3rd Qu.:3.0   3rd Qu.:3.000   3rd Qu.:3.000  
    ##  Max.   :4.000   Max.   :4.000   Max.   :4.0   Max.   :4.000   Max.   :4.000  
    ##       Q30             Q31             Q32             Q33             Q34     
    ##  Min.   :0.000   Min.   :0.000   Min.   :0.000   Min.   :0.000   Min.   :0.0  
    ##  1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.0  
    ##  Median :1.000   Median :2.000   Median :2.000   Median :1.000   Median :1.0  
    ##  Mean   :1.494   Mean   :2.124   Mean   :2.059   Mean   :1.806   Mean   :1.9  
    ##  3rd Qu.:3.000   3rd Qu.:4.000   3rd Qu.:4.000   3rd Qu.:4.000   3rd Qu.:4.0  
    ##  Max.   :4.000   Max.   :4.000   Max.   :4.000   Max.   :4.000   Max.   :4.0  
    ##       Q35             Q36             Q37             Q38       
    ##  Min.   :0.000   Min.   :0.000   Min.   :0.000   Min.   :0.000  
    ##  1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000  
    ##  Median :0.500   Median :0.000   Median :2.000   Median :1.000  
    ##  Mean   :1.671   Mean   :1.606   Mean   :2.088   Mean   :1.859  
    ##  3rd Qu.:4.000   3rd Qu.:4.000   3rd Qu.:4.000   3rd Qu.:4.000  
    ##  Max.   :4.000   Max.   :4.000   Max.   :4.000   Max.   :4.000  
    ##       Q39             Q40             Q41             Q42       
    ##  Min.   :0.000   Min.   :0.000   Min.   :0.000   Min.   :0.000  
    ##  1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000   1st Qu.:0.000  
    ##  Median :2.000   Median :1.500   Median :2.000   Median :2.000  
    ##  Mean   :2.088   Mean   :1.871   Mean   :1.994   Mean   :2.159  
    ##  3rd Qu.:4.000   3rd Qu.:4.000   3rd Qu.:4.000   3rd Qu.:4.000  
    ##  Max.   :4.000   Max.   :4.000   Max.   :4.000   Max.   :4.000  
    ##       Q43             Q44             Q45             Q46       
    ##  Min.   :0.000   Min.   :0.000   Min.   :0.000   Min.   :0.000  
    ##  1st Qu.:2.000   1st Qu.:0.000   1st Qu.:1.000   1st Qu.:2.000  
    ##  Median :3.000   Median :2.000   Median :3.000   Median :3.000  
    ##  Mean   :2.706   Mean   :1.941   Mean   :2.459   Mean   :2.553  
    ##  3rd Qu.:4.000   3rd Qu.:4.000   3rd Qu.:4.000   3rd Qu.:4.000  
    ##  Max.   :4.000   Max.   :4.000   Max.   :4.000   Max.   :4.000  
    ##       Q47             Q48             Q49             Q50       
    ##  Min.   :0.000   Min.   :0.000   Min.   :0.000   Min.   :0.000  
    ##  1st Qu.:1.000   1st Qu.:2.000   1st Qu.:1.000   1st Qu.:1.000  
    ##  Median :2.000   Median :3.000   Median :3.000   Median :2.000  
    ##  Mean   :2.271   Mean   :2.741   Mean   :2.382   Mean   :2.429  
    ##  3rd Qu.:4.000   3rd Qu.:4.000   3rd Qu.:4.000   3rd Qu.:4.000  
    ##  Max.   :4.000   Max.   :4.000   Max.   :4.000   Max.   :4.000  
    ##       Q51             Q52             Q53             Q54       
    ##  Min.   :0.000   Min.   :0.000   Min.   :0.000   Min.   :0.000  
    ##  1st Qu.:2.000   1st Qu.:1.000   1st Qu.:1.000   1st Qu.:0.000  
    ##  Median :3.000   Median :3.000   Median :2.000   Median :2.000  
    ##  Mean   :2.476   Mean   :2.518   Mean   :2.241   Mean   :2.012  
    ##  3rd Qu.:4.000   3rd Qu.:4.000   3rd Qu.:4.000   3rd Qu.:4.000  
    ##  Max.   :4.000   Max.   :4.000   Max.   :4.000   Max.   :4.000  
    ##     Divorce      
    ##  Min.   :0.0000  
    ##  1st Qu.:0.0000  
    ##  Median :0.0000  
    ##  Mean   :0.4941  
    ##  3rd Qu.:1.0000  
    ##  Max.   :1.0000

``` r
df <- data.frame(divorce_data)
head(df)
```

    ##   Q1 Q2 Q3 Q4 Q5 Q6 Q7 Q8 Q9 Q10 Q11 Q12 Q13 Q14 Q15 Q16 Q17 Q18 Q19 Q20 Q21
    ## 1  2  2  4  1  0  0  0  0  0   0   1   0   1   1   0   1   0   0   0   1   0
    ## 2  4  4  4  4  4  0  0  4  4   4   4   3   4   0   4   4   4   4   3   2   1
    ## 3  2  2  2  2  1  3  2  1  1   2   3   4   2   3   3   3   3   3   3   2   1
    ## 4  3  2  3  2  3  3  3  3  3   3   4   3   3   4   3   3   3   3   3   4   1
    ## 5  2  2  1  1  1  1  0  0  0   0   0   1   0   1   1   1   1   1   2   1   1
    ## 6  0  0  1  0  0  2  0  0  0   1   0   2   1   0   2   0   2   1   0   1   0
    ##   Q22 Q23 Q24 Q25 Q26 Q27 Q28 Q29 Q30 Q31 Q32 Q33 Q34 Q35 Q36 Q37 Q38 Q39 Q40
    ## 1   0   0   0   0   0   0   0   0   1   1   2   1   2   0   1   2   1   3   3
    ## 2   1   0   2   2   1   2   0   1   1   0   4   2   3   0   2   3   4   2   4
    ## 3   0   1   2   2   2   2   2   3   2   3   3   1   1   1   1   2   1   3   3
    ## 4   1   1   1   2   1   1   1   1   3   2   3   2   2   1   1   3   3   4   4
    ## 5   0   0   0   0   2   1   2   1   1   1   1   1   1   0   0   0   0   2   1
    ## 6   0   0   0   2   2   0   0   0   0   4   1   1   1   1   1   1   2   0   2
    ##   Q41 Q42 Q43 Q44 Q45 Q46 Q47 Q48 Q49 Q50 Q51 Q52 Q53 Q54 Divorce
    ## 1   2   1   1   2   3   2   1   3   3   3   2   3   2   1       1
    ## 2   2   2   3   4   2   2   2   3   4   4   4   4   2   2       1
    ## 3   3   3   2   3   2   3   2   3   1   1   1   2   2   2       1
    ## 4   2   2   3   2   3   2   2   3   3   3   3   2   2   2       1
    ## 5   0   2   3   0   2   2   1   2   3   2   2   2   1   0       1
    ## 6   2   1   2   3   0   2   2   1   2   1   1   1   2   0       1

``` r
#naive bayes model
set.seed(34000)
target <- sample(2, nrow(df), replace = TRUE, prob = c(0.8, 0.2))
training <- df[target == 2,]
testing <- df[target == 1,]

nb_train <- naiveBayes(training$Divorce ~ ., data = training)
print(nb_train)
```

    ## 
    ## Naive Bayes Classifier for Discrete Predictors
    ## 
    ## Call:
    ## naiveBayes.default(x = X, y = Y, laplace = laplace)
    ## 
    ## A-priori probabilities:
    ## Y
    ##        0        1 
    ## 0.516129 0.483871 
    ## 
    ## Conditional probabilities:
    ##    Q1
    ## Y       [,1]      [,2]
    ##   0 0.625000 1.2583057
    ##   1 3.066667 0.8837151
    ## 
    ##    Q2
    ## Y       [,1]      [,2]
    ##   0 0.562500 0.8920949
    ##   1 2.733333 1.0327956
    ## 
    ##    Q3
    ## Y       [,1]      [,2]
    ##   0 0.875000 1.3102163
    ##   1 2.666667 0.8997354
    ## 
    ##    Q4
    ## Y       [,1]     [,2]
    ##   0 0.375000 1.024695
    ##   1 2.533333 1.125463
    ## 
    ##    Q5
    ## Y       [,1]     [,2]
    ##   0 0.125000 0.500000
    ##   1 2.933333 1.162919
    ## 
    ##    Q6
    ## Y   [,1]      [,2]
    ##   0 0.25 0.4472136
    ##   1 1.20 1.0823255
    ## 
    ##    Q7
    ## Y        [,1]      [,2]
    ##   0 0.0000000 0.0000000
    ##   1 0.5333333 0.5163978
    ## 
    ##    Q8
    ## Y   [,1]      [,2]
    ##   0 0.25 0.6831301
    ##   1 3.00 0.7559289
    ## 
    ##    Q9
    ## Y       [,1]      [,2]
    ##   0 0.062500 0.2500000
    ##   1 2.933333 0.5936168
    ## 
    ##    Q10
    ## Y     [,1]      [,2]
    ##   0 0.4375 0.7274384
    ##   1 2.8000 0.7745967
    ## 
    ##    Q11
    ## Y    [,1]      [,2]
    ##   0 0.375 0.5000000
    ##   1 3.400 0.5070926
    ## 
    ##    Q12
    ## Y   [,1]      [,2]
    ##   0  0.5 0.6324555
    ##   1  2.8 1.0141851
    ## 
    ##    Q13
    ## Y       [,1]      [,2]
    ##   0 0.750000 0.7745967
    ##   1 3.066667 1.0327956
    ## 
    ##    Q14
    ## Y       [,1]      [,2]
    ##   0 0.312500 0.6020797
    ##   1 2.733333 1.0327956
    ## 
    ##    Q15
    ## Y       [,1]      [,2]
    ##   0 0.375000 0.6191392
    ##   1 2.933333 0.7988086
    ## 
    ##    Q16
    ## Y       [,1]      [,2]
    ##   0 0.187500 0.4031129
    ##   1 2.733333 1.0327956
    ## 
    ##    Q17
    ## Y       [,1]      [,2]
    ##   0 0.250000 0.5773503
    ##   1 3.133333 0.8338094
    ## 
    ##    Q18
    ## Y     [,1]      [,2]
    ##   0 0.1875 0.4031129
    ##   1 3.0000 0.7559289
    ## 
    ##    Q19
    ## Y       [,1]      [,2]
    ##   0 0.312500 0.4787136
    ##   1 3.066667 0.7988086
    ## 
    ##    Q20
    ## Y     [,1]     [,2]
    ##   0 0.0625 0.250000
    ##   1 2.6000 1.121224
    ## 
    ##    Q21
    ## Y       [,1]      [,2]
    ##   0 0.187500 0.4031129
    ##   1 2.466667 0.9904304
    ## 
    ##    Q22
    ## Y       [,1]     [,2]
    ##   0 0.125000 0.341565
    ##   1 2.333333 1.112697
    ## 
    ##    Q23
    ## Y       [,1]     [,2]
    ##   0 0.125000 0.500000
    ##   1 2.666667 1.345185
    ## 
    ##    Q24
    ## Y       [,1]      [,2]
    ##   0 0.437500 0.7274384
    ##   1 2.733333 0.8837151
    ## 
    ##    Q25
    ## Y   [,1]      [,2]
    ##   0 0.25 0.4472136
    ##   1 3.00 0.8451543
    ## 
    ##    Q26
    ## Y       [,1]      [,2]
    ##   0 0.125000 0.3415650
    ##   1 2.733333 0.9611501
    ## 
    ##    Q27
    ## Y   [,1]      [,2]
    ##   0 0.25 0.4472136
    ##   1 2.60 0.7367884
    ## 
    ##    Q28
    ## Y       [,1]     [,2]
    ##   0 0.125000 0.341565
    ##   1 2.466667 1.060099
    ## 
    ##    Q29
    ## Y       [,1]      [,2]
    ##   0 0.062500 0.2500000
    ##   1 2.866667 0.9904304
    ## 
    ##    Q30
    ## Y       [,1]      [,2]
    ##   0 0.125000 0.3415650
    ##   1 2.733333 0.9611501
    ## 
    ##    Q31
    ## Y       [,1]     [,2]
    ##   0 1.250000 1.341641
    ##   1 3.133333 1.505545
    ## 
    ##    Q32
    ## Y     [,1]     [,2]
    ##   0 0.6875 1.078193
    ##   1 3.2000 1.207122
    ## 
    ##    Q33
    ## Y       [,1]      [,2]
    ##   0 0.312500 0.7932003
    ##   1 3.133333 1.5522641
    ## 
    ##    Q34
    ## Y    [,1]     [,2]
    ##   0 0.625 1.087811
    ##   1 3.200 1.082326
    ## 
    ##    Q35
    ## Y       [,1]     [,2]
    ##   0 0.062500 0.250000
    ##   1 3.133333 1.552264
    ## 
    ##    Q36
    ## Y     [,1]    [,2]
    ##   0 0.0625 0.25000
    ##   1 3.0000 1.36277
    ## 
    ##    Q37
    ## Y       [,1]     [,2]
    ##   0 0.812500 1.376893
    ##   1 3.533333 1.060099
    ## 
    ##    Q38
    ## Y       [,1]      [,2]
    ##   0 0.187500 0.4031129
    ##   1 3.466667 0.9154754
    ## 
    ##    Q39
    ## Y     [,1]      [,2]
    ##   0 0.4375 0.5123475
    ##   1 3.6000 0.8280787
    ## 
    ##    Q40
    ## Y       [,1]      [,2]
    ##   0 0.250000 0.4472136
    ##   1 3.533333 0.7432234
    ## 
    ##    Q41
    ## Y       [,1]      [,2]
    ##   0 0.625000 0.8850612
    ##   1 3.533333 0.9904304
    ## 
    ##    Q42
    ## Y     [,1]      [,2]
    ##   0 0.6875 1.1383468
    ##   1 3.4000 0.8280787
    ## 
    ##    Q43
    ## Y       [,1]      [,2]
    ##   0 1.500000 1.2110601
    ##   1 3.666667 0.6172134
    ## 
    ##    Q44
    ## Y     [,1]      [,2]
    ##   0 0.1875 0.5439056
    ##   1 3.4000 0.8280787
    ## 
    ##    Q45
    ## Y     [,1]     [,2]
    ##   0 1.4375 1.504161
    ##   1 3.2000 1.207122
    ## 
    ##    Q46
    ## Y       [,1]     [,2]
    ##   0 2.000000 1.414214
    ##   1 3.066667 1.279881
    ## 
    ##    Q47
    ## Y     [,1]      [,2]
    ##   0 0.8125 0.9810708
    ##   1 3.2000 1.4242793
    ## 
    ##    Q48
    ## Y       [,1]      [,2]
    ##   0 1.812500 1.1086779
    ##   1 3.266667 0.9611501
    ## 
    ##    Q49
    ## Y       [,1]      [,2]
    ##   0 1.312500 1.1383468
    ##   1 3.533333 0.9904304
    ## 
    ##    Q50
    ## Y       [,1]      [,2]
    ##   0 1.375000 1.0878113
    ##   1 3.533333 0.9904304
    ## 
    ##    Q51
    ## Y       [,1]     [,2]
    ##   0 1.625000 1.204159
    ##   1 3.266667 1.099784
    ## 
    ##    Q52
    ## Y    [,1]     [,2]
    ##   0 1.625 1.500000
    ##   1 3.600 1.055597
    ## 
    ##    Q53
    ## Y   [,1]     [,2]
    ##   0 1.25 1.290994
    ##   1 3.20 1.082326
    ## 
    ##    Q54
    ## Y       [,1]      [,2]
    ##   0 0.625000 0.8062258
    ##   1 3.333333 1.2909944

``` r
nb_test <- naiveBayes(testing$Divorce ~ ., data = testing)
print(nb_test)
```

    ## 
    ## Naive Bayes Classifier for Discrete Predictors
    ## 
    ## Call:
    ## naiveBayes.default(x = X, y = Y, laplace = laplace)
    ## 
    ## A-priori probabilities:
    ## Y
    ##         0         1 
    ## 0.5035971 0.4964029 
    ## 
    ## Conditional probabilities:
    ##    Q1
    ## Y        [,1]      [,2]
    ##   0 0.3428571 0.7964726
    ##   1 3.2173913 0.7249658
    ## 
    ##    Q2
    ## Y        [,1]      [,2]
    ##   0 0.4428571 0.7349596
    ##   1 2.8985507 0.8935212
    ## 
    ##    Q3
    ## Y        [,1]      [,2]
    ##   0 0.5857143 0.8074447
    ##   1 2.9710145 0.7065037
    ## 
    ##    Q4
    ## Y        [,1]      [,2]
    ##   0 0.2428571 0.7505691
    ##   1 2.7681159 0.8769085
    ## 
    ##    Q5
    ## Y       [,1]      [,2]
    ##   0 0.100000 0.3468283
    ##   1 3.028986 0.9388825
    ## 
    ##    Q6
    ## Y       [,1]      [,2]
    ##   0 0.400000 0.7875848
    ##   1 1.115942 0.8666404
    ## 
    ##    Q7
    ## Y         [,1]      [,2]
    ##   0 0.01428571 0.1195229
    ##   1 1.08695652 1.1342156
    ## 
    ##    Q8
    ## Y       [,1]      [,2]
    ##   0 0.100000 0.3021661
    ##   1 2.768116 1.0592024
    ## 
    ##    Q9
    ## Y         [,1]      [,2]
    ##   0 0.05714286 0.2337913
    ##   1 2.88405797 0.9320476
    ## 
    ##    Q10
    ## Y       [,1]      [,2]
    ##   0 0.400000 0.6681144
    ##   1 2.768116 0.9098307
    ## 
    ##    Q11
    ## Y        [,1]      [,2]
    ##   0 0.1571429 0.3665631
    ##   1 3.1739130 0.8904150
    ## 
    ##    Q12
    ## Y        [,1]      [,2]
    ##   0 0.3714286 0.5690426
    ##   1 2.9710145 0.8219599
    ## 
    ##    Q13
    ## Y        [,1]      [,2]
    ##   0 0.5714286 0.7136643
    ##   1 3.1014493 0.8250656
    ## 
    ##    Q14
    ## Y        [,1]      [,2]
    ##   0 0.2857143 0.4858239
    ##   1 2.9130435 0.9352434
    ## 
    ##    Q15
    ## Y       [,1]      [,2]
    ##   0 0.200000 0.4373835
    ##   1 2.942029 0.8022772
    ## 
    ##    Q16
    ## Y        [,1]      [,2]
    ##   0 0.1571429 0.4041708
    ##   1 2.8405797 0.8848926
    ## 
    ##    Q17
    ## Y        [,1]      [,2]
    ##   0 0.1571429 0.3665631
    ##   1 3.1739130 0.7267276
    ## 
    ##    Q18
    ## Y         [,1]      [,2]
    ##   0 0.07142857 0.2593989
    ##   1 2.97101449 0.8219599
    ## 
    ##    Q19
    ## Y       [,1]      [,2]
    ##   0 0.100000 0.3468283
    ##   1 3.202899 0.7780010
    ## 
    ##    Q20
    ## Y         [,1]      [,2]
    ##   0 0.07142857 0.2593989
    ##   1 2.94202899 0.8381362
    ## 
    ##    Q21
    ## Y        [,1]      [,2]
    ##   0 0.1428571 0.3524537
    ##   1 2.6956522 0.9747450
    ## 
    ##    Q22
    ## Y         [,1]      [,2]
    ##   0 0.05714286 0.2337913
    ##   1 2.47826087 1.1454346
    ## 
    ##    Q23
    ## Y         [,1]      [,2]
    ##   0 0.07142857 0.3102794
    ##   1 2.79710145 1.1829638
    ## 
    ##    Q24
    ## Y        [,1]      [,2]
    ##   0 0.2285714 0.5155953
    ##   1 2.7971014 1.0513256
    ## 
    ##    Q25
    ## Y        [,1]      [,2]
    ##   0 0.3571429 0.5117663
    ##   1 2.9420290 1.0415494
    ## 
    ##    Q26
    ## Y        [,1]      [,2]
    ##   0 0.2142857 0.4132886
    ##   1 2.8260870 0.9694832
    ## 
    ##    Q27
    ## Y        [,1]      [,2]
    ##   0 0.1285714 0.3371418
    ##   1 2.6956522 1.0044657
    ## 
    ##    Q28
    ## Y         [,1]      [,2]
    ##   0 0.07142857 0.2593989
    ##   1 2.57971014 1.0901403
    ## 
    ##    Q29
    ## Y       [,1]      [,2]
    ##   0 0.100000 0.3021661
    ##   1 2.942029 0.9834522
    ## 
    ##    Q30
    ## Y        [,1]      [,2]
    ##   0 0.2142857 0.4469821
    ##   1 2.8405797 0.9490427
    ## 
    ##    Q31
    ## Y        [,1]      [,2]
    ##   0 0.7428571 0.8958150
    ##   1 3.5072464 0.8681147
    ## 
    ##    Q32
    ## Y        [,1]      [,2]
    ##   0 0.7428571 0.9733509
    ##   1 3.4637681 0.7190621
    ## 
    ##    Q33
    ## Y        [,1]      [,2]
    ##   0 0.2857143 0.6625129
    ##   1 3.4057971 0.9749637
    ## 
    ##    Q34
    ## Y        [,1]      [,2]
    ##   0 0.5428571 0.6742836
    ##   1 3.2898551 1.0160687
    ## 
    ##    Q35
    ## Y        [,1]     [,2]
    ##   0 0.1142857 0.435486
    ##   1 3.3043478 1.204213
    ## 
    ##    Q36
    ## Y         [,1]      [,2]
    ##   0 0.02857143 0.1678015
    ##   1 3.26086957 1.1330876
    ## 
    ##    Q37
    ## Y        [,1]      [,2]
    ##   0 0.5857143 0.7707113
    ##   1 3.5942029 0.7730543
    ## 
    ##    Q38
    ## Y        [,1]      [,2]
    ##   0 0.3857143 0.6436503
    ##   1 3.3913043 1.0031918
    ## 
    ##    Q39
    ## Y       [,1]      [,2]
    ##   0 0.600000 0.8235659
    ##   1 3.652174 0.7441159
    ## 
    ##    Q40
    ## Y      [,1]      [,2]
    ##   0 0.20000 0.4992748
    ##   1 3.57971 0.7357629
    ## 
    ##    Q41
    ## Y        [,1]      [,2]
    ##   0 0.4428571 0.6513245
    ##   1 3.5507246 0.8141439
    ## 
    ##    Q42
    ## Y       [,1]      [,2]
    ##   0 1.085714 1.2127685
    ##   1 3.318841 0.9154355
    ## 
    ##    Q43
    ## Y       [,1]      [,2]
    ##   0 2.057143 1.2727434
    ##   1 3.434783 0.9773653
    ## 
    ##    Q44
    ## Y        [,1]      [,2]
    ##   0 0.6142857 0.9523861
    ##   1 3.3768116 0.9091277
    ## 
    ##    Q45
    ## Y       [,1]     [,2]
    ##   0 1.700000 1.427981
    ##   1 3.304348 1.019001
    ## 
    ##    Q46
    ## Y       [,1]     [,2]
    ##   0 1.942857 1.350102
    ##   1 3.188406 1.061012
    ## 
    ##    Q47
    ## Y       [,1]     [,2]
    ##   0 1.342857 1.317504
    ##   1 3.347826 1.054766
    ## 
    ##    Q48
    ## Y       [,1]      [,2]
    ##   0 2.100000 1.0236338
    ##   1 3.492754 0.6558154
    ## 
    ##    Q49
    ## Y       [,1]      [,2]
    ##   0 1.271429 1.2267718
    ##   1 3.507246 0.7400952
    ## 
    ##    Q50
    ## Y       [,1]      [,2]
    ##   0 1.385714 1.0114254
    ##   1 3.492754 0.7788224
    ## 
    ##    Q51
    ## Y       [,1]      [,2]
    ##   0 1.614286 0.9214490
    ##   1 3.376812 0.7878013
    ## 
    ##    Q52
    ## Y       [,1]     [,2]
    ##   0 1.557143 1.292517
    ##   1 3.463768 0.832779
    ## 
    ##    Q53
    ## Y       [,1]      [,2]
    ##   0 1.171429 1.1668293
    ##   1 3.347826 0.8882582
    ## 
    ##    Q54
    ## Y       [,1]      [,2]
    ##   0 0.700000 0.8903671
    ##   1 3.376812 1.0585985

``` r
#prediction
nb_predict <- predict(nb_test, testing$Divorce)
```

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q1'. Did you use factors with numeric labels
    ## for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q2'. Did you use factors with numeric labels
    ## for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q3'. Did you use factors with numeric labels
    ## for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q4'. Did you use factors with numeric labels
    ## for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q5'. Did you use factors with numeric labels
    ## for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q6'. Did you use factors with numeric labels
    ## for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q7'. Did you use factors with numeric labels
    ## for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q8'. Did you use factors with numeric labels
    ## for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q9'. Did you use factors with numeric labels
    ## for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q10'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q11'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q12'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q13'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q14'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q15'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q16'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q17'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q18'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q19'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q20'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q21'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q22'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q23'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q24'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q25'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q26'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q27'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q28'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q29'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q30'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q31'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q32'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q33'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q34'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q35'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q36'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q37'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q38'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q39'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q40'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q41'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q42'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q43'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q44'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q45'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q46'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q47'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q48'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q49'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q50'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q51'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q52'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q53'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

    ## Warning in predict.naiveBayes(nb_test, testing$Divorce): Type mismatch between
    ## training and new data for variable 'Q54'. Did you use factors with numeric
    ## labels for training, and numeric values for new data?

``` r
nb_actual <- testing$Divorce
mean(nb_predict == testing$Divorce)
```

    ## [1] 0.5035971

``` r
#confusion matrix
matrix <- confusionMatrix(factor(nb_predict), factor(nb_actual))
```

    ## Warning in confusionMatrix.default(factor(nb_predict), factor(nb_actual)):
    ## Levels are not in the same order for reference and data. Refactoring data to
    ## match.

``` r
print(matrix)
```

    ## Confusion Matrix and Statistics
    ## 
    ##           Reference
    ## Prediction  0  1
    ##          0 70 69
    ##          1  0  0
    ##                                           
    ##                Accuracy : 0.5036          
    ##                  95% CI : (0.4176, 0.5894)
    ##     No Information Rate : 0.5036          
    ##     P-Value [Acc > NIR] : 0.5339          
    ##                                           
    ##                   Kappa : 0               
    ##                                           
    ##  Mcnemar's Test P-Value : 2.695e-16       
    ##                                           
    ##             Sensitivity : 1.0000          
    ##             Specificity : 0.0000          
    ##          Pos Pred Value : 0.5036          
    ##          Neg Pred Value :    NaN          
    ##              Prevalence : 0.5036          
    ##          Detection Rate : 0.5036          
    ##    Detection Prevalence : 1.0000          
    ##       Balanced Accuracy : 0.5000          
    ##                                           
    ##        'Positive' Class : 0               
    ## 

``` r
#visualization
divorce <- c(86, 84)
percent <- round(100 * divorce / sum(divorce), 1)
pie(divorce, labels = percent, main = "Divorce Prediction", col = rainbow(length(divorce)))
legend("topright", c("No", "Yes"), cex = 0.8, fill = rainbow(length(divorce)))
```

![](Divorce-Prediction_files/figure-gfm/unnamed-chunk-1-1.png)<!-- -->
