Grocery Sales Prediction
================
Bayu Nova
July 24, 2021

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook.

``` r
#import library
library(readr)
library(tidyverse)
```

    ## -- Attaching packages --------------------------------------- tidyverse 1.3.1 --

    ## v ggplot2 3.3.5     v dplyr   1.0.7
    ## v tibble  3.1.2     v stringr 1.4.0
    ## v tidyr   1.1.3     v forcats 0.5.1
    ## v purrr   0.3.4

    ## -- Conflicts ------------------------------------------ tidyverse_conflicts() --
    ## x dplyr::filter() masks stats::filter()
    ## x dplyr::lag()    masks stats::lag()

``` r
library(arules)
```

    ## Loading required package: Matrix

    ## 
    ## Attaching package: 'Matrix'

    ## The following objects are masked from 'package:tidyr':
    ## 
    ##     expand, pack, unpack

    ## 
    ## Attaching package: 'arules'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     recode

    ## The following objects are masked from 'package:base':
    ## 
    ##     abbreviate, write

``` r
library(arulesViz)
library(knitr)
library(gridExtra)
```

    ## 
    ## Attaching package: 'gridExtra'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     combine

``` r
library(lubridate)
```

    ## 
    ## Attaching package: 'lubridate'

    ## The following objects are masked from 'package:arules':
    ## 
    ##     intersect, setdiff, union

    ## The following objects are masked from 'package:base':
    ## 
    ##     date, intersect, setdiff, union

``` r
#data extracion
data("Groceries")
str(Groceries)
```

    ## Formal class 'transactions' [package "arules"] with 3 slots
    ##   ..@ data       :Formal class 'ngCMatrix' [package "Matrix"] with 5 slots
    ##   .. .. ..@ i       : int [1:43367] 13 60 69 78 14 29 98 24 15 29 ...
    ##   .. .. ..@ p       : int [1:9836] 0 4 7 8 12 16 21 22 27 28 ...
    ##   .. .. ..@ Dim     : int [1:2] 169 9835
    ##   .. .. ..@ Dimnames:List of 2
    ##   .. .. .. ..$ : NULL
    ##   .. .. .. ..$ : NULL
    ##   .. .. ..@ factors : list()
    ##   ..@ itemInfo   :'data.frame':  169 obs. of  3 variables:
    ##   .. ..$ labels: chr [1:169] "frankfurter" "sausage" "liver loaf" "ham" ...
    ##   .. ..$ level2: Factor w/ 55 levels "baby food","bags",..: 44 44 44 44 44 44 44 42 42 41 ...
    ##   .. ..$ level1: Factor w/ 10 levels "canned food",..: 6 6 6 6 6 6 6 6 6 6 ...
    ##   ..@ itemsetInfo:'data.frame':  0 obs. of  0 variables

``` r
dim(Groceries)
```

    ## [1] 9835  169

``` r
summary(Groceries)
```

    ## transactions as itemMatrix in sparse format with
    ##  9835 rows (elements/itemsets/transactions) and
    ##  169 columns (items) and a density of 0.02609146 
    ## 
    ## most frequent items:
    ##       whole milk other vegetables       rolls/buns             soda 
    ##             2513             1903             1809             1715 
    ##           yogurt          (Other) 
    ##             1372            34055 
    ## 
    ## element (itemset/transaction) length distribution:
    ## sizes
    ##    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16 
    ## 2159 1643 1299 1005  855  645  545  438  350  246  182  117   78   77   55   46 
    ##   17   18   19   20   21   22   23   24   26   27   28   29   32 
    ##   29   14   14    9   11    4    6    1    1    1    1    3    1 
    ## 
    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##   1.000   2.000   3.000   4.409   6.000  32.000 
    ## 
    ## includes extended item information - examples:
    ##        labels  level2           level1
    ## 1 frankfurter sausage meat and sausage
    ## 2     sausage sausage meat and sausage
    ## 3  liver loaf sausage meat and sausage

``` r
df <- Groceries
head(df)
```

    ## transactions in sparse format with
    ##  6 transactions (rows) and
    ##  169 items (columns)

``` r
#association rules model
rules <- apriori(df, parameter = list(supp = 0.001, conf = 0.80))
```

    ## Apriori
    ## 
    ## Parameter specification:
    ##  confidence minval smax arem  aval originalSupport maxtime support minlen
    ##         0.8    0.1    1 none FALSE            TRUE       5   0.001      1
    ##  maxlen target  ext
    ##      10  rules TRUE
    ## 
    ## Algorithmic control:
    ##  filter tree heap memopt load sort verbose
    ##     0.1 TRUE TRUE  FALSE TRUE    2    TRUE
    ## 
    ## Absolute minimum support count: 9 
    ## 
    ## set item appearances ...[0 item(s)] done [0.00s].
    ## set transactions ...[169 item(s), 9835 transaction(s)] done [0.00s].
    ## sorting and recoding items ... [157 item(s)] done [0.00s].
    ## creating transaction tree ... done [0.00s].
    ## checking subsets of size 1 2 3 4 5 6 done [0.01s].
    ## writing ... [410 rule(s)] done [0.00s].
    ## creating S4 object  ... done [0.00s].

``` r
inspect(rules[1:10])
```

    ##      lhs                         rhs                    support confidence    coverage      lift count
    ## [1]  {liquor,                                                                                         
    ##       red/blush wine}         => {bottled beer}     0.001931876  0.9047619 0.002135231 11.235269    19
    ## [2]  {curd,                                                                                           
    ##       cereals}                => {whole milk}       0.001016777  0.9090909 0.001118454  3.557863    10
    ## [3]  {yogurt,                                                                                         
    ##       cereals}                => {whole milk}       0.001728521  0.8095238 0.002135231  3.168192    17
    ## [4]  {butter,                                                                                         
    ##       jam}                    => {whole milk}       0.001016777  0.8333333 0.001220132  3.261374    10
    ## [5]  {soups,                                                                                          
    ##       bottled beer}           => {whole milk}       0.001118454  0.9166667 0.001220132  3.587512    11
    ## [6]  {napkins,                                                                                        
    ##       house keeping products} => {whole milk}       0.001321810  0.8125000 0.001626843  3.179840    13
    ## [7]  {whipped/sour cream,                                                                             
    ##       house keeping products} => {whole milk}       0.001220132  0.9230769 0.001321810  3.612599    12
    ## [8]  {pastry,                                                                                         
    ##       sweet spreads}          => {whole milk}       0.001016777  0.9090909 0.001118454  3.557863    10
    ## [9]  {turkey,                                                                                         
    ##       curd}                   => {other vegetables} 0.001220132  0.8000000 0.001525165  4.134524    12
    ## [10] {rice,                                                                                           
    ##       sugar}                  => {whole milk}       0.001220132  1.0000000 0.001220132  3.913649    12

``` r
#visualization
arules::itemFrequencyPlot(df, topN=20, main='Relative Item Frequency Plot of Grocery Sales', col = "chartreuse1", type = "relative",ylab = "Item Frequency")
```

![](Grocery-Sales-Prediction_files/figure-gfm/unnamed-chunk-1-1.png)<!-- -->

``` r
arules::itemFrequencyPlot(df, topN=15, type="absolute", col="royalblue1",xlab="Item name", ylab="Frequency (absolute)", main="Absolute Item Frequency Plot of Grocery Sales")
```

![](Grocery-Sales-Prediction_files/figure-gfm/unnamed-chunk-1-2.png)<!-- -->

``` r
plot(rules[1:20],method = "graph",control = list(type = "items"))
```

    ## Warning: Unknown control parameters: type

    ## Available control parameters (with default values):
    ## layout    =  list(fun = function (graph, dim = 2, ...)  {     if ("layout" %in% graph_attr_names(graph)) {         lay <- graph_attr(graph, "layout")         if (is.function(lay)) {             lay(graph, ...)         }         else {             lay         }     }     else if (all(c("x", "y") %in% vertex_attr_names(graph))) {         if ("z" %in% vertex_attr_names(graph)) {             cbind(V(graph)$x, V(graph)$y, V(graph)$z)         }         else {             cbind(V(graph)$x, V(graph)$y)         }     }     else if (vcount(graph) < 1000) {         layout_with_fr(graph, dim = dim, ...)     }     else {         layout_with_drl(graph, dim = dim, ...)     } }, call_str = c("layout_nicely(<graph>, input = \"D:/R-Notebook/Association Rules/R-Notebook/Grocery Sales Prediction.Rmd\", ", "    encoding = \"UTF-8\")"), args = list())
    ## edges     =  <environment>
    ## nodes     =  <environment>
    ## nodetext  =  <environment>
    ## colors    =  c("#EE0000FF", "#EEEEEEFF")
    ## engine    =  ggplot2
    ## max   =  100
    ## verbose   =  FALSE

![](Grocery-Sales-Prediction_files/figure-gfm/unnamed-chunk-1-3.png)<!-- -->

``` r
plot(rules[1:20],method = "paracoord",control = list(reorder = TRUE))
```

![](Grocery-Sales-Prediction_files/figure-gfm/unnamed-chunk-1-4.png)<!-- -->

``` r
image(df[1:5])
```

![](Grocery-Sales-Prediction_files/figure-gfm/unnamed-chunk-1-5.png)<!-- -->

``` r
image(sample(df, 100))
```

![](Grocery-Sales-Prediction_files/figure-gfm/unnamed-chunk-1-6.png)<!-- -->

``` r
plot(rules)
```

    ## To reduce overplotting, jitter is added! Use jitter = 0 to prevent jitter.

![](Grocery-Sales-Prediction_files/figure-gfm/unnamed-chunk-1-7.png)<!-- -->

``` r
plot(rules, measure = c("support", "lift"), shading = "confidence")
```

    ## To reduce overplotting, jitter is added! Use jitter = 0 to prevent jitter.

![](Grocery-Sales-Prediction_files/figure-gfm/unnamed-chunk-1-8.png)<!-- -->

``` r
plot(rules, shading="order", control=list(main = "Two-key plot"))
```

    ## To reduce overplotting, jitter is added! Use jitter = 0 to prevent jitter.

![](Grocery-Sales-Prediction_files/figure-gfm/unnamed-chunk-1-9.png)<!-- -->

``` r
plot(rules, method="matrix", measure= c("support", "lift"))
```

    ## Itemsets in Antecedent (LHS)
    ##   [1] "{liquor,red/blush wine}"                                                   
    ##   [2] "{citrus fruit,other vegetables,soda,fruit/vegetable juice}"                
    ##   [3] "{tropical fruit,other vegetables,whole milk,yogurt,oil}"                   
    ##   [4] "{citrus fruit,grapes,fruit/vegetable juice}"                               
    ##   [5] "{other vegetables,whole milk,yogurt,rice}"                                 
    ##   [6] "{tropical fruit,other vegetables,whole milk,oil}"                          
    ##   [7] "{ham,pip fruit,other vegetables,yogurt}"                                   
    ##   [8] "{beef,citrus fruit,tropical fruit,other vegetables}"                       
    ##   [9] "{tropical fruit,whole milk,butter,sliced cheese}"                          
    ##  [10] "{other vegetables,curd,whipped/sour cream,cream cheese }"                  
    ##  [11] "{tropical fruit,other vegetables,butter,white bread}"                      
    ##  [12] "{sausage,pip fruit,sliced cheese}"                                         
    ##  [13] "{tropical fruit,whole milk,butter,curd}"                                   
    ##  [14] "{tropical fruit,butter,margarine}"                                         
    ##  [15] "{whole milk,curd,whipped/sour cream,cream cheese }"                        
    ##  [16] "{whipped/sour cream,cream cheese ,margarine}"                              
    ##  [17] "{beef,tropical fruit,butter}"                                              
    ##  [18] "{pork,tropical fruit,fruit/vegetable juice}"                               
    ##  [19] "{tropical fruit,other vegetables,butter,curd}"                             
    ##  [20] "{tropical fruit,root vegetables,whole milk,margarine}"                     
    ##  [21] "{other vegetables,butter milk,pastry}"                                     
    ##  [22] "{whipped/sour cream,pastry,fruit/vegetable juice}"                         
    ##  [23] "{tropical fruit,butter,white bread}"                                       
    ##  [24] "{citrus fruit,root vegetables,soft cheese}"                                
    ##  [25] "{pip fruit,whipped/sour cream,brown bread}"                                
    ##  [26] "{tropical fruit,grapes,whole milk,yogurt}"                                 
    ##  [27] "{ham,tropical fruit,pip fruit,yogurt}"                                     
    ##  [28] "{ham,tropical fruit,pip fruit,whole milk}"                                 
    ##  [29] "{tropical fruit,butter,whipped/sour cream,fruit/vegetable juice}"          
    ##  [30] "{whole milk,rolls/buns,soda,newspapers}"                                   
    ##  [31] "{citrus fruit,tropical fruit,root vegetables,whipped/sour cream}"          
    ##  [32] "{root vegetables,butter,cream cheese }"                                    
    ##  [33] "{root vegetables,whole milk,yogurt,oil}"                                   
    ##  [34] "{citrus fruit,tropical fruit,root vegetables,whole milk,yogurt}"           
    ##  [35] "{butter,whipped/sour cream,soda}"                                          
    ##  [36] "{pip fruit,butter,pastry}"                                                 
    ##  [37] "{root vegetables,whole milk,yogurt,rice}"                                  
    ##  [38] "{tropical fruit,whipped/sour cream,soft cheese}"                           
    ##  [39] "{citrus fruit,root vegetables,cream cheese }"                              
    ##  [40] "{citrus fruit,whole milk,whipped/sour cream,domestic eggs}"                
    ##  [41] "{grapes,onions}"                                                           
    ##  [42] "{hard cheese,oil}"                                                         
    ##  [43] "{tropical fruit,dessert,whipped/sour cream}"                               
    ##  [44] "{citrus fruit,whole milk,whipped/sour cream,cream cheese }"                
    ##  [45] "{tropical fruit,yogurt,whipped/sour cream,fruit/vegetable juice}"          
    ##  [46] "{herbs,whole milk,fruit/vegetable juice}"                                  
    ##  [47] "{tropical fruit,whipped/sour cream,hard cheese}"                           
    ##  [48] "{pork,whole milk,butter milk}"                                             
    ##  [49] "{pip fruit,butter milk,fruit/vegetable juice}"                             
    ##  [50] "{yogurt,oil,coffee}"                                                       
    ##  [51] "{root vegetables,onions,napkins}"                                          
    ##  [52] "{hamburger meat,tropical fruit,whipped/sour cream}"                        
    ##  [53] "{tropical fruit,butter,yogurt,white bread}"                                
    ##  [54] "{root vegetables,whole milk,butter,white bread}"                           
    ##  [55] "{whole milk,butter,whipped/sour cream,soda}"                               
    ##  [56] "{tropical fruit,whole milk,whipped/sour cream,fruit/vegetable juice}"      
    ##  [57] "{tropical fruit,root vegetables,whole milk,yogurt,oil}"                    
    ##  [58] "{citrus fruit,root vegetables,whole milk,yogurt,whipped/sour cream}"       
    ##  [59] "{tropical fruit,whipped/sour cream,fruit/vegetable juice}"                 
    ##  [60] "{ham,tropical fruit,pip fruit}"                                            
    ##  [61] "{tropical fruit,root vegetables,onions}"                                   
    ##  [62] "{citrus fruit,tropical fruit,root vegetables,whole milk}"                  
    ##  [63] "{citrus fruit,root vegetables,pastry}"                                     
    ##  [64] "{whole milk,yogurt,whipped/sour cream,frozen vegetables}"                  
    ##  [65] "{root vegetables,whole milk,yogurt,frozen vegetables}"                     
    ##  [66] "{tropical fruit,whipped/sour cream,cream cheese }"                         
    ##  [67] "{root vegetables,onions,frozen vegetables}"                                
    ##  [68] "{tropical fruit,root vegetables,whole milk,oil}"                           
    ##  [69] "{pork,butter milk}"                                                        
    ##  [70] "{meat,root vegetables,yogurt}"                                             
    ##  [71] "{pip fruit,root vegetables,butter milk}"                                   
    ##  [72] "{tropical fruit,oil,fruit/vegetable juice}"                                
    ##  [73] "{root vegetables,yogurt,salty snack}"                                      
    ##  [74] "{whipped/sour cream,domestic eggs,pastry}"                                 
    ##  [75] "{root vegetables,domestic eggs,pastry}"                                    
    ##  [76] "{pip fruit,root vegetables,whole milk,brown bread}"                        
    ##  [77] "{citrus fruit,root vegetables,whole milk,domestic eggs}"                   
    ##  [78] "{meat,margarine}"                                                          
    ##  [79] "{tropical fruit,root vegetables,oil}"                                      
    ##  [80] "{root vegetables,curd,cream cheese }"                                      
    ##  [81] "{tropical fruit,whipped/sour cream,white bread}"                           
    ##  [82] "{pip fruit,whole milk,frozen fish}"                                        
    ##  [83] "{root vegetables,herbs,shopping bags}"                                     
    ##  [84] "{citrus fruit,oil,fruit/vegetable juice}"                                  
    ##  [85] "{beef,root vegetables,sugar}"                                              
    ##  [86] "{tropical fruit,root vegetables,long life bakery product}"                 
    ##  [87] "{pork,pip fruit,soda}"                                                     
    ##  [88] "{root vegetables,pastry,shopping bags}"                                    
    ##  [89] "{root vegetables,whole milk,whipped/sour cream,white bread}"               
    ##  [90] "{beef,tropical fruit,whole milk,whipped/sour cream}"                       
    ##  [91] "{citrus fruit,root vegetables,whole milk,curd}"                            
    ##  [92] "{tropical fruit,root vegetables,butter,whipped/sour cream}"                
    ##  [93] "{tropical fruit,whole milk,soda,newspapers}"                               
    ##  [94] "{tropical fruit,root vegetables,yogurt,fruit/vegetable juice}"             
    ##  [95] "{citrus fruit,tropical fruit,root vegetables,rolls/buns}"                  
    ##  [96] "{citrus fruit,root vegetables,newspapers}"                                 
    ##  [97] "{whole milk,yogurt,rice}"                                                  
    ##  [98] "{tropical fruit,pip fruit,frozen fish}"                                    
    ##  [99] "{root vegetables,rolls/buns,flour}"                                        
    ## [100] "{pork,grapes,whole milk}"                                                  
    ## [101] "{meat,tropical fruit,whole milk}"                                          
    ## [102] "{root vegetables,rolls/buns,candy}"                                        
    ## [103] "{ham,whole milk,frozen vegetables}"                                        
    ## [104] "{sausage,dessert,whipped/sour cream}"                                      
    ## [105] "{chicken,root vegetables,pastry}"                                          
    ## [106] "{sausage,whipped/sour cream,brown bread}"                                  
    ## [107] "{root vegetables,whole milk,whipped/sour cream,hard cheese}"               
    ## [108] "{tropical fruit,root vegetables,onions,whole milk}"                        
    ## [109] "{whole milk,butter,yogurt,white bread}"                                    
    ## [110] "{root vegetables,whole milk,frozen vegetables,bottled water}"              
    ## [111] "{beef,citrus fruit,tropical fruit,root vegetables}"                        
    ## [112] "{frankfurter,pip fruit,root vegetables,whole milk}"                        
    ## [113] "{citrus fruit,root vegetables,whole milk,newspapers}"                      
    ## [114] "{tropical fruit,whole milk,butter,yogurt,domestic eggs}"                   
    ## [115] "{tropical fruit,root vegetables,yogurt,oil}"                               
    ## [116] "{yogurt,rice}"                                                             
    ## [117] "{herbs,shopping bags}"                                                     
    ## [118] "{tropical fruit,grapes,yogurt}"                                            
    ## [119] "{citrus fruit,root vegetables,onions}"                                     
    ## [120] "{citrus fruit,whole milk,yogurt,fruit/vegetable juice}"                    
    ## [121] "{onions,butter milk}"                                                      
    ## [122] "{tropical fruit,yogurt,semi-finished bread}"                               
    ## [123] "{root vegetables,whipped/sour cream,hard cheese}"                          
    ## [124] "{tropical fruit,yogurt,salty snack}"                                       
    ## [125] "{citrus fruit,whipped/sour cream,cream cheese }"                           
    ## [126] "{sausage,whipped/sour cream,bottled water}"                                
    ## [127] "{citrus fruit,tropical fruit,whole milk,fruit/vegetable juice}"            
    ## [128] "{citrus fruit,tropical fruit,root vegetables,yogurt}"                      
    ## [129] "{turkey,curd}"                                                             
    ## [130] "{herbs,fruit/vegetable juice}"                                             
    ## [131] "{onions,waffles}"                                                          
    ## [132] "{turkey,tropical fruit,root vegetables}"                                   
    ## [133] "{turkey,root vegetables,whole milk}"                                       
    ## [134] "{tropical fruit,grapes,root vegetables}"                                   
    ## [135] "{tropical fruit,grapes,whole milk}"                                        
    ## [136] "{onions,whole milk,napkins}"                                               
    ## [137] "{tropical fruit,whipped/sour cream,salty snack}"                           
    ## [138] "{butter,whipped/sour cream,long life bakery product}"                      
    ## [139] "{tropical fruit,whole milk,yogurt,cream cheese }"                          
    ## [140] "{pip fruit,whole milk,whipped/sour cream,domestic eggs}"                   
    ## [141] "{root vegetables,whole milk,yogurt,fruit/vegetable juice}"                 
    ## [142] "{tropical fruit,pip fruit,whole milk,whipped/sour cream}"                  
    ## [143] "{sausage,citrus fruit,whole milk,whipped/sour cream}"                      
    ## [144] "{tropical fruit,root vegetables,whipped/sour cream,rolls/buns}"            
    ## [145] "{frankfurter,tropical fruit,frozen meals}"                                 
    ## [146] "{tropical fruit,butter,yogurt,domestic eggs}"                              
    ## [147] "{root vegetables,yogurt,rice}"                                             
    ## [148] "{citrus fruit,tropical fruit,herbs}"                                       
    ## [149] "{root vegetables,butter,white bread}"                                      
    ## [150] "{butter,curd,domestic eggs}"                                               
    ## [151] "{rice,sugar}"                                                              
    ## [152] "{canned fish,hygiene articles}"                                            
    ## [153] "{root vegetables,butter,rice}"                                             
    ## [154] "{root vegetables,whipped/sour cream,flour}"                                
    ## [155] "{butter,soft cheese,domestic eggs}"                                        
    ## [156] "{pip fruit,butter,hygiene articles}"                                       
    ## [157] "{root vegetables,whipped/sour cream,hygiene articles}"                     
    ## [158] "{pip fruit,root vegetables,hygiene articles}"                              
    ## [159] "{cream cheese ,domestic eggs,sugar}"                                       
    ## [160] "{curd,domestic eggs,sugar}"                                                
    ## [161] "{cream cheese ,domestic eggs,napkins}"                                     
    ## [162] "{root vegetables,other vegetables,yogurt,oil}"                             
    ## [163] "{root vegetables,other vegetables,butter,white bread}"                     
    ## [164] "{pork,other vegetables,butter,whipped/sour cream}"                         
    ## [165] "{other vegetables,butter,whipped/sour cream,domestic eggs}"                
    ## [166] "{citrus fruit,whipped/sour cream,rolls/buns,pastry}"                       
    ## [167] "{pip fruit,root vegetables,other vegetables,bottled water}"                
    ## [168] "{sausage,tropical fruit,root vegetables,rolls/buns}"                       
    ## [169] "{tropical fruit,root vegetables,other vegetables,yogurt,oil}"              
    ## [170] "{root vegetables,whipped/sour cream,white bread}"                          
    ## [171] "{beef,tropical fruit,whipped/sour cream}"                                  
    ## [172] "{pip fruit,root vegetables,brown bread}"                                   
    ## [173] "{root vegetables,soft cheese,domestic eggs}"                               
    ## [174] "{butter,whipped/sour cream,sugar}"                                         
    ## [175] "{citrus fruit,root vegetables,herbs}"                                      
    ## [176] "{other vegetables,cream cheese ,sugar}"                                    
    ## [177] "{sausage,tropical fruit,root vegetables,yogurt}"                           
    ## [178] "{citrus fruit,domestic eggs,sugar}"                                        
    ## [179] "{yogurt,domestic eggs,sugar}"                                              
    ## [180] "{pip fruit,whipped/sour cream,cream cheese }"                              
    ## [181] "{root vegetables,other vegetables,yogurt,rice}"                            
    ## [182] "{beef,tropical fruit,yogurt,rolls/buns}"                                   
    ## [183] "{tropical fruit,butter,yogurt,whipped/sour cream}"                         
    ## [184] "{whipped/sour cream,house keeping products}"                               
    ## [185] "{rice,bottled water}"                                                      
    ## [186] "{root vegetables,whipped/sour cream,soft cheese}"                          
    ## [187] "{butter,whipped/sour cream,sliced cheese}"                                 
    ## [188] "{tropical fruit,butter,hygiene articles}"                                  
    ## [189] "{tropical fruit,domestic eggs,hygiene articles}"                           
    ## [190] "{root vegetables,whipped/sour cream,sugar}"                                
    ## [191] "{tropical fruit,long life bakery product,napkins}"                         
    ## [192] "{butter,whipped/sour cream,coffee}"                                        
    ## [193] "{root vegetables,other vegetables,yogurt,hard cheese}"                     
    ## [194] "{frankfurter,tropical fruit,root vegetables,yogurt}"                       
    ## [195] "{pip fruit,root vegetables,other vegetables,brown bread}"                  
    ## [196] "{pip fruit,other vegetables,whipped/sour cream,domestic eggs}"             
    ## [197] "{soups,bottled beer}"                                                      
    ## [198] "{tropical fruit,domestic eggs,sugar}"                                      
    ## [199] "{citrus fruit,butter,curd}"                                                
    ## [200] "{domestic eggs,margarine,fruit/vegetable juice}"                           
    ## [201] "{pip fruit,other vegetables,yogurt,cream cheese }"                         
    ## [202] "{tropical fruit,curd,yogurt,domestic eggs}"                                
    ## [203] "{root vegetables,butter,yogurt,domestic eggs}"                             
    ## [204] "{tropical fruit,yogurt,whipped/sour cream,domestic eggs}"                  
    ## [205] "{tropical fruit,other vegetables,whipped/sour cream,domestic eggs}"        
    ## [206] "{pip fruit,root vegetables,yogurt,fruit/vegetable juice}"                  
    ## [207] "{tropical fruit,root vegetables,rolls/buns,bottled water}"                 
    ## [208] "{curd,cereals}"                                                            
    ## [209] "{pastry,sweet spreads}"                                                    
    ## [210] "{tropical fruit,butter,frozen meals}"                                      
    ## [211] "{frankfurter,root vegetables,sliced cheese}"                               
    ## [212] "{sausage,berries,butter}"                                                  
    ## [213] "{butter,hygiene articles,napkins}"                                         
    ## [214] "{pork,rolls/buns,waffles}"                                                 
    ## [215] "{whipped/sour cream,long life bakery product,napkins}"                     
    ## [216] "{sausage,butter,long life bakery product}"                                 
    ## [217] "{sausage,pip fruit,cream cheese }"                                         
    ## [218] "{root vegetables,domestic eggs,coffee}"                                    
    ## [219] "{domestic eggs,margarine,bottled beer}"                                    
    ## [220] "{tropical fruit,root vegetables,herbs,other vegetables}"                   
    ## [221] "{tropical fruit,pip fruit,yogurt,frozen meals}"                            
    ## [222] "{tropical fruit,butter,yogurt,sliced cheese}"                              
    ## [223] "{root vegetables,other vegetables,yogurt,waffles}"                         
    ## [224] "{pip fruit,root vegetables,other vegetables,cream cheese }"                
    ## [225] "{citrus fruit,other vegetables,yogurt,frozen vegetables}"                  
    ## [226] "{citrus fruit,tropical fruit,curd,yogurt}"                                 
    ## [227] "{other vegetables,butter,whipped/sour cream,napkins}"                      
    ## [228] "{pork,root vegetables,other vegetables,butter}"                            
    ## [229] "{root vegetables,other vegetables,rolls/buns,brown bread}"                 
    ## [230] "{citrus fruit,other vegetables,butter,bottled water}"                      
    ## [231] "{citrus fruit,tropical fruit,other vegetables,domestic eggs}"              
    ## [232] "{tropical fruit,root vegetables,yogurt,pastry}"                            
    ## [233] "{tropical fruit,other vegetables,butter,yogurt,domestic eggs}"             
    ## [234] "{pip fruit,butter,whipped/sour cream}"                                     
    ## [235] "{tropical fruit,whipped/sour cream,domestic eggs}"                         
    ## [236] "{tropical fruit,root vegetables,butter,yogurt}"                            
    ## [237] "{citrus fruit,cream cheese ,domestic eggs}"                                
    ## [238] "{whipped/sour cream,rolls/buns,margarine}"                                 
    ## [239] "{tropical fruit,root vegetables,herbs}"                                    
    ## [240] "{root vegetables,butter milk,yogurt}"                                      
    ## [241] "{citrus fruit,whipped/sour cream,pastry}"                                  
    ## [242] "{beef,tropical fruit,other vegetables,rolls/buns}"                         
    ## [243] "{pork,butter,whipped/sour cream}"                                          
    ## [244] "{frozen vegetables,rolls/buns,margarine}"                                  
    ## [245] "{root vegetables,onions,other vegetables,butter}"                          
    ## [246] "{pip fruit,other vegetables,butter,whipped/sour cream}"                    
    ## [247] "{pip fruit,root vegetables,other vegetables,fruit/vegetable juice}"        
    ## [248] "{citrus fruit,tropical fruit,other vegetables,bottled water}"              
    ## [249] "{tropical fruit,pip fruit,root vegetables,other vegetables,yogurt}"        
    ## [250] "{oil,mustard}"                                                             
    ## [251] "{pickled vegetables,chocolate}"                                            
    ## [252] "{root vegetables,yogurt,frozen fish}"                                      
    ## [253] "{other vegetables,yogurt,frozen fish}"                                     
    ## [254] "{root vegetables,herbs,curd}"                                              
    ## [255] "{other vegetables,flour,sugar}"                                            
    ## [256] "{tropical fruit,onions,butter}"                                            
    ## [257] "{butter,bottled water,hygiene articles}"                                   
    ## [258] "{citrus fruit,root vegetables,hygiene articles}"                           
    ## [259] "{root vegetables,yogurt,hygiene articles}"                                 
    ## [260] "{other vegetables,long life bakery product,napkins}"                       
    ## [261] "{whipped/sour cream,cream cheese ,domestic eggs}"                          
    ## [262] "{sausage,chicken,domestic eggs}"                                           
    ## [263] "{pip fruit,butter,domestic eggs}"                                          
    ## [264] "{butter,whipped/sour cream,bottled water}"                                 
    ## [265] "{other vegetables,yogurt,whipped/sour cream,long life bakery product}"     
    ## [266] "{root vegetables,other vegetables,frozen vegetables,fruit/vegetable juice}"
    ## [267] "{root vegetables,other vegetables,curd,domestic eggs}"                     
    ## [268] "{citrus fruit,other vegetables,curd,yogurt}"                               
    ## [269] "{citrus fruit,root vegetables,other vegetables,brown bread}"               
    ## [270] "{citrus fruit,other vegetables,whipped/sour cream,domestic eggs}"          
    ## [271] "{pip fruit,root vegetables,other vegetables,domestic eggs}"                
    ## [272] "{pip fruit,root vegetables,yogurt,soda}"                                   
    ## [273] "{root vegetables,onions,butter}"                                           
    ## [274] "{pip fruit,root vegetables,other vegetables,whipped/sour cream}"           
    ## [275] "{domestic eggs,rice}"                                                      
    ## [276] "{tropical fruit,yogurt,baking powder}"                                     
    ## [277] "{curd,yogurt,frozen meals}"                                                
    ## [278] "{tropical fruit,root vegetables,frozen meals}"                             
    ## [279] "{sausage,butter milk,yogurt}"                                              
    ## [280] "{onions,curd,yogurt}"                                                      
    ## [281] "{onions,butter,domestic eggs}"                                             
    ## [282] "{pip fruit,yogurt,salty snack}"                                            
    ## [283] "{citrus fruit,whipped/sour cream,sugar}"                                   
    ## [284] "{butter,whipped/sour cream,cream cheese }"                                 
    ## [285] "{pip fruit,cream cheese ,domestic eggs}"                                   
    ## [286] "{citrus fruit,frozen vegetables,napkins}"                                  
    ## [287] "{butter,bottled water,napkins}"                                            
    ## [288] "{butter,yogurt,napkins}"                                                   
    ## [289] "{pork,rolls/buns,bottled beer}"                                            
    ## [290] "{pork,other vegetables,butter}"                                            
    ## [291] "{curd,yogurt,whipped/sour cream,cream cheese }"                            
    ## [292] "{citrus fruit,other vegetables,cream cheese ,domestic eggs}"               
    ## [293] "{citrus fruit,other vegetables,whipped/sour cream,cream cheese }"          
    ## [294] "{tropical fruit,pip fruit,yogurt,brown bread}"                             
    ## [295] "{other vegetables,yogurt,brown bread,soda}"                                
    ## [296] "{root vegetables,yogurt,rolls/buns,margarine}"                             
    ## [297] "{root vegetables,other vegetables,rolls/buns,margarine}"                   
    ## [298] "{root vegetables,butter,yogurt,whipped/sour cream}"                        
    ## [299] "{beef,tropical fruit,root vegetables,other vegetables,rolls/buns}"         
    ## [300] "{tropical fruit,root vegetables,other vegetables,butter,yogurt}"           
    ## [301] "{root vegetables,rolls/buns,waffles}"                                      
    ## [302] "{citrus fruit,frozen vegetables,fruit/vegetable juice}"                    
    ## [303] "{butter,whipped/sour cream,domestic eggs}"                                 
    ## [304] "{butter,jam}"                                                              
    ## [305] "{butter,rice}"                                                             
    ## [306] "{tropical fruit,other vegetables,rice}"                                    
    ## [307] "{herbs,other vegetables,domestic eggs}"                                    
    ## [308] "{herbs,other vegetables,fruit/vegetable juice}"                            
    ## [309] "{tropical fruit,herbs,yogurt}"                                             
    ## [310] "{root vegetables,herbs,rolls/buns}"                                        
    ## [311] "{other vegetables,whipped/sour cream,detergent}"                           
    ## [312] "{frankfurter,yogurt,sliced cheese}"                                        
    ## [313] "{root vegetables,oil,shopping bags}"                                       
    ## [314] "{onions,butter,bottled water}"                                             
    ## [315] "{berries,butter,whipped/sour cream}"                                       
    ## [316] "{hamburger meat,root vegetables,whipped/sour cream}"                       
    ## [317] "{rolls/buns,margarine,hygiene articles}"                                   
    ## [318] "{citrus fruit,butter,hygiene articles}"                                    
    ## [319] "{tropical fruit,whipped/sour cream,hygiene articles}"                      
    ## [320] "{other vegetables,salty snack,long life bakery product}"                   
    ## [321] "{sausage,whipped/sour cream,long life bakery product}"                     
    ## [322] "{pork,yogurt,cream cheese }"                                               
    ## [323] "{frankfurter,yogurt,cream cheese }"                                        
    ## [324] "{sausage,domestic eggs,chocolate}"                                         
    ## [325] "{yogurt,coffee,bottled beer}"                                              
    ## [326] "{beef,butter,curd}"                                                        
    ## [327] "{root vegetables,domestic eggs,brown bread}"                               
    ## [328] "{whipped/sour cream,margarine,bottled water}"                              
    ## [329] "{root vegetables,herbs,other vegetables,rolls/buns}"                       
    ## [330] "{root vegetables,other vegetables,butter milk,yogurt}"                     
    ## [331] "{tropical fruit,root vegetables,yogurt,sliced cheese}"                     
    ## [332] "{pip fruit,root vegetables,yogurt,white bread}"                            
    ## [333] "{citrus fruit,other vegetables,frozen vegetables,fruit/vegetable juice}"   
    ## [334] "{root vegetables,other vegetables,frozen vegetables,bottled water}"        
    ## [335] "{pip fruit,other vegetables,curd,whipped/sour cream}"                      
    ## [336] "{frankfurter,pip fruit,root vegetables,other vegetables}"                  
    ## [337] "{tropical fruit,root vegetables,yogurt,domestic eggs}"                     
    ## [338] "{root vegetables,other vegetables,yogurt,fruit/vegetable juice}"           
    ## [339] "{citrus fruit,root vegetables,other vegetables,yogurt,whipped/sour cream}" 
    ## [340] "{tropical fruit,yogurt,hard cheese}"                                       
    ## [341] "{root vegetables,butter,hygiene articles}"                                 
    ## [342] "{other vegetables,curd,domestic eggs}"                                     
    ## [343] "{butter,whipped/sour cream,napkins}"                                       
    ## [344] "{pork,butter,yogurt}"                                                      
    ## [345] "{sausage,pip fruit,domestic eggs}"                                         
    ## [346] "{beef,tropical fruit,root vegetables,rolls/buns}"                          
    ## [347] "{frankfurter,tropical fruit,other vegetables,yogurt}"                      
    ## [348] "{citrus fruit,tropical fruit,root vegetables,other vegetables,yogurt}"     
    ## [349] "{tropical fruit,herbs}"                                                    
    ## [350] "{citrus fruit,root vegetables,other vegetables,yogurt}"                    
    ## [351] "{root vegetables,other vegetables,rice}"                                   
    ## [352] "{pip fruit,curd,whipped/sour cream}"                                       
    ## [353] "{tropical fruit,root vegetables,yogurt,rolls/buns}"                        
    ## [354] "{napkins,house keeping products}"                                          
    ## [355] "{other vegetables,yogurt,specialty cheese}"                                
    ## [356] "{tropical fruit,herbs,other vegetables}"                                   
    ## [357] "{butter,yogurt,hard cheese}"                                               
    ## [358] "{dessert,butter milk,yogurt}"                                              
    ## [359] "{onions,butter,yogurt}"                                                    
    ## [360] "{sausage,pip fruit,hygiene articles}"                                      
    ## [361] "{other vegetables,sugar,bottled water}"                                    
    ## [362] "{yogurt,cream cheese ,domestic eggs}"                                      
    ## [363] "{other vegetables,frozen vegetables,napkins}"                              
    ## [364] "{curd,rolls/buns,margarine}"                                               
    ## [365] "{tropical fruit,domestic eggs,napkins}"                                    
    ## [366] "{pip fruit,butter,bottled water}"                                          
    ## [367] "{tropical fruit,domestic eggs,pastry}"                                     
    ## [368] "{root vegetables,other vegetables,whipped/sour cream,frozen vegetables}"   
    ## [369] "{pip fruit,root vegetables,other vegetables,soda}"                         
    ## [370] "{yogurt,cereals}"                                                          
    ## [371] "{hamburger meat,bottled beer}"                                             
    ## [372] "{whipped/sour cream,rolls/buns,waffles}"                                   
    ## [373] "{yogurt,whipped/sour cream,long life bakery product}"                      
    ## [374] "{pip fruit,root vegetables,butter}"                                        
    ## [375] "{hamburger meat,curd}"                                                     
    ## [376] "{herbs,rolls/buns}"                                                        
    ## [377] "{butter,yogurt,soft cheese}"                                               
    ## [378] "{frankfurter,other vegetables,frozen meals}"                               
    ## [379] "{tropical fruit,yogurt,frozen meals}"                                      
    ## [380] "{hamburger meat,other vegetables,curd}"                                    
    ## [381] "{hamburger meat,butter,whipped/sour cream}"                                
    ## [382] "{tropical fruit,hygiene articles,napkins}"                                 
    ## [383] "{chicken,butter,whipped/sour cream}"                                       
    ## [384] "{root vegetables,chocolate,napkins}"                                       
    ## [385] "{pip fruit,butter,curd}"                                                   
    ## [386] "{rolls/buns,bottled beer,napkins}"                                         
    ## [387] "{whipped/sour cream,domestic eggs,margarine}"                              
    ## [388] "{citrus fruit,tropical fruit,butter}"                                      
    ## [389] "{sausage,pip fruit,whipped/sour cream}"                                    
    ## [390] "{chicken,root vegetables,other vegetables,domestic eggs}"                  
    ## [391] "{tropical fruit,butter,curd,yogurt}"                                       
    ## [392] "{other vegetables,butter,whipped/sour cream,rolls/buns}"                   
    ## [393] "{sausage,citrus fruit,other vegetables,whipped/sour cream}"                
    ## [394] "{citrus fruit,tropical fruit,yogurt,whipped/sour cream}"                   
    ## Itemsets in Consequent (RHS)
    ## [1] "{whole milk}"       "{other vegetables}" "{yogurt}"          
    ## [4] "{tropical fruit}"   "{root vegetables}"  "{bottled beer}"

![](Grocery-Sales-Prediction_files/figure-gfm/unnamed-chunk-1-10.png)<!-- -->
