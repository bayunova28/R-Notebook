Delhi House Price Prediction
================
Bayu Nova
July 28, 2021

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook.

``` r
#import library
library(readr)
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

``` r
library(ggplot2)
library(visreg)
library(visreg)
library(rgl)
library(knitr)
library(scatterplot3d)
library(MASS)
```

    ## 
    ## Attaching package: 'MASS'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     select

``` r
library(GGally)
```

    ## Registered S3 method overwritten by 'GGally':
    ##   method from   
    ##   +.gg   ggplot2

``` r
#data extraction
MagicBricks <- read_csv("D:/MagicBricks.csv")
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   Area = col_double(),
    ##   BHK = col_double(),
    ##   Bathroom = col_double(),
    ##   Furnishing = col_character(),
    ##   Locality = col_character(),
    ##   Parking = col_double(),
    ##   Price = col_double(),
    ##   Status = col_character(),
    ##   Transaction = col_character(),
    ##   Type = col_character(),
    ##   Per_Sqft = col_double()
    ## )

``` r
View(MagicBricks)
str(MagicBricks)
```

    ## spec_tbl_df [1,259 x 11] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
    ##  $ Area       : num [1:1259] 800 750 950 600 650 1300 1350 650 985 1300 ...
    ##  $ BHK        : num [1:1259] 3 2 2 2 2 4 4 2 3 4 ...
    ##  $ Bathroom   : num [1:1259] 2 2 2 2 2 3 3 2 3 4 ...
    ##  $ Furnishing : chr [1:1259] "Semi-Furnished" "Semi-Furnished" "Furnished" "Semi-Furnished" ...
    ##  $ Locality   : chr [1:1259] "Rohini Sector 25" "J R Designers Floors, Rohini Sector 24" "Citizen Apartment, Rohini Sector 13" "Rohini Sector 24" ...
    ##  $ Parking    : num [1:1259] 1 1 1 1 1 1 1 1 1 1 ...
    ##  $ Price      : num [1:1259] 6500000 5000000 15500000 4200000 6200000 15500000 10000000 4000000 6800000 15000000 ...
    ##  $ Status     : chr [1:1259] "Ready_to_move" "Ready_to_move" "Ready_to_move" "Ready_to_move" ...
    ##  $ Transaction: chr [1:1259] "New_Property" "New_Property" "Resale" "Resale" ...
    ##  $ Type       : chr [1:1259] "Builder_Floor" "Apartment" "Apartment" "Builder_Floor" ...
    ##  $ Per_Sqft   : num [1:1259] NA 6667 6667 6667 6667 ...
    ##  - attr(*, "spec")=
    ##   .. cols(
    ##   ..   Area = col_double(),
    ##   ..   BHK = col_double(),
    ##   ..   Bathroom = col_double(),
    ##   ..   Furnishing = col_character(),
    ##   ..   Locality = col_character(),
    ##   ..   Parking = col_double(),
    ##   ..   Price = col_double(),
    ##   ..   Status = col_character(),
    ##   ..   Transaction = col_character(),
    ##   ..   Type = col_character(),
    ##   ..   Per_Sqft = col_double()
    ##   .. )

``` r
dim(MagicBricks)
```

    ## [1] 1259   11

``` r
knitr::kable(head(MagicBricks))
```

| Area | BHK | Bathroom | Furnishing     | Locality                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | Parking |    Price | Status          | Transaction   | Type           | Per\_Sqft |
|-----:|----:|---------:|:---------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------:|---------:|:----------------|:--------------|:---------------|----------:|
|  800 |   3 |        2 | Semi-Furnished | Rohini Sector 25                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |       1 |  6500000 | Ready\_to\_move | New\_Property | Builder\_Floor |        NA |
|  750 |   2 |        2 | Semi-Furnished | J R Designers Floors, Rohini Sector 24                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |       1 |  5000000 | Ready\_to\_move | New\_Property | Apartment      |      6667 |
|  950 |   2 |        2 | Furnished      | Citizen Apartment, Rohini Sector 13                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |       1 | 15500000 | Ready\_to\_move | Resale        | Apartment      |      6667 |
|  600 |   2 |        2 | Semi-Furnished | Rohini Sector 24                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |       1 |  4200000 | Ready\_to\_move | Resale        | Builder\_Floor |      6667 |
|  650 |   2 |        2 | Semi-Furnished | Rohini Sector 24 carpet area 650 sqft status Ready to Move floor 4 out of 4 floors transaction New Property furnishing Semi-Furnished facing East overlooking Garden/Park, Main Road car parking 1 Open bathroom 2 balcony 1 ownership Freehold Newly Constructed Property Newly Constructed Property East Facing Property 2BHK Newly build property for Sale. A House is waiting for a Friendly Family to make it a lovely home.So please come and make his house feel alive once again. read more Contact Agent View Phone No. Share Feedback Garima properties Certified Agent Trusted by Users Genuine Listings Market Knowledge |       1 |  6200000 | Ready\_to\_move | New\_Property | Builder\_Floor |      6667 |
| 1300 |   4 |        3 | Semi-Furnished | Rohini Sector 24                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |       1 | 15500000 | Ready\_to\_move | New\_Property | Builder\_Floor |      6667 |

``` r
summary(MagicBricks)
```

    ##       Area            BHK            Bathroom      Furnishing       
    ##  Min.   :   28   Min.   : 1.000   Min.   :1.000   Length:1259       
    ##  1st Qu.:  800   1st Qu.: 2.000   1st Qu.:2.000   Class :character  
    ##  Median : 1200   Median : 3.000   Median :2.000   Mode  :character  
    ##  Mean   : 1466   Mean   : 2.797   Mean   :2.556                     
    ##  3rd Qu.: 1700   3rd Qu.: 3.000   3rd Qu.:3.000                     
    ##  Max.   :24300   Max.   :10.000   Max.   :7.000                     
    ##                                   NA's   :2                         
    ##    Locality            Parking            Price              Status         
    ##  Length:1259        Min.   :  1.000   Min.   :  1000000   Length:1259       
    ##  Class :character   1st Qu.:  1.000   1st Qu.:  5700000   Class :character  
    ##  Mode  :character   Median :  1.000   Median : 14200000   Mode  :character  
    ##                     Mean   :  1.936   Mean   : 21306704                     
    ##                     3rd Qu.:  2.000   3rd Qu.: 25500000                     
    ##                     Max.   :114.000   Max.   :240000000                     
    ##                     NA's   :33                                              
    ##  Transaction            Type              Per_Sqft     
    ##  Length:1259        Length:1259        Min.   :  1259  
    ##  Class :character   Class :character   1st Qu.:  6364  
    ##  Mode  :character   Mode  :character   Median : 11292  
    ##                                        Mean   : 15690  
    ##                                        3rd Qu.: 18000  
    ##                                        Max.   :183333  
    ##                                        NA's   :241

``` r
#selection data
df <- MagicBricks[,c("Type", "Transaction", "Status", "Price")]
head(df)
```

    ## # A tibble: 6 x 4
    ##   Type          Transaction  Status           Price
    ##   <chr>         <chr>        <chr>            <dbl>
    ## 1 Builder_Floor New_Property Ready_to_move  6500000
    ## 2 Apartment     New_Property Ready_to_move  5000000
    ## 3 Apartment     Resale       Ready_to_move 15500000
    ## 4 Builder_Floor Resale       Ready_to_move  4200000
    ## 5 Builder_Floor New_Property Ready_to_move  6200000
    ## 6 Builder_Floor New_Property Ready_to_move 15500000

``` r
#transform data
table(df['Type'])
```

    ## 
    ##     Apartment Builder_Floor 
    ##           593           661

``` r
df$Type[df$Type == "Apartment"] <- 0
df$Type[df$Type == "Builder_Floor"] <- 1

table(df['Transaction'])
```

    ## 
    ## New_Property       Resale 
    ##          478          781

``` r
df$Transaction[df$Transaction == "New_Property"] <- 0
df$Transaction[df$Transaction == "Resale"] <- 1

table(df['Status'])
```

    ## 
    ##  Almost_ready Ready_to_move 
    ##            75          1184

``` r
df$Status[df$Status == "Almost_ready"] <- 0
df$Status[df$Status == "Ready_to_move"] <- 1
head(df)
```

    ## # A tibble: 6 x 4
    ##   Type  Transaction Status    Price
    ##   <chr> <chr>       <chr>     <dbl>
    ## 1 1     0           1       6500000
    ## 2 0     0           1       5000000
    ## 3 0     1           1      15500000
    ## 4 1     1           1       4200000
    ## 5 1     0           1       6200000
    ## 6 1     0           1      15500000

``` r
#linear regression model
set.seed(34000)
target <- sample(2, nrow(df), replace = TRUE)
training <- df[target == 2,]
testing <- df[target == 1,]

lr_model1 <- lm(Price ~ ., data = training)
summary(lr_model1)
```

    ## 
    ## Call:
    ## lm(formula = Price ~ ., data = training)
    ## 
    ## Residuals:
    ##       Min        1Q    Median        3Q       Max 
    ## -40754536 -15560071  -5195596   5304404 211254037 
    ## 
    ## Coefficients:
    ##               Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)   40460071    4604065   8.788  < 2e-16 ***
    ## Type1          5794465    2139078   2.709 0.006933 ** 
    ## Transaction1  -7550367    2264078  -3.335 0.000903 ***
    ## Status1      -17508573    4845715  -3.613 0.000326 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 26020000 on 637 degrees of freedom
    ##   (3 observations deleted due to missingness)
    ## Multiple R-squared:  0.06486,    Adjusted R-squared:  0.06045 
    ## F-statistic: 14.73 on 3 and 637 DF,  p-value: 2.783e-09

``` r
lr_model2 <- lm(Type ~ Price + Transaction + Status, data = training)
summary(lr_model2)
```

    ## 
    ## Call:
    ## lm(formula = Type ~ Price + Transaction + Status, data = training)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -0.9181 -0.4243  0.1851  0.5246  0.9686 
    ## 
    ## Coefficients:
    ##                Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)   2.834e-01  8.908e-02   3.182  0.00153 ** 
    ## Price         1.965e-09  7.255e-10   2.709  0.00693 ** 
    ## Transaction1 -2.609e-01  4.077e-02  -6.400 3.02e-10 ***
    ## Status1       3.693e-01  8.896e-02   4.151 3.76e-05 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.4791 on 637 degrees of freedom
    ##   (3 observations deleted due to missingness)
    ## Multiple R-squared:  0.08663,    Adjusted R-squared:  0.08233 
    ## F-statistic: 20.14 on 3 and 637 DF,  p-value: 1.764e-12

``` r
#prediction
prediction <- predict(lr_model2, testing)
summary(prediction)
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ## 0.03236 0.40558 0.42917 0.50702 0.66256 1.08512

``` r
#accuracy score
accuracy <- mean((testing$Price - prediction) ^ 2)
print(paste('Accuracy Score : ', accuracy))
```

    ## [1] "Accuracy Score :  1009155543336113"

``` r
#correlation
actual <- data.frame(prediction, testing$Price)
names(actual) <- c("Price", "Predicted")
correlation_target <- cor(actual)
print(correlation_target)
```

    ##              Price Predicted
    ## Price     1.000000  0.401745
    ## Predicted 0.401745  1.000000

``` r
#evaluate prediction
step(lr_model1, direction = "backward")
```

    ## Start:  AIC=21893.16
    ## Price ~ Type + Transaction + Status
    ## 
    ##               Df  Sum of Sq        RSS   AIC
    ## <none>                      4.3115e+17 21893
    ## - Type         1 4.9667e+15 4.3612e+17 21899
    ## - Transaction  1 7.5274e+15 4.3868e+17 21902
    ## - Status       1 8.8364e+15 4.3999e+17 21904

    ## 
    ## Call:
    ## lm(formula = Price ~ Type + Transaction + Status, data = training)
    ## 
    ## Coefficients:
    ##  (Intercept)         Type1  Transaction1       Status1  
    ##     40460071       5794465      -7550367     -17508573

``` r
#visualization
hist(training$Price)
```

![](Delhi-House-Price-Prediction_files/figure-gfm/unnamed-chunk-1-1.png)<!-- -->

``` r
qqnorm(training$Price)
qqline(training$Price)
```

![](Delhi-House-Price-Prediction_files/figure-gfm/unnamed-chunk-1-2.png)<!-- -->

``` r
plot(df, col = "darkorange", main = "Matrix")
```

![](Delhi-House-Price-Prediction_files/figure-gfm/unnamed-chunk-1-3.png)<!-- -->

``` r
plot(lr_model1, which = 1)
```

![](Delhi-House-Price-Prediction_files/figure-gfm/unnamed-chunk-1-4.png)<!-- -->

``` r
plot(lr_model2, which = 1)
```

![](Delhi-House-Price-Prediction_files/figure-gfm/unnamed-chunk-1-5.png)<!-- -->

``` r
scatter_plot <- training %>% ggplot(aes(Type, Price)) + geom_boxplot()
scatter_plot
```

![](Delhi-House-Price-Prediction_files/figure-gfm/unnamed-chunk-1-6.png)<!-- -->

``` r
scatter_plot <- training %>% ggplot(aes(Transaction, Price)) + geom_boxplot()
scatter_plot
```

![](Delhi-House-Price-Prediction_files/figure-gfm/unnamed-chunk-1-7.png)<!-- -->

``` r
scatter_plot <- training %>% ggplot(aes(Status, Price)) + geom_boxplot()
scatter_plot
```

![](Delhi-House-Price-Prediction_files/figure-gfm/unnamed-chunk-1-8.png)<!-- -->

``` r
plot(lr_model1$residuals, main = "Residuals")
```

![](Delhi-House-Price-Prediction_files/figure-gfm/unnamed-chunk-1-9.png)<!-- -->

``` r
ggcorr(MagicBricks, label = T, hjust = 1)
```

    ## Warning in ggcorr(MagicBricks, label = T, hjust = 1): data in column(s)
    ## 'Furnishing', 'Locality', 'Status', 'Transaction', 'Type' are not numeric and
    ## were ignored

![](Delhi-House-Price-Prediction_files/figure-gfm/unnamed-chunk-1-10.png)<!-- -->
