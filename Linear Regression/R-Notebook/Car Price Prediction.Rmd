---
title: "Car Price Prediction"
author: Bayu Nova
date: July 22, 2021
output: github_document
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook.

```{r}
#import library
library(readr)
library(dplyr)
library(ISLR)
library(MASS)
library(corrplot)
library(ggplot2)
library(visreg)
library(visreg)
library(rgl)
library(knitr)
library(scatterplot3d)

#data extraction
CarPrice_Assignment <- read_csv("D:/CarPrice_Assignment.csv")
View(CarPrice_Assignment)
dim(CarPrice_Assignment)
str(CarPrice_Assignment)
knitr::kable(head(CarPrice_Assignment))
summary(CarPrice_Assignment)

#selection data target
df <- CarPrice_Assignment[,c("enginesize", "horsepower", "price")]
head(df)

#training & testing data target
set.seed(34000)
target <- sample(2, nrow(df), replace = TRUE)
training <- df[target == 2,]
testing <- df[target == 1,]

#linear regression model
lr1 <- lm(price ~ ., data = training)
summary(lr1)
lr2 <- lm(enginesize ~ price + horsepower, data = training)
summary(lr2)
lr3 <- lm(log(enginesize) ~ price + horsepower, data = training)
summary(lr3)
lr4 <- lm(log(enginesize) ~ log(price) + horsepower, data = df)
summary(lr4)

#prediction 
prediction <- predict(lr4, testing)
accuracy_score <- mean((testing$price - prediction) ^ 2)
print(accuracy_score)
sigma(lr4) / mean(testing$price)

#correlation data target
testing$prediction <- predict(lr4, testing)
actual_pred <- data.frame(testing$prediction, testing$price)
names(actual_pred) <- c("price", "Prediction")
correlation <- cor(actual_pred)
print(correlation)
actual_pred$log_price <- log(actual_pred)
head(actual_pred)

#visualization
hist(training$price)
qqnorm(training$price)
qqline(training$price)

plot(df, col = "midnightblue", main = "Matrix of Target Data")
plot(lr1, which = 1)
plot(lr2, which = 1)
plot(lr3, which = 1)
plot(lr4, which = 1)

plot <- training %>%
ggplot(aes(horsepower, price)) +
geom_boxplot()
plot
plot <- training %>%
ggplot(aes(enginesize, price)) +
geom_boxplot()
plot

ggplot(training, aes(x=horsepower, y=price)) +
geom_point(color='red', size = 4) +
geom_smooth(method=lm, se=FALSE, fullrange=TRUE, color = 'black')
ggplot(training, aes(x=enginesize, y=price)) +
geom_point(color='blue', size = 4) +
geom_smooth(method=lm, se=FALSE, fullrange=TRUE, color = 'black')
```
