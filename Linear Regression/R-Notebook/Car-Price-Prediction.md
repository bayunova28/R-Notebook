Car Price Prediction
================
Bayu Nova
July 22, 2021

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook.

``` r
#import library
library(readr)
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

``` r
library(ISLR)
library(MASS)
```

    ## 
    ## Attaching package: 'MASS'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     select

``` r
library(corrplot)
```

    ## corrplot 0.90 loaded

``` r
library(ggplot2)
library(visreg)
library(visreg)
library(rgl)
library(knitr)
library(scatterplot3d)

#data extraction
CarPrice_Assignment <- read_csv("D:/CarPrice_Assignment.csv")
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   .default = col_double(),
    ##   CarName = col_character(),
    ##   fueltype = col_character(),
    ##   aspiration = col_character(),
    ##   doornumber = col_character(),
    ##   carbody = col_character(),
    ##   drivewheel = col_character(),
    ##   enginelocation = col_character(),
    ##   enginetype = col_character(),
    ##   cylindernumber = col_character(),
    ##   fuelsystem = col_character()
    ## )
    ## i Use `spec()` for the full column specifications.

``` r
View(CarPrice_Assignment)
dim(CarPrice_Assignment)
```

    ## [1] 205  26

``` r
str(CarPrice_Assignment)
```

    ## spec_tbl_df [205 x 26] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
    ##  $ car_ID          : num [1:205] 1 2 3 4 5 6 7 8 9 10 ...
    ##  $ symboling       : num [1:205] 3 3 1 2 2 2 1 1 1 0 ...
    ##  $ CarName         : chr [1:205] "alfa-romero giulia" "alfa-romero stelvio" "alfa-romero Quadrifoglio" "audi 100 ls" ...
    ##  $ fueltype        : chr [1:205] "gas" "gas" "gas" "gas" ...
    ##  $ aspiration      : chr [1:205] "std" "std" "std" "std" ...
    ##  $ doornumber      : chr [1:205] "two" "two" "two" "four" ...
    ##  $ carbody         : chr [1:205] "convertible" "convertible" "hatchback" "sedan" ...
    ##  $ drivewheel      : chr [1:205] "rwd" "rwd" "rwd" "fwd" ...
    ##  $ enginelocation  : chr [1:205] "front" "front" "front" "front" ...
    ##  $ wheelbase       : num [1:205] 88.6 88.6 94.5 99.8 99.4 ...
    ##  $ carlength       : num [1:205] 169 169 171 177 177 ...
    ##  $ carwidth        : num [1:205] 64.1 64.1 65.5 66.2 66.4 66.3 71.4 71.4 71.4 67.9 ...
    ##  $ carheight       : num [1:205] 48.8 48.8 52.4 54.3 54.3 53.1 55.7 55.7 55.9 52 ...
    ##  $ curbweight      : num [1:205] 2548 2548 2823 2337 2824 ...
    ##  $ enginetype      : chr [1:205] "dohc" "dohc" "ohcv" "ohc" ...
    ##  $ cylindernumber  : chr [1:205] "four" "four" "six" "four" ...
    ##  $ enginesize      : num [1:205] 130 130 152 109 136 136 136 136 131 131 ...
    ##  $ fuelsystem      : chr [1:205] "mpfi" "mpfi" "mpfi" "mpfi" ...
    ##  $ boreratio       : num [1:205] 3.47 3.47 2.68 3.19 3.19 3.19 3.19 3.19 3.13 3.13 ...
    ##  $ stroke          : num [1:205] 2.68 2.68 3.47 3.4 3.4 3.4 3.4 3.4 3.4 3.4 ...
    ##  $ compressionratio: num [1:205] 9 9 9 10 8 8.5 8.5 8.5 8.3 7 ...
    ##  $ horsepower      : num [1:205] 111 111 154 102 115 110 110 110 140 160 ...
    ##  $ peakrpm         : num [1:205] 5000 5000 5000 5500 5500 5500 5500 5500 5500 5500 ...
    ##  $ citympg         : num [1:205] 21 21 19 24 18 19 19 19 17 16 ...
    ##  $ highwaympg      : num [1:205] 27 27 26 30 22 25 25 25 20 22 ...
    ##  $ price           : num [1:205] 13495 16500 16500 13950 17450 ...
    ##  - attr(*, "spec")=
    ##   .. cols(
    ##   ..   car_ID = col_double(),
    ##   ..   symboling = col_double(),
    ##   ..   CarName = col_character(),
    ##   ..   fueltype = col_character(),
    ##   ..   aspiration = col_character(),
    ##   ..   doornumber = col_character(),
    ##   ..   carbody = col_character(),
    ##   ..   drivewheel = col_character(),
    ##   ..   enginelocation = col_character(),
    ##   ..   wheelbase = col_double(),
    ##   ..   carlength = col_double(),
    ##   ..   carwidth = col_double(),
    ##   ..   carheight = col_double(),
    ##   ..   curbweight = col_double(),
    ##   ..   enginetype = col_character(),
    ##   ..   cylindernumber = col_character(),
    ##   ..   enginesize = col_double(),
    ##   ..   fuelsystem = col_character(),
    ##   ..   boreratio = col_double(),
    ##   ..   stroke = col_double(),
    ##   ..   compressionratio = col_double(),
    ##   ..   horsepower = col_double(),
    ##   ..   peakrpm = col_double(),
    ##   ..   citympg = col_double(),
    ##   ..   highwaympg = col_double(),
    ##   ..   price = col_double()
    ##   .. )

``` r
knitr::kable(head(CarPrice_Assignment))
```

| car\_ID | symboling | CarName                  | fueltype | aspiration | doornumber | carbody     | drivewheel | enginelocation | wheelbase | carlength | carwidth | carheight | curbweight | enginetype | cylindernumber | enginesize | fuelsystem | boreratio | stroke | compressionratio | horsepower | peakrpm | citympg | highwaympg | price |
|--------:|----------:|:-------------------------|:---------|:-----------|:-----------|:------------|:-----------|:---------------|----------:|----------:|---------:|----------:|-----------:|:-----------|:---------------|-----------:|:-----------|----------:|-------:|-----------------:|-----------:|--------:|--------:|-----------:|------:|
|       1 |         3 | alfa-romero giulia       | gas      | std        | two        | convertible | rwd        | front          |      88.6 |     168.8 |     64.1 |      48.8 |       2548 | dohc       | four           |        130 | mpfi       |      3.47 |   2.68 |              9.0 |        111 |    5000 |      21 |         27 | 13495 |
|       2 |         3 | alfa-romero stelvio      | gas      | std        | two        | convertible | rwd        | front          |      88.6 |     168.8 |     64.1 |      48.8 |       2548 | dohc       | four           |        130 | mpfi       |      3.47 |   2.68 |              9.0 |        111 |    5000 |      21 |         27 | 16500 |
|       3 |         1 | alfa-romero Quadrifoglio | gas      | std        | two        | hatchback   | rwd        | front          |      94.5 |     171.2 |     65.5 |      52.4 |       2823 | ohcv       | six            |        152 | mpfi       |      2.68 |   3.47 |              9.0 |        154 |    5000 |      19 |         26 | 16500 |
|       4 |         2 | audi 100 ls              | gas      | std        | four       | sedan       | fwd        | front          |      99.8 |     176.6 |     66.2 |      54.3 |       2337 | ohc        | four           |        109 | mpfi       |      3.19 |   3.40 |             10.0 |        102 |    5500 |      24 |         30 | 13950 |
|       5 |         2 | audi 100ls               | gas      | std        | four       | sedan       | 4wd        | front          |      99.4 |     176.6 |     66.4 |      54.3 |       2824 | ohc        | five           |        136 | mpfi       |      3.19 |   3.40 |              8.0 |        115 |    5500 |      18 |         22 | 17450 |
|       6 |         2 | audi fox                 | gas      | std        | two        | sedan       | fwd        | front          |      99.8 |     177.3 |     66.3 |      53.1 |       2507 | ohc        | five           |        136 | mpfi       |      3.19 |   3.40 |              8.5 |        110 |    5500 |      19 |         25 | 15250 |

``` r
summary(CarPrice_Assignment)
```

    ##      car_ID      symboling         CarName            fueltype        
    ##  Min.   :  1   Min.   :-2.0000   Length:205         Length:205        
    ##  1st Qu.: 52   1st Qu.: 0.0000   Class :character   Class :character  
    ##  Median :103   Median : 1.0000   Mode  :character   Mode  :character  
    ##  Mean   :103   Mean   : 0.8341                                        
    ##  3rd Qu.:154   3rd Qu.: 2.0000                                        
    ##  Max.   :205   Max.   : 3.0000                                        
    ##   aspiration         doornumber          carbody           drivewheel       
    ##  Length:205         Length:205         Length:205         Length:205        
    ##  Class :character   Class :character   Class :character   Class :character  
    ##  Mode  :character   Mode  :character   Mode  :character   Mode  :character  
    ##                                                                             
    ##                                                                             
    ##                                                                             
    ##  enginelocation       wheelbase        carlength        carwidth    
    ##  Length:205         Min.   : 86.60   Min.   :141.1   Min.   :60.30  
    ##  Class :character   1st Qu.: 94.50   1st Qu.:166.3   1st Qu.:64.10  
    ##  Mode  :character   Median : 97.00   Median :173.2   Median :65.50  
    ##                     Mean   : 98.76   Mean   :174.0   Mean   :65.91  
    ##                     3rd Qu.:102.40   3rd Qu.:183.1   3rd Qu.:66.90  
    ##                     Max.   :120.90   Max.   :208.1   Max.   :72.30  
    ##    carheight       curbweight    enginetype        cylindernumber    
    ##  Min.   :47.80   Min.   :1488   Length:205         Length:205        
    ##  1st Qu.:52.00   1st Qu.:2145   Class :character   Class :character  
    ##  Median :54.10   Median :2414   Mode  :character   Mode  :character  
    ##  Mean   :53.72   Mean   :2556                                        
    ##  3rd Qu.:55.50   3rd Qu.:2935                                        
    ##  Max.   :59.80   Max.   :4066                                        
    ##    enginesize     fuelsystem          boreratio        stroke     
    ##  Min.   : 61.0   Length:205         Min.   :2.54   Min.   :2.070  
    ##  1st Qu.: 97.0   Class :character   1st Qu.:3.15   1st Qu.:3.110  
    ##  Median :120.0   Mode  :character   Median :3.31   Median :3.290  
    ##  Mean   :126.9                      Mean   :3.33   Mean   :3.255  
    ##  3rd Qu.:141.0                      3rd Qu.:3.58   3rd Qu.:3.410  
    ##  Max.   :326.0                      Max.   :3.94   Max.   :4.170  
    ##  compressionratio   horsepower       peakrpm        citympg     
    ##  Min.   : 7.00    Min.   : 48.0   Min.   :4150   Min.   :13.00  
    ##  1st Qu.: 8.60    1st Qu.: 70.0   1st Qu.:4800   1st Qu.:19.00  
    ##  Median : 9.00    Median : 95.0   Median :5200   Median :24.00  
    ##  Mean   :10.14    Mean   :104.1   Mean   :5125   Mean   :25.22  
    ##  3rd Qu.: 9.40    3rd Qu.:116.0   3rd Qu.:5500   3rd Qu.:30.00  
    ##  Max.   :23.00    Max.   :288.0   Max.   :6600   Max.   :49.00  
    ##    highwaympg        price      
    ##  Min.   :16.00   Min.   : 5118  
    ##  1st Qu.:25.00   1st Qu.: 7788  
    ##  Median :30.00   Median :10295  
    ##  Mean   :30.75   Mean   :13277  
    ##  3rd Qu.:34.00   3rd Qu.:16503  
    ##  Max.   :54.00   Max.   :45400

``` r
#selection data target
df <- CarPrice_Assignment[,c("enginesize", "horsepower", "price")]
head(df)
```

    ## # A tibble: 6 x 3
    ##   enginesize horsepower price
    ##        <dbl>      <dbl> <dbl>
    ## 1        130        111 13495
    ## 2        130        111 16500
    ## 3        152        154 16500
    ## 4        109        102 13950
    ## 5        136        115 17450
    ## 6        136        110 15250

``` r
#training & testing data target
set.seed(34000)
target <- sample(2, nrow(df), replace = TRUE)
training <- df[target == 2,]
testing <- df[target == 1,]

#linear regression model
lr1 <- lm(price ~ ., data = training)
summary(lr1)
```

    ## 
    ## Call:
    ## lm(formula = price ~ ., data = training)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -10010.8  -1948.9   -374.5   1220.7  14182.2 
    ## 
    ## Coefficients:
    ##             Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept) -8181.18    1104.46  -7.407 3.83e-11 ***
    ## enginesize    133.51      13.78   9.692 3.91e-16 ***
    ## horsepower     40.71      15.20   2.679   0.0086 ** 
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 3841 on 102 degrees of freedom
    ## Multiple R-squared:  0.8148, Adjusted R-squared:  0.8112 
    ## F-statistic: 224.4 on 2 and 102 DF,  p-value: < 2.2e-16

``` r
lr2 <- lm(enginesize ~ price + horsepower, data = training)
summary(lr2)
```

    ## 
    ## Call:
    ## lm(formula = enginesize ~ price + horsepower, data = training)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -59.239  -7.062   0.544  11.676  63.826 
    ## 
    ## Coefficients:
    ##              Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept) 4.721e+01  5.349e+00   8.826 3.21e-14 ***
    ## price       3.591e-03  3.705e-04   9.692 3.91e-16 ***
    ## horsepower  3.271e-01  7.482e-02   4.371 2.98e-05 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 19.92 on 102 degrees of freedom
    ## Multiple R-squared:  0.833,  Adjusted R-squared:  0.8298 
    ## F-statistic: 254.5 on 2 and 102 DF,  p-value: < 2.2e-16

``` r
lr3 <- lm(log(enginesize) ~ price + horsepower, data = training)
summary(lr3)
```

    ## 
    ## Call:
    ## lm(formula = log(enginesize) ~ price + horsepower, data = training)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -0.56035 -0.07306  0.02241  0.10052  0.25525 
    ## 
    ## Coefficients:
    ##              Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept) 4.266e+00  3.863e-02 110.440  < 2e-16 ***
    ## price       1.987e-05  2.676e-06   7.425 3.52e-11 ***
    ## horsepower  2.692e-03  5.403e-04   4.982 2.57e-06 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.1438 on 102 degrees of freedom
    ## Multiple R-squared:  0.7934, Adjusted R-squared:  0.7893 
    ## F-statistic: 195.8 on 2 and 102 DF,  p-value: < 2.2e-16

``` r
lr4 <- lm(log(enginesize) ~ log(price) + horsepower, data = df)
summary(lr4)
```

    ## 
    ## Call:
    ## lm(formula = log(enginesize) ~ log(price) + horsepower, data = df)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -0.59545 -0.06078  0.00640  0.08466  0.33418 
    ## 
    ## Coefficients:
    ##              Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept) 1.6315749  0.2876560   5.672 4.84e-08 ***
    ## log(price)  0.3104654  0.0346663   8.956 2.24e-16 ***
    ## horsepower  0.0025385  0.0004417   5.748 3.30e-08 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.1407 on 202 degrees of freedom
    ## Multiple R-squared:  0.7553, Adjusted R-squared:  0.7529 
    ## F-statistic: 311.7 on 2 and 202 DF,  p-value: < 2.2e-16

``` r
#prediction 
prediction <- predict(lr4, testing)
accuracy_score <- mean((testing$price - prediction) ^ 2)
print(accuracy_score)
```

    ## [1] 209444056

``` r
sigma(lr4) / mean(testing$price)
```

    ## [1] 1.107697e-05

``` r
#correlation data target
testing$prediction <- predict(lr4, testing)
actual_pred <- data.frame(testing$prediction, testing$price)
names(actual_pred) <- c("price", "Prediction")
correlation <- cor(actual_pred)
print(correlation)
```

    ##                price Prediction
    ## price      1.0000000  0.9455384
    ## Prediction 0.9455384  1.0000000

``` r
actual_pred$log_price <- log(actual_pred)
head(actual_pred)
```

    ##      price Prediction log_price.price log_price.Prediction
    ## 1 4.865902      13495        1.582252             9.510075
    ## 2 4.928318      16500        1.594998             9.711116
    ## 3 5.037475      16500        1.616905             9.711116
    ## 4 4.853350      13950        1.579669             9.543235
    ## 5 4.955852      17450        1.600569             9.767095
    ## 6 4.947751      17710        1.598933             9.781885

``` r
#visualization
hist(training$price)
```

![](Car-Price-Prediction_files/figure-gfm/unnamed-chunk-1-1.png)<!-- -->

``` r
qqnorm(training$price)
qqline(training$price)
```

![](Car-Price-Prediction_files/figure-gfm/unnamed-chunk-1-2.png)<!-- -->

``` r
plot(df, col = "midnightblue", main = "Matrix of Target Data")
```

![](Car-Price-Prediction_files/figure-gfm/unnamed-chunk-1-3.png)<!-- -->

``` r
plot(lr1, which = 1)
```

![](Car-Price-Prediction_files/figure-gfm/unnamed-chunk-1-4.png)<!-- -->

``` r
plot(lr2, which = 1)
```

![](Car-Price-Prediction_files/figure-gfm/unnamed-chunk-1-5.png)<!-- -->

``` r
plot(lr3, which = 1)
```

![](Car-Price-Prediction_files/figure-gfm/unnamed-chunk-1-6.png)<!-- -->

``` r
plot(lr4, which = 1)
```

![](Car-Price-Prediction_files/figure-gfm/unnamed-chunk-1-7.png)<!-- -->

``` r
plot <- training %>%
ggplot(aes(horsepower, price)) +
geom_boxplot()
plot
```

    ## Warning: Continuous x aesthetic -- did you forget aes(group=...)?

![](Car-Price-Prediction_files/figure-gfm/unnamed-chunk-1-8.png)<!-- -->

``` r
plot <- training %>%
ggplot(aes(enginesize, price)) +
geom_boxplot()
plot
```

    ## Warning: Continuous x aesthetic -- did you forget aes(group=...)?

![](Car-Price-Prediction_files/figure-gfm/unnamed-chunk-1-9.png)<!-- -->

``` r
ggplot(training, aes(x=horsepower, y=price)) +
geom_point(color='red', size = 4) +
geom_smooth(method=lm, se=FALSE, fullrange=TRUE, color = 'black')
```

    ## `geom_smooth()` using formula 'y ~ x'

![](Car-Price-Prediction_files/figure-gfm/unnamed-chunk-1-10.png)<!-- -->

``` r
ggplot(training, aes(x=enginesize, y=price)) +
geom_point(color='blue', size = 4) +
geom_smooth(method=lm, se=FALSE, fullrange=TRUE, color = 'black')
```

    ## `geom_smooth()` using formula 'y ~ x'

![](Car-Price-Prediction_files/figure-gfm/unnamed-chunk-1-11.png)<!-- -->
