Amazon Alexa Reviews
================
Bayu Nova
July 24, 2021

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook.

``` r
#import library
library(readr)
library(tidytext)
library(textdata)
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

``` r
library(tidyr)
library(ggplot2)
library(tidyverse)
```

    ## -- Attaching packages --------------------------------------- tidyverse 1.3.1 --

    ## v tibble  3.1.2     v stringr 1.4.0
    ## v purrr   0.3.4     v forcats 0.5.1

    ## -- Conflicts ------------------------------------------ tidyverse_conflicts() --
    ## x dplyr::filter() masks stats::filter()
    ## x dplyr::lag()    masks stats::lag()

``` r
library(wordcloud)
```

    ## Loading required package: RColorBrewer

``` r
library(reshape2)
```

    ## 
    ## Attaching package: 'reshape2'

    ## The following object is masked from 'package:tidyr':
    ## 
    ##     smiths

``` r
#data extraction
amazon_alexa <- read_csv("D:/amazon_alexa.csv")
```

    ## 
    ## -- Column specification --------------------------------------------------------
    ## cols(
    ##   rating = col_double(),
    ##   date = col_character(),
    ##   variation = col_character(),
    ##   verified_reviews = col_character(),
    ##   feedback = col_double()
    ## )

``` r
View(amazon_alexa)
str(amazon_alexa)
```

    ## spec_tbl_df [3,150 x 5] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
    ##  $ rating          : num [1:3150] 5 5 4 5 5 5 3 5 5 5 ...
    ##  $ date            : chr [1:3150] "31-Jul-18" "31-Jul-18" "31-Jul-18" "31-Jul-18" ...
    ##  $ variation       : chr [1:3150] "Charcoal Fabric" "Charcoal Fabric" "Walnut Finish" "Charcoal Fabric" ...
    ##  $ verified_reviews: chr [1:3150] "Love my Echo!" "Loved it!" "Sometimes while playing a game, you can answer a question correctly but Alexa says you got it wrong and answers"| __truncated__ "I have had a lot of fun with this thing. My 4 yr old learns about dinosaurs, i control the lights and play game"| __truncated__ ...
    ##  $ feedback        : num [1:3150] 1 1 1 1 1 1 1 1 1 1 ...
    ##  - attr(*, "spec")=
    ##   .. cols(
    ##   ..   rating = col_double(),
    ##   ..   date = col_character(),
    ##   ..   variation = col_character(),
    ##   ..   verified_reviews = col_character(),
    ##   ..   feedback = col_double()
    ##   .. )

``` r
dim(amazon_alexa)
```

    ## [1] 3150    5

``` r
knitr::kable(head(amazon_alexa))
```

| rating | date      | variation           | verified\_reviews                                                                                                                                                                                  | feedback |
|-------:|:----------|:--------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------:|
|      5 | 31-Jul-18 | Charcoal Fabric     | Love my Echo!                                                                                                                                                                                      |        1 |
|      5 | 31-Jul-18 | Charcoal Fabric     | Loved it!                                                                                                                                                                                          |        1 |
|      4 | 31-Jul-18 | Walnut Finish       | Sometimes while playing a game, you can answer a question correctly but Alexa says you got it wrong and answers the same as you. I like being able to turn lights on and off while away from home. |        1 |
|      5 | 31-Jul-18 | Charcoal Fabric     | I have had a lot of fun with this thing. My 4 yr old learns about dinosaurs, i control the lights and play games like categories. Has nice sound when playing music as well.                       |        1 |
|      5 | 31-Jul-18 | Charcoal Fabric     | Music                                                                                                                                                                                              |        1 |
|      5 | 31-Jul-18 | Heather Gray Fabric | I received the echo as a gift. I needed another Bluetooth or something to play music easily accessible, and found this smart speaker. Can’t wait to see what else it can do.                       |        1 |

``` r
summary(amazon_alexa)
```

    ##      rating          date            variation         verified_reviews  
    ##  Min.   :1.000   Length:3150        Length:3150        Length:3150       
    ##  1st Qu.:4.000   Class :character   Class :character   Class :character  
    ##  Median :5.000   Mode  :character   Mode  :character   Mode  :character  
    ##  Mean   :4.463                                                           
    ##  3rd Qu.:5.000                                                           
    ##  Max.   :5.000                                                           
    ##     feedback     
    ##  Min.   :0.0000  
    ##  1st Qu.:1.0000  
    ##  Median :1.0000  
    ##  Mean   :0.9184  
    ##  3rd Qu.:1.0000  
    ##  Max.   :1.0000

``` r
#selection data target
df <- amazon_alexa[,c("variation", "verified_reviews")]
head(df)
```

    ## # A tibble: 6 x 2
    ##   variation         verified_reviews                                            
    ##   <chr>             <chr>                                                       
    ## 1 Charcoal Fabric   Love my Echo!                                               
    ## 2 Charcoal Fabric   Loved it!                                                   
    ## 3 Walnut Finish     Sometimes while playing a game, you can answer a question c~
    ## 4 Charcoal Fabric   I have had a lot of fun with this thing. My 4 yr old learns~
    ## 5 Charcoal Fabric   Music                                                       
    ## 6 Heather Gray Fab~ I received the echo as a gift. I needed another Bluetooth o~

``` r
#tokenise data target
df_target <- df %>%
select(verified_reviews) %>%
unnest_tokens(word, verified_reviews)
print(df_target)
```

    ## # A tibble: 80,037 x 1
    ##    word     
    ##    <chr>    
    ##  1 love     
    ##  2 my       
    ##  3 echo     
    ##  4 loved    
    ##  5 it       
    ##  6 sometimes
    ##  7 while    
    ##  8 playing  
    ##  9 a        
    ## 10 game     
    ## # ... with 80,027 more rows

``` r
#remove stop words
remove <- df_target %>%
anti_join(stop_words)
```

    ## Joining, by = "word"

``` r
print(remove)
```

    ## # A tibble: 27,459 x 1
    ##    word     
    ##    <chr>    
    ##  1 love     
    ##  2 echo     
    ##  3 loved    
    ##  4 playing  
    ##  5 game     
    ##  6 answer   
    ##  7 question 
    ##  8 correctly
    ##  9 alexa    
    ## 10 wrong    
    ## # ... with 27,449 more rows

``` r
#sentiment analysis model & visualization
remove %>%
count(word, sort = TRUE) %>%
top_n(20) %>%
mutate(word = reorder(word, n)) %>%
ggplot(aes(x = word, y = n)) +
geom_col() +
coord_flip() +
theme_light() +
labs(x = "Unique Review", y = "Count", title = "Unique Review on Amazon Alexa")
```

    ## Selecting by n

![](Amazon-Alexa-Reviews_files/figure-gfm/unnamed-chunk-1-1.png)<!-- -->

``` r
reaction <- remove %>%
inner_join(get_sentiments("bing")) %>%
count(word, sentiment, sort = TRUE) %>%
ungroup()
```

    ## Joining, by = "word"

``` r
print(reaction)
```

    ## # A tibble: 584 x 3
    ##    word    sentiment     n
    ##    <chr>   <chr>     <int>
    ##  1 love    positive    955
    ##  2 easy    positive    342
    ##  3 smart   positive    184
    ##  4 fun     positive    152
    ##  5 alarm   negative    110
    ##  6 nice    positive     98
    ##  7 amazing positive     84
    ##  8 awesome positive     75
    ##  9 perfect positive     74
    ## 10 pretty  positive     62
    ## # ... with 574 more rows

``` r
reaction %>% group_by(sentiment) %>% top_n(10) %>% ungroup() %>%
mutate(word = reorder(word, n)) %>% ggplot(aes(word, n, fill = sentiment)) +
geom_col(show.legend = FALSE) + facet_wrap(~sentiment, scales = "free_y") +
labs(title = "Amazon Alexa Review", y = "Sentiment", x = NULL) + coord_flip() + theme_bw()
```

    ## Selecting by n

![](Amazon-Alexa-Reviews_files/figure-gfm/unnamed-chunk-1-2.png)<!-- -->

``` r
remove %>%
anti_join(stop_words) %>%
count(word) %>%
with(wordcloud(word, n, max.words = 80))
```

    ## Joining, by = "word"

![](Amazon-Alexa-Reviews_files/figure-gfm/unnamed-chunk-1-3.png)<!-- -->

``` r
remove %>%
inner_join(get_sentiments("bing")) %>%
count(word, sentiment, sort = TRUE) %>%
acast(word ~ sentiment, value.var = "n", fill = 0) %>%
comparison.cloud(colors = c("blue", "red", "purple","orange","brown","dark green"),
max.words = 100) #sentiment : positive
```

    ## Joining, by = "word"

![](Amazon-Alexa-Reviews_files/figure-gfm/unnamed-chunk-1-4.png)<!-- -->
